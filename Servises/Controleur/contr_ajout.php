<?php 
require("../trame/auth/EtreAuthentifie.php");
require("../Modele/m1.php");

//controleur_utilisateurs

if(isset($_POST['login']) && isset($_POST['mdp']) && isset($_POST['mdp2']) ){
	if($_POST['mdp'] == $_POST['mdp2']) {
		$pass_hache = password_hash($_POST['mdp'], PASSWORD_DEFAULT);
		$error='Utilisateur ajouté  ';
		ajout_utilisateur($_POST['login'],$pass_hache) ;
		$utilisateur=list_users() ; 
		require("../Vue/l1.php");
	}else{
		$error='mauvais mot de passe ';
		require("../Vue/ajouter_utilisateur.php");
	}

}
//controleur_enseignants

else if(isset($_POST['nom']) && isset($_POST['prenom']) &&  isset($_POST['email']) &&  isset($_POST['tel']) &&  isset($_POST['etid'])) {
	
	
	ajout_enseignants($idm->getUid(),$_POST['nom'],$_POST['prenom'],$_POST['email'],$_POST['tel'] ,$_POST['etid'],$_GET['anne']) ; 
	$enseignants=list_enseignants($_GET['anne']) ; 
	require("../Vue/l2.php");

}
//controleur_modules
else if (isset($_POST['intitule']) && isset($_POST['code']) &&  isset($_POST['enseignants']) &&  isset($_POST['category']) ){

	ajout_modules($_POST['intitule'],$_POST['code'],$_POST['enseignants'],$_POST['category'],$_GET['anne']) ; 

	$modules=list_modules($_GET['anne']) ; 
	require("../Vue/l3.php");
}

//controleur_groupes
else if (isset($_POST['modules']) && isset($_POST['nom'])  &&  isset($_POST['gtypes'])){
	ajout_groupes($_POST['modules'],$_POST['nom'],$_GET['annee'],$_POST['gtypes']) ; 
	$groupes=list_groupes($_GET['annee']) ; 
	require("../Vue/l4.php");

}

//controleur_gtypes
else if (isset($_POST['nom']) && isset($_POST['nbh'])  &&  isset($_POST['coeff']) ){
	
	ajout_gtypes($_POST['nom'],$_POST['nbh'],$_POST['coeff']) ; 
	$gtypes=list_gtypes() ; 
	require("../Vue/l5.php");

}

