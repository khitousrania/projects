<?php 
require("../trame/auth/EtreAuthentifie.php");

require("../Modele/m1.php");


//controleur_enseignants

if(isset($_GET['eid'])){
	
	if( isset($_POST['nom']) &&  !empty($_POST['nom'] )) {

		modif_enseignants_nom($_POST['nom'],$_GET['eid']) ; 
	}
	if(isset($_POST['prenom']) &&  !empty($_POST['prenom'] )) {

		modif_enseignants_prenom($_POST['prenom'],$_GET['eid']) ; 

	}
	if(isset($_POST['email']) &&  !empty($_POST['email'] )) {

		modif_enseignants_email($_POST['email'],$_GET['eid']) ; 
	}
	if(isset($_POST['tel']) &&  !empty($_POST['tel'] )){

		modif_enseignants_tel($_POST['tel'],$_GET['eid']) ; 	
	}
	if(isset($_POST['etid']) &&  !empty($_POST['etid'] )){

		modif_enseignants_etid($_POST['etid'],$_GET['eid']) ; 
		
	}
	$enseignants=list_enseignants($_GET['anne']) ; 
	require("../Vue/l2.php");

}
//controleur_modules

if(isset($_GET['mid'])){
	
	if( isset($_POST['intitule']) &&  !empty($_POST['intitule'] )) {

		modif_modules_intitule($_POST['intitule'],$_GET['mid']) ; 
	}
	if(isset($_POST['code']) &&  !empty($_POST['code'] )) {

		modif_modules_code($_POST['code'],$_GET['mid']) ; 

	}

	if(isset($_POST['enseignants']) &&  !empty($_POST['enseignants'] )) {

		modif_modules_eid($_POST['enseignants'],$_GET['mid']) ; 
	}
	if(isset($_POST['category']) &&  !empty($_POST['category'] )){

		modif_modules_cid($_POST['category'],$_GET['mid']) ; 	
	}
	
	$modules=list_modules($_GET['anne']) ; 
	require("../Vue/l3.php");

}

//controleur_ggroupes

if(isset($_GET['gid'])){
	
	if( isset($_POST['modules']) &&  !empty($_POST['modules'] )) {

		modif_groupes_mid($_POST['modules'],$_GET['gid']) ; 
	}
	if(isset($_POST['nom']) &&  !empty($_POST['nom'] )) {

		modif_groupes_nom($_POST['nom'],$_GET['gid']) ; 

	}

	if(isset($_POST['gtypes']) &&  !empty($_POST['gtypes'] )) {

		modif_groupes_gtid($_POST['gtypes'],$_GET['gid'],$_GET['annee']) ; 
	}
	
	$groupes=list_groupes($_GET['annee']) ; 
	require("../Vue/l4.php");

}

//controleur_gtypes

if(isset($_GET['gtid'])){
	
	if( isset($_POST['nom']) &&  !empty($_POST['nom'] )) {

		modif_gtypes_nom($_POST['nom'],$_GET['gtid']) ; 
	}
	if(isset($_POST['nbh']) &&  !empty($_POST['nbh'] )) {

		modif_gtypes_nbh($_POST['nbh'],$_GET['gtid']) ; 

	}

	if(isset($_POST['coeff']) &&  !empty($_POST['coeff'] )) {

		modif_gtypes_coeff($_POST['coeff'],$_GET['gtid']) ; 
	}
	
	$gtypes=list_gtypes() ; 
	require("../Vue/l5.php");

}







