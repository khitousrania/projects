<?php 

function dbConnect()
{   
    require("../trame/db_config.php");
    try
    {
        $db = new PDO($dsn, $username, $password);
        return $db;
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}
/*UTILISATEURS:*/

function list_users() {
 
	$db=dbConnect() ; 
	$reqeute=$db->prepare('SELECT uid , login , role FROM users
        WHERE role = ? ') ; 
	$reqeute->execute(array("user")); 
	return $reqeute ; 
}





function sup_utilisateur($uid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('DELETE FROM users WHERE uid = ?') ;   
    $reqeute->execute(array($uid)); 
}

function modifiermdp($mdp,$uid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE users SET mdp= ?  WHERE uid = ? ') ;
    $reqeute->execute(array($mdp,$uid)); 
}

function ajout_utilisateur($login,$mdp) {

    $db=dbConnect() ; 
    $reqeute=$db->prepare('INSERT INTO users(login, mdp,role) VALUES (?,?,?) ') ;
    $reqeute->execute(array($login,$mdp,'user')); 
}


/*ENSEIGNANTS*/
function list_enseignants($annee){

    $db=dbConnect() ; 

    $reqeute=$db->prepare('SELECT * FROM enseignants  WHERE annee = ?'); 
    $reqeute->execute(array($annee)); 
    return $reqeute ; 
}

function list_ttenseignants(){

    $db=dbConnect() ; 

    $reqeute=$db->query('SELECT * FROM enseignants '); 
    return $reqeute ; 


}
function list_affect_enseignants($eid){

    $db=dbConnect() ; 

    $reqeute=$db->prepare('SELECT *FROM `enseignants` where eid = ?'); 
    $reqeute->execute(array($eid)); 
    return $reqeute ; 
}


function sup_enseignats($eid,$annee){

    $db=dbConnect() ; 
    $reqeute=$db->prepare('DELETE FROM enseignants WHERE eid = ?  AND annee = ? ' ) ;   
    $reqeute->execute(array($eid,$annee)); 
    

}

function ajout_enseignants($uid,$nom,$prenom,$email,$tel,$etid,$annee){

    $db=dbConnect() ; 
    $reqeute=$db->prepare('INSERT INTO enseignants(uid, nom,prenom,email,tel,etid,annee) VALUES (?,?,?,?,?,?,?) ') ;
    $reqeute->execute(array($uid,$nom,$prenom,$email,$tel,$etid,$annee));

}
function modif_enseignants_nom($nom,$eid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE enseignants SET nom= ?  WHERE eid = ? ') ;
    $reqeute->execute(array($nom,$eid));
}
function modif_enseignants_prenom($prenom,$eid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE enseignants SET prenom= ?  WHERE eid = ? ') ;
    $reqeute->execute(array($prenom,$eid));
}
function modif_enseignants_email($email,$eid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE enseignants SET email= ?  WHERE eid = ? ') ;
    $reqeute->execute(array($email,$eid));
}
function modif_enseignants_tel($tel,$eid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE enseignants SET tel= ?  WHERE eid = ? ') ;
    $reqeute->execute(array($tel,$eid));
}

function modif_enseignants_etid($etid,$eid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE enseignants SET etid= ?  WHERE eid = ? ') ;
    $reqeute->execute(array($etid,$eid));
}


/*MODULES*/
function list_modules($annee) {
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT mid,intitule, code, enseignants.nom as Nom, enseignants.prenom as Prenom , categories.nom ,categories.nom ,modules.annee FROM enseignants INNER JOIN modules on enseignants.eid =modules.eid INNER JOIN categories on modules.cid=categories.cid WHERE modules.annee=?') ; 
    $reqeute->execute(array($annee)); 
    return $reqeute ; 
}
function list_affect_modules($mid) {
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT *  FROM  modules  WHERE mid = ?') ; 
    $reqeute->execute(array($mid)); 
    return $reqeute ; 
}
function ajout_modules($intitule,$code,$eid,$cid,$annee){

    $db=dbConnect() ; 
    $reqeute=$db->prepare('INSERT INTO modules(intitule,code,eid,cid,annee) VALUES (?,?,?,?,?) ') ;
    $reqeute->execute(array($intitule,$code,$eid,$cid,$annee));

}

function sup_modules($mid,$annee){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('DELETE FROM modules WHERE mid= ? AND annee= ? ') ;   
    $reqeute->execute(array($mid,$annee)); 
}

function modif_modules_intitule($intitule,$mid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE modules SET intitule= ?  WHERE mid = ? ') ;
    $reqeute->execute(array($intitule,$mid));
}
function modif_modules_code ($code,$mid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE modules SET code= ?  WHERE mid = ? ') ;
    $reqeute->execute(array($code,$mid));
}
function modif_modules_eid($eid,$mid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE modules SET eid= ?  WHERE mid = ? ') ;
    $reqeute->execute(array($eid,$mid));
}
function modif_modules_cid($cid,$mid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE modules SET cid= ?  WHERE mid = ? ') ;
    $reqeute->execute(array($cid,$mid));
}

/*GROUPES*/
function list_groupes($annee) {
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT gid, modules.intitule, groupes.nom as GroupeNom, groupes.annee, gtypes.nom FROM modules INNER JOIN groupes on modules.mid=groupes.mid INNER JOIN gtypes on groupes.gtid=gtypes.gtid WHERE groupes.annee = ? ') ; 
    $reqeute->execute(array($annee));  
    return $reqeute ;  
}


function sup_groupes($gid,$annee){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('DELETE FROM groupes WHERE gid= ? AND annee= ? ') ;
    $reqeute->execute(array($gid,$annee)); 
}

function ajout_groupes($mid,$nom,$annee,$gtid){

    $db=dbConnect() ; 
    $reqeute=$db->prepare('INSERT INTO groupes(mid,nom,annee,gtid) VALUES (?,?,?,?) ') ;
    $reqeute->execute(array($mid,$nom,$annee,$gtid));

}

function modif_groupes_mid ($mid,$gid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE groupes SET mid= ?  WHERE gid = ? ') ;
    $reqeute->execute(array($mid,$gid));
}
function modif_groupes_nom($nom,$gid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE groupes SET nom= ?  WHERE gid = ? ') ;
    $reqeute->execute(array($nom,$gid));
}
function modif_groupes_gtid($gtid,$gid,$annee){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE groupes SET gtid= ?  WHERE gid = ? AND groupes.annee=?') ;
    $reqeute->execute(array($gtid,$gid,$annee));
}

function list_affect_groupe($mid,$anne){
   $db=dbConnect() ; 
   $reqeute=$db->prepare('SELECT *  FROM  groupes  WHERE mid = ? AND annee= ? ') ; 
   $reqeute->execute(array($mid,$anne)); 
   return $reqeute ; 

}
function list_affect_groupe_1($gid,$anne){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT *  FROM  groupes  WHERE gid = ? AND annee= ? ') ; 
    $reqeute->execute(array($gid,$anne)); 
    return $reqeute ; 

}

/*GTYPES*/
function list_gtypes() {
    $db=dbConnect() ; 
    $reqeute=$db->query('SELECT * FROM gtypes ') ;  
    return $reqeute ; 

}

function sup_gtypes($gtid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('DELETE FROM gtypes WHERE gtid=? ') ;
    $reqeute->execute(array($gtid));
}

function ajout_gtypes($nom,$nbh,$coeff){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('INSERT INTO gtypes(nom,nbh,coeff) VALUES (?,?,?) ') ;
    $reqeute->execute(array($nom,$nbh,$coeff));

}

function modif_gtypes_nom ($nom,$gtid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE gtypes SET nom= ?  WHERE gtid = ? ') ;
    $reqeute->execute(array($nom,$gtid));
}
function modif_gtypes_nbh($nbh,$gtid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE gtypes SET nbh= ?  WHERE gtid = ? ') ;
    $reqeute->execute(array($nbh,$gtid));
}
function modif_gtypes_coeff($coeff,$gtid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE gtypes SET coeff= ?  WHERE gtid = ? ') ;
    $reqeute->execute(array($coeff,$gtid));
}

//AUTRES:
function list_categorie(){
    $db=dbConnect() ; 
    $reqeute=$db->query('SELECT *  FROM categories  ') ;  
    return $reqeute ; 

}
//affectation 
function list_affectations($annee){    
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT affectations.eid, affectations.gid, enseignants.nom as nomensg, enseignants.prenom , groupes.nom , nbh FROM affectations INNER JOIN enseignants 
        ON enseignants.eid = affectations.eid INNER JOIN groupes 
        ON groupes.gid = affectations.gid WHERE groupes.annee=? '); 
    $reqeute->execute(array($annee));  
    return $reqeute ;  
}

function list_nbh($gid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT gtypes.nbh FROM gtypes INNER JOIN groupes on groupes.gtid=gtypes.gtid WHERE groupes.gid = ?') ;  
    $reqeute->execute(array($gid));
    return $reqeute ; 
}

function affect_groupes ($eid,$gid,$nbh){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('INSERT INTO affectations(eid,gid,nbh) VALUES (? ,? ,?)   ' ) ;
    $reqeute->execute(array($eid,$gid,$nbh)); 
}

function sup_affectations($eid,$gid,$nbh){
   $db=dbConnect() ; 
   $reqeute=$db->prepare('DELETE FROM affectations WHERE eid =? AND gid=? AND nbh=?') ;   
   $reqeute->execute(array($eid,$gid,$nbh));

}

function modif_affectations_nbh ($nbh,$eid,$gid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('UPDATE affectations SET nbh= ?  WHERE eid = ? AND gid=? ') ;
    $reqeute->execute(array($nbh,$eid,$gid));

}
//etypes
function list_etypes() {
    $db=dbConnect() ; 
    $reqeute=$db->query('SELECT * FROM etypes ') ;  
    return $reqeute ; 
}
//tableau de bord enseignant 
function nom_enseignants ($eid){
   $db=dbConnect() ; 
   $reqeute=$db->prepare('SELECT nom ,prenom FROM enseignants WHERE eid=?') ;
   $reqeute->execute(array($eid));
   return $reqeute ; 
}

function list_groupe_par_module_pour_chaque_eng($eid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT enseignants.nom as Nom , enseignants.prenom as Prenom , modules.intitule ,groupes.nom ,affectations.nbh as nbh ,(affectations.nbh)*gtypes.coeff as eqtd,(gtypes.nbh)-(affectations.nbh) as nbh_non_effect FROM enseignants JOIN affectations ON enseignants.eid=affectations.eid join groupes ON groupes.gid=affectations.gid JOIN modules on groupes.mid=modules.mid JOIN gtypes on gtypes.gtid=groupes.gtid WHERE enseignants.eid=?') ;
    $reqeute->execute(array($eid));
    return $reqeute ; 
}
//tableau de bord groupes
function list_groupes_ensg($gid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT groupes.nom,groupes.gid,enseignants.nom AS Nom ,enseignants.prenom AS Prenom, affectations.nbh , (affectations.nbh)*gtypes.coeff AS eqtd ,(gtypes.nbh)-(affectations.nbh) AS nbrmnq ,groupes.annee FROM gtypes JOIN groupes on gtypes.gtid=groupes.gtid JOIN affectations on groupes.gid=affectations.gid JOIN enseignants on affectations.eid=enseignants.eid WHERE groupes.gid = ? ORDER by groupes.gid  ') ;
    $reqeute->execute(array($gid));
    return $reqeute ; 
}
////////////////
function nbh_groupes_mnq($gid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT groupes.gid,( gtypes.nbh) -SUM(affectations.nbh) as nbrmnq FROM affectations join groupes on affectations.gid=groupes.gid join gtypes on gtypes.gtid=groupes.gtid GROUP by groupes.gid HAVING groupes.gid=?  ') ;
    $reqeute->execute(array($gid));
    return $reqeute ; 
}
//tableau de bord modules 
function list_modules_par_groupes($mid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT gid , mid , nom FROM `groupes` where mid = ?') ;
    $reqeute->execute(array($mid));
    
    return $reqeute ; 

}

function nom_modules ($mid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT modules.intitule as intitule FROM groupes JOIN modules on groupes.mid=modules.mid WHERE groupes.mid=?') ;
    $reqeute->execute(array($mid));
    return $reqeute ; 
}


function modules_information($mid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT groupes.mid, sum(gtypes.nbh) as nbh_afaire_module ,SUM(affectations.nbh) as nbh_module,sum(gtypes.nbh)-SUM(affectations.nbh) as nbh_mnq FROM gtypes JOIN groupes on gtypes.gtid=groupes.gtid JOIN affectations on groupes.gid=affectations.gid GROUP by groupes.mid having groupes.mid=? ');
    $reqeute->execute(array($mid));
    return $reqeute ; 

}

//autres option 
function list_ensg_nbhmnq($annee){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT enseignants.nom as Nom , enseignants.prenom as Prenom , modules.intitule ,groupes.nom ,affectations.nbh as nbh ,(gtypes.nbh)-(affectations.nbh) as nbh_non_effect FROM enseignants JOIN affectations ON enseignants.eid=affectations.eid join groupes ON groupes.gid=affectations.gid JOIN modules on groupes.mid=modules.mid JOIN gtypes on gtypes.gtid=groupes.gtid WHERE (gtypes.nbh)-(affectations.nbh) <>0 AND groupes.annee=?') ;
    $reqeute->execute(array($annee)); 
    return $reqeute ; 
}

function list_modules_nbhmnq ($annee){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT modules.intitule, groupes.gid ,groupes.nom ,gtypes.nbh FROM groupes JOIN modules on modules.mid=groupes.mid join gtypes on gtypes.gtid=groupes.gtid WHERE groupes.gid <>ALL(SELECT affectations.gid FROM affectations) AND groupes.annee =?') ;
    $reqeute->execute(array($annee)); 
    return $reqeute ; 
}

function nom_groupes($gid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT groupes.nom  FROM groupes WHERE groupes.gid=?') ;
    $reqeute->execute(array($gid));
    return $reqeute ; 

}

function nbh_affect ($annee){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT SUM(affectations.nbh) as total_affect FROM affectations join groupes on groupes.gid=affectations.gid WHERE groupes.annee=?') ;
    $reqeute->execute(array($annee)); 
    return $reqeute ; 
}

function eid_ensg($annee,$uid){
    $db=dbConnect() ; 
    $reqeute=$db->prepare('SELECT eid FROM enseignants WHERE annee = ? AND uid = ?') ;
    $reqeute->execute(array($annee,$uid)); 
    return $reqeute ; 
}
