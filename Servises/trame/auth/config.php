<?php

$authTableData = [
    'table' => 'users',
    'idfield' => 'login',
    'cfield' => 'mdp',
    'uidfield' => 'uid',
    'rfield' => 'role',
];

$pathFor = [
    "login"  => "/~u21816610/Servises/trame/login.php" ,
    "logout" => "/~u21816610/Servises/trame/logout.php",
    "adduser" => "/~u21816610/Servises/trame/adduser.php",
    "root"   => "/~u21816610/Servises/trame",
];

const SKEY = '_Redirect';