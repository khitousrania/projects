<?php
$title="Authentification";
include("../trame/header.php");

?>

<div class='center' id="login_form">
    <h2>Authentifiez-vous</h2>
    <form method="post">
        <table class="center">
            <?php echo "<p class=\"error\">".($error??"")."</p>"; ?>
            <tr>
                <td><label for="inputLogin" class="control-label"></label></td>
                <td><input type="text" name="login" size="25" class="form-control" id="inputLogin" required placeholder="Votre login"
                    required value="<?= $data['login']??"" ?>"></td>
                </tr>
                <tr>
                    <td><label for="inputMDP" class="control-label"></label></td>
                    <td><input type="password" name="password" size="25" class="form-control" required id="inputMDP" placeholder="Votre mot de passe"></td>
                </tr>
            </table>
            <br>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Se connecter</button>
            </div>
        </form>
        
    </div>

    <?php
    include("../trame/footer.php"); 

