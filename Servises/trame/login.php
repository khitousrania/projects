<?php
include("auth/EtreInvite.php");

// premiere visite 
if ((empty($_POST['login']) && empty($_POST['password']))) {
    include('login_form.php');
    exit();
}

$error = "";
foreach (['login', 'password'] as $name) {
    if (empty($_POST[$name])) {
        $error = "veuillez renseigner le champ '$name' ";
    }
}

// do the next step if no errors
if (empty($error)) {
    $data['login'] = $_POST['login'];
    $data['password'] = $_POST['password'];
    if (!$auth->existIdentity($data['login'])) {
        $error =  "Le login que vous avez entré n'appartient à aucun compte";
    }
}

// s'il ne y'a pas d erreur fait cela verification mot de passe
if (empty($error)) {
    $role = $auth->authenticate($data['login'], $data['password']);
    if (!$role) {
        $error = "Votre mot de passe était incorrect. Veuillez revérifier votre mot de passe.";
    }
}

// si erreur aprés arret 
if (!empty($error)) {
    include('login_form.php');
    exit();
}

// Redirection a la bonne section 
// Essayer d'avoir un $_SESSION usage...
if (isset($_SESSION[SKEY])) {
    $uri = $_SESSION[SKEY];
    unset($_SESSION[SKEY]);
    redirect($uri);
    exit();
}
redirect($pathFor['root']);



