<?php
$title="Utilisateurs";
include("../trame/headerconnect.php");
?>
<button type="button" class="btn btn-danger" style="float: right;"><a href="../trame/home.php">Retour menu</a> </button></div>

<div class="container">
	<h2 class="center">Liste des utilisateurs: <p class="error"><?= $error??""?></p> </h2><br>

	<table class="table table-bordered">
		<thead> 
			<tr class="text-center">
				<th>  <i style='font-size:19px' class='fas'>&#xf007;</i> Uid</th>
				<th> <i style='font-size:19px' class='fas'>&#xf007;</i>  Login </th>
				<th>Role </th>	
				<th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 1</th>  
				<th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 2</th> 
				<th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 3</th>  
			</tr>
		</thead>
		<tbody>
			<?php  
			$a=1;   
			while ($data=$utilisateur->fetch()){
				?>    
				<tr class="text-center">  
					<td>  <?= htmlspecialchars($data['uid'] )?> </td>
					<td> <?= htmlspecialchars($data['login']) ?> </td>
					<td> <?=htmlspecialchars($data['role']) ?></td>
					<td><button type="button" class="btn btn-primary badge-pill"> <a href="../Vue/modf_mdp_utili.php?uid=<?= $data['uid'] ?>&anneerecherche=<?=$_GET['anneerecherche'] ?>   "><i style='font-size:24px' class='fas'>&#xf044;</i> Modification mot de passe</td>
						<td><button type="button" class="btn btn-danger badge-pill"> <a href="../Controleur/contr_supp_utili.php?uid=<?= $data['uid'] ?>&anneerecherche=<?=$_GET['anneerecherche'] ?>  "><i style="font-size:24px" class="fa">&#xf1f8;</i>Supprimer</a></td>
							<td><button type="button" class="btn btn-success badge-pill"> <a href="../Vue/modf_utili_ensg.php?uid=<?= $data['uid'] ?>&anneerecherche=<?=$_GET['anneerecherche'] ?>  "><i style='font-size:24px' class='fas'>&#xf044;</i> Modification association enseignant </a></td>
							</tr>
							<?php
							$a++;    
						}
						$utilisateur->closeCursor() ;  
						?>	
					</tbody>
				</table>
			</div><br>
			<h2 class="center"><i style="font-size:24px" class="fa">&#xf0ad;</i>Plus d'option: <button type="button" class="btn btn-info badge-pill" > <a href="../Vue/ajouter_utilisateur.php?anneerecherche=<?=$_GET['anneerecherche'] ?> "> <i style='font-size:24px' class='fas'>&#xf234;</i>Ajouter un utilisateur</a></button></h2>

			<?php
			include("../trame/footer.php");

