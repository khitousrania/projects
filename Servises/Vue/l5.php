<?php
$title="Gtypes";
include("../trame/headerconnect.php");
?>
<div ><button type="button" class="btn btn-danger" style="float: right;"><a href="../trame/home.php">Retour menu</a> </button></div>
<p class="error"><?= $error??""?></p> 
<div class="container">
<h2 class="center">Liste des gtypes: </h2><br>
    <table class="table table-bordered">
    <thead>
    <tr class="text-center">
      <th>Gtid</th>
      <th><i style="font-size:24px" class="fa">&#xf0c0;</i>Groupes</th>
      <th><i style="font-size:24px" class="fa">&#xf252;</i>Nombre d'heure(s)</th>
      <th><i style='font-size:24px' class='fas'>&#xf1ec;</i>Coefficient</th>
      <th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 1</th>
      <th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 2</th>
    </tr> 
    </thead>
    <tbody> 

    <?php
    $e=1;
    while ($data=$gtypes->fetch()){
      ?>    
        <tr class="text-center">
          <td> <?= htmlspecialchars($data['gtid']) ?> </td>
          <td> <?= htmlspecialchars($data['nom'] )?> </td>
          <td> <?= htmlspecialchars($data['nbh'] )?> </td>
          <td> <?= htmlspecialchars($data['coeff'] )?> </td>
          <td><button type="button" class="btn btn-danger badge-pill"> <a href="../Controleur/contr_supp_gtypes.php?gtid=<?= $data['gtid'] ?>"><i style="font-size:24px" class="fa">&#xf1f8;</i>Supprimer</a></td>
          <td><button type="button" class="btn btn-primary badge-pill"> <a href="../Vue/modf_gtypes.php?gtid=<?=$data['gtid']?>"><i style='font-size:24px' class='fas'>&#xf044;</i> Modifier</a></td>
        </tr>
      
      <?php
      $e++;    
    }
    $gtypes->closeCursor() ; 
    ?>
    </tbody>
  </table>
</div><br> 
<h2 class="center"><i style="font-size:24px" class="fa">&#xf0ad;</i>Plus d'option: <button type="button" class="btn btn-info badge-pill" > <a href="../Vue/ajouter_gtypes.php"><i class="fa fa-plus-circle" style="font-size:24px"></i>Ajouter un gtypes </a></button>  </h2>

<?php

include("../trame/footer.php");
