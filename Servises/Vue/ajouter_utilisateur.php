<?php
$title="Ajouter un utilisateur";
include("../trame/header.php");
?>

<h2 class="center"><i style='font-size:24px' class='fas'>&#xf234;</i>Ajouter un utilisateur  :</h2>
<div class="center">
    <form method="post" action="../Controleur/contr_ajout.php?anneerecherche=<?=$_GET['anneerecherche'] ?> ">
        <p class="error"><?= $error??""?></p>  
        <table>
            <tr>
                <td><label for="inputLogin" class="label"></label></td>
                <td><input type="text" name="login" class="input" id="inputLogin" placeholder="Login utilisateur" required value="<?= $data['login']??""?>"></td>
            </tr>
            <tr>
                <td><label for="inputMDP" class="label"></label></td>
                <td><input type="password" name="mdp" class="input" id="inputMDP" placeholder="Mot de passe" required value=""></td>
            </tr>
            <tr>
                <td><label for="inputMDP2" class="label"></label></td>
                <td><input type="password" name="mdp2" class="input" id="inputMDP" placeholder="Confirmer le mot de passe" required value=""></td>
            </tr>
        </table>
        <br>
        <div class="form-group">
            <button type="submit"  class="btn btn-primary badge-pill">S'inscrire</button>
        </div>
    </form>
</div>
<?php

include("../trame/footer.php");
