<?php
$title="Affectation groupe ";
include("../trame/headerconnect.php");
require("../Modele/m1.php");
?>
<p class="error"><?= $error??""?></p>
<div class="center">

  <h1>Affectation du  groupe: <?= $_GET['annee'] ?> </h1>
  
  <form method="post" action="../Vue/affect_groupes_1.php?annee=<?= $_GET['annee'] ?>">
    <table>
     <tr>
       <td>
         <div class="form-group">
          <label for="enseignants" class="label"> Enseignants: </label>
        </div>
      </td>

      <td>  
       <div class="form-group">
         <select name = "enseignants" class="form-control" id="enseignants">
          <?php
          $enseignants=list_enseignants($_GET['annee']) ; 
          while($data=$enseignants->fetch()){
            ?>
            <option value="<?= $data['eid'] ?> "> <?= $data['nom'] ?> <?= $data['prenom'] ?> </option> 
            <?php 
          }
          $enseignants->closeCursor() ; 
          ?> 
        </select> 
      </td>
    </td>
  </tr>
  <tr>
   <td><label for="modules"  class="label"> Choisir un modules : </label></td>
   <td>  
     <select name = "modules" class="form-control" id="modules">
      
       <?php
                                          //$groupes=list_groupes($_GET['annee']) ;
       $modules=list_modules($_GET['annee']) ; 
       while($data=$modules->fetch()){
        ?>
        <option value="<?= $data['mid'] ?>" > <?= $data['intitule'] ?> </option> 
        <?php 
      }
      $modules->closeCursor() ; 
      ?> 
    </select> 
  </td>
</tr>

</table>
<div class="form-group">
  <button type="submit"  class="btn btn-primary">Suivant </button>
</div>
</form>
</div>
<?php

include("../trame/footer.php");
