
<?php
$title="Ajout de l'enseignants";
include("../trame/header.php");
require("../Modele/m1.php");
?>

<h2 class="center"><i style='font-size:24px' class='fas'>&#xf234;</i>Ajouter un enseignant en <?= $_GET['annee'] ?> </h2>
<div class="center">
  <p class="error"><?= $error??""?></p>
  <form method="post" action="../Controleur/contr_ajout.php?anne=<?= $_GET['annee'] ?>">
    <table>
     <tr>
      <td><label for="inputNom" class="label"></label></td>
      <td><input type="text" name="nom" class="input" id="inputNom" placeholder="Nom enseignant" required value="">
      </td>
    </tr>
    <tr>
     <td> <label for="inputPrenom" class="label"></label></td>
     <td>  <input type="text" name="prenom" class="input" id="inputPrenom" placeholder="Prenom enseignant" required aria-required="true" value=""></td>
   </tr> 
   <tr>
    <td><label for="email" class="label"></label></td>
    <td><input type="email" name="email" class="input" id="inputEmail" placeholder="Email enseignant" required value=""></td>
  </tr>
  <tr>
    <td><label for="tel" class="control-label"></label></td>
    <td><input type="tel" name="tel" class="input" id="inputtel" placeholder="Telephone enseignant"  pattern="[0-9]{10}" required value=""></td>
  </tr>
  <tr>
    <td> <label for="etid" class="label" ></label></td>
    <td>
     <p>
       <select name="etid" >
         <?php
         $etypes=list_etypes() ; 
         while($data=$etypes->fetch()){
          ?>
          <option value="<?= $data['etid'] ?> "> <?= $data['nom'] ?> </option> 
          <?php 
        }
        $etypes->closeCursor() ; 
        ?> 
        
      </select>
    </p>
  </td>
</tr>
</table>
<div class="form-group">
  <button type="submit" class="btn btn-primary badge-pill">S'inscrire</button>
</div>
</div>
</form>
</div>
<?php

include("../trame/footer.php");
