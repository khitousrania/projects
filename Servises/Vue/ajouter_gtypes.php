<?php
$title="Ajouter un gtype";
include("../trame/header.php");
?>

<h2 class="center"><i class="fa fa-plus-circle" style="font-size:24px"></i>Ajouter un gtype  </h2>
<div class="center">
    <p class="error"><?= $error??""?></p>
    <form method="post" action="../Controleur/contr_ajout.php">
        <table>
            <tr>
                <td><label for="inputNom" class="label">Nom</label></td>
                <td><input type="text" name="nom" class="input" id="inputNom" placeholder="nom" required value=""?> </td>
            </tr>
            <tr>
                <td><label for="inputNBH" class="label">Nombre d'heure(s)</label></td>
                <td><input type="number" name="nbh" class="input" id="inputNBH" placeholder="Nombre d'heure(s)" required value=""></td>
            </tr>
            <tr>
                <td><label for="inputCoeff" class="label">Coefficient </label></td>
                <td><input type="number" step="any" name="coeff" class="input" id="inputCoeff" placeholder="Coefficient"  value=""></td>
            </tr>
        </table><br>
        <div class="form-group">
            <button type="submit" class="btn btn-primary badge-pill">Ajouter</button>
        </div>
    </div>
</form>
</div>
<?php

include("../trame/footer.php");
