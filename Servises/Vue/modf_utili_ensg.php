
<?php
$title="Modification association enseignant";
require("../trame/auth/EtreAuthentifie.php");
include("../trame/headerconnect.php");
include("../Modele/m1.php");
?>

<p class="error"><?= $error??""?></p>

<div class="center">

  <h1>Modification association enseignant par annee</h1>

  <form method="post" 
  action="../Controleur/contr_utili_ensg.php?uid=<?= $_GET['uid'] ?>&anneerecherche=<?=$_GET['anneerecherche'] ?>">  
  <table>
    <tr>
      <td><label for="enseignants" class="label"> Associer a l'enseignant : </label></td>
      <td>  
        <select name = "enseignants" id="enseignants">
          <?php
          $enseignants=list_enseignants($_GET['anneerecherche']) ; 
          while($data=$enseignants->fetch()){
            ?>
            <option value="<?= $data['eid'] ?> "> <?= $data['nom'] ?> <?= $data['prenom'] ?>   </option> 
            <?php 
          }
          $enseignants->closeCursor() ; 
          ?> 
        </select> 
      </td>
    </tr>
    
  </table>
  
  <div class="form-group">
    <button type="submit" class="button">Modifier</button>
  </div>
  
</form>
</div>

<?php

include("../trame/footer.php");
