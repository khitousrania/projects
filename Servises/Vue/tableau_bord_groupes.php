<?php
$title="Tableau de bord groupes ";

include("../Vue/tableaux_bort.php");
?>
<h2 class="center"> <?php 
$nom_groupe=nom_groupes($_GET['gid']);
$data3=$nom_groupe->fetch();
?><i style="font-size:24px" class="fa">&#xf0c0;</i>
<?=("Le groupe: ".$data3['nom']);
?></h2><br>

<div class="container">
  <h2 class="center">Informations sur le groupe: </h2><br>
  <table class="table table-bordered">
    <thead>
      <tr class="text-center">
        <th><i style='font-size:24px' class='fas'>&#xf51c;</i>Nom  enseignant</th>
        <th><i style='font-size:24px' class='fas'>&#xf51c;</i> Prénom  enseignant</th>
        <th><i class='far fa-calendar-check' style='font-size:24px'></i> Nombre d'heure(s) en nbh</th>
        <th> <i style='font-size:24px' class='fas'>&#xf1ec;</i>Nombre d'heure(s) en eqtd </th>
        <th><i class='far fa-calendar-minus' style='font-size:24px'></i>nbh manquante(s)  </th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $b=1;
      $nbh_groupes_mnq=nbh_groupes_mnq($_GET['gid']);
      $data1=$nbh_groupes_mnq->fetch();
      $list_groupes=list_groupes_ensg($_GET['gid']) ;
      $nbh=0;
      $eqtd=0;
      $nbrmnq=0;
      while($data=$list_groupes->fetch())
      {
        $nbh+=$data['nbh'];
        $eqtd+=$data['eqtd'];
        $nbrmnq+=$data1['nbrmnq'];
        ?>  
        <tr class="text-center">
          <td class="text-center"> <?= htmlspecialchars($data['Nom']) ?> </td>
          <td  class="text-center"> <?= htmlspecialchars($data['Prenom']) ?> </td>
          <td class="text-center"> <?= htmlspecialchars($data['nbh']) ?> </td>
          <td class="text-center"> <?= htmlspecialchars($data['eqtd']) ?> </td>
          <td class="text-center"> <?= htmlspecialchars($data1['nbrmnq']) ?> </td>
        </tr>
        <?php
      }
      $list_groupes->closeCursor() ; 
      ?> 
    </tbody>
  </table>
</div><br>
<!--table 2 -->
<div class="container">
  <h2 class="center"><i class='far fa-clock' style='font-size:24px'></i>Informations sur les heures :</h2></br>
  <table class="table table-bordered">
    <thead>
     <tr class="text-center">
      <th><i class='far fa-calendar-check' style='font-size:24px'></i></i>Total nombre d'heure(s) en nbh</th>
      <th><i class='far fa-calendar-check' style='font-size:24px'></i></i>Total nombre d'heure(s) en eqtd</th>
      <th><i class='far fa-calendar-minus' style='font-size:24px'></i>Total nombre d'heure(s) manquantes  </th>
    </tr>
  </thead>
  <tbody>
    <tr class="text-center">
      <td class="text-center"><?=htmlspecialchars($nbh)?> </td>
      <td class="text-center"><?=htmlspecialchars($eqtd)?> </td>
      <td class="text-center"><?=htmlspecialchars($nbrmnq)?> </td>
    </tr>
  </tbody>
</table>
</div>
<?php

include("../trame/footer.php");








