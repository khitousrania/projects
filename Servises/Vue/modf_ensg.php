<?php
$title="Modification informations enseignants";
include("../trame/headerconnect.php");
?>
<h2 class="center"><i style='font-size:36px' class='fas'>&#xf044;</i>Modification information enseignant </h2>
<div class="center">
  <form method="post" action="../Controleur/contr_modif.php?anne=<?= $_GET['annee'] ?>&eid=<?= $_GET['eid'] ?>">
    <p class="error"><?= $error??""?></p>  
    <table>
      <tr>
        <td><label for="inputNom" class="label"></label></td>
        <td><input type="text" name="nom" class="input" id="inputNom" placeholder="Nom enseignant"  value="">
        </td>
      </tr>
      <tr>
       <td> <label for="inputPrenom" class="label"></label></td>
       <td>  <input type="text" name="Prenom " class="input" id="inputPrenom" placeholder="Prenom enseignant"  value=""></td>
     </tr> 
     <tr>
      <td><label for="email" class="label"></label></td>
      <td><input type="email" name="email" class="input" id="inputEmail" placeholder="email"  value=""></td>
    </tr>
    <tr>
      <td><label for="tel" class="control-label"></label></td>
      <td><input type="tel" name="tel" class="input" id="inputtel" placeholder="Telephone" pattern="[0-9]{10}"    value=""></td>
    </tr>
    <tr>
      <td>  <label for="etid" class="label"></label></td>
      <td>
       <p>
         <select name="etid" >
           <option value="1">MCF</option>
           <option value="2">PF</option>
           <option value="3">ATER</option>
           <option value="4">VAC1</option> 
         </select>
       </p>
     </td>
   </tr>
 </table>
 <div class="form-group">
  <button type="submit" class="btn btn-primary badge-pill">Modifier</button>
</div>
</div>
</form>
</div>
<?php

include("../trame/footer.php");
