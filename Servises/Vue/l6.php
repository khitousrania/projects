<?php
$title="Affectation";
include("../trame/headerconnect.php");
?>
<p class="error"><?= $error??""?></p>
<div ><button type="button" class="btn btn-danger" style="float: right;"><a href="../trame/home.php">Retour menu</a> </button></div>
<?php 
$annee =date('Y');
?>    
<div class="container">
	<h2 class="center">Affectations groupes : </h2><br>
	<table class="table table-bordered">
		<thead> 
			<tr class="text-center">
				<th><i style='font-size:24px' class='fas'>&#xf51c;</i></i>Nom enseignant</th>
				<th><i style='font-size:24px' class='fas'>&#xf51c;</i></i>Prenom enseignant</th>
				<th><i style="font-size:24px" class="fa">&#xf0c0;</i>Groupe</th>
				<th><i style="font-size:24px" class="fa">&#xf252;</i>Nombre d'heure(s)</th>
				<th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 1</th>
				<th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 2</th>
			</tr>  		
			<?php 

			$b=1;
			while($data=$affectations->fetch())
			{
				?>	
				
				<tr class="text-center">
					<td><?= htmlspecialchars($data['nomensg']) ?></td>
					<td> <?= htmlspecialchars($data['prenom']) ?> </td>
					<td> <?= htmlspecialchars($data['nom']) ?> </td>
					<td> <?= htmlspecialchars($data['nbh'] )?>  </td>
					<td><button type="button" class="btn btn-danger badge-pill"> <a href="../Controleur/contr_supp_affect.php?eid=<?= $data['eid'] ?>&gid=<?= $data['gid'] ?>&nbh=<?= $data['nbh'] ?>&annee=<?= $_GET['anneerecherche'] ?>"><i style="font-size:24px" class="fa">&#xf1f8;</i>Supprimer</a></td>
						<td><button type="button" class="btn btn-primary badge-pill"> <a href="../Vue/modf_affect_nbh.php?nbh=<?= $data['nbh'] ?>&eid=<?= $data['eid'] ?>&gid=<?= $data['gid'] ?>&annee=<?= $_GET['anneerecherche']?>"><i style='font-size:24px' class='fas'>&#xf044;</i> Modifcation nombre d'heure(s)</a></td>
							
						</tr>

						<?php
						$b++;

					}
					$affectations->closeCursor() ; 
					?> 
				</tbody>
			</table>
		</div><br>
		<h2 class="center"><i style="font-size:24px" class="fa">&#xf0ad;</i>Plus d'option: <button type="button" class="btn btn-info badge-pill" > <a href="../Vue/affect_groupes.php?annee=<?= $_GET['anneerecherche'] ?>"><i style="font-size:24px" class="fa">&#xf040;</i>Affectation </a></button>  </h2>

		
		<?php

		include("../trame/footer.php");



