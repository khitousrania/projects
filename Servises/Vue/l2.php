
<?php
$title="Enseignants ";
include("../trame/headerconnect.php");
?>
<p class="error"><?= $error??""?></p>
<button type="button" class="btn btn-danger" style="float: right;"><a href="../trame/home.php">Retour menu</a> </button>
<?php 
$annee =date('Y'); 
?>	  
<div class="container">
	<h2 class="center">Liste des enseignants: </h2><br>
	<table class="table table-bordered">
		<thead> 
			<tr class="text-center">
				<th><i style='font-size:24px' class='fas'>&#xf51c;</i>Nom</th>
				<th><i style='font-size:24px' class='fas'>&#xf51c;</i>Prenom</th>
				<th><i style="font-size:24px" class="fa">&#xf003;</i>Email</th>  
				<th> <i class="fa fa-phone"></i>Téléphone</th> 
				<th><i style="font-size:24px" class="fa">&#xf073;</i>Année</th>  
				<th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 1</th>  
				<th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 2</th>  
			</tr> 
		</thead>
		<tbody>
			<?php 
			$b=1;
			while($data=$enseignants->fetch())
			{
				?>	

				<tr class="text-center">
					<td><?= htmlspecialchars($data['nom'] )?></td>
					<td> <?= htmlspecialchars($data['prenom'] )?> </td>
					<td> <?=htmlspecialchars( $data['email']) ?> </td>
					<td> <?= htmlspecialchars($data['tel']) ?></td>   
					<td> <?= htmlspecialchars($data['annee']) ?> </td>
					<td><button type="button" class="btn btn-danger badge-pill"> <a href="../Controleur/contr_supp_ensg.php?eid=<?= $data['eid'] ?>&annee=<?= $data['annee'] ?>"><i style="font-size:24px" class="fa">&#xf1f8;</i>Supprimer</a></td>
						<td><button type="button" class="btn btn-primary badge-pill"> <a href="../Vue/modf_ensg.php?eid=<?= $data['eid'] ?>&annee=<?= $data['annee'] ?>"><i style='font-size:24px' class='fas'>&#xf044;</i> Modifier</a></td>
						</tr>
						<?php
						$b++;
						$annee=$data['annee'] ; 
					}
					$enseignants->closeCursor() ; 
					?> 
				</tbody>
			</table>
		</div><br>	
		<h2 class="center"><i style="font-size:24px" class="fa">&#xf0ad;</i>Plus d'option: <button type="button" class="btn btn-info badge-pill" > <a href="../Vue/ajouter_ensg.php?annee=<?= $annee ?>"> <i style='font-size:24px' class='fas'>&#xf234;</i>Ajouter un enseignant </a></button>	</h2>

	</div>
	



	
	<?php

	include("../trame/footer.php");

