<?php
$title="Modules";
include("../trame/headerconnect.php");
?>
<p class="error"><?= $error??""?></p>
<div ><button type="button" class="btn btn-danger" style="float: right;"><a href="../trame/home.php">Retour menu</a> </button></div> 
<?php 
$annee =date('Y'); 
?>    
<div class="container">
    <h2 class="center">Liste des modules : </h2><br>
    <table class="table table-bordered">
        <thead> 
            <tr class="text-center">
                <th>Code </th>
                <th><i style='font-size:24px' class='fas'>&#xf518;</i></i>Module</th>
                <th><i style='font-size:24px' class='fas'>&#xf51c;</i></i>Nom de lé enseignant</th>  
                <th><i style='font-size:24px' class='fas'>&#xf51c;</i>Prenom de l'enseignant</th> 
                <th><i style='font-size:24px' class='fas'>&#xf501;</i>Catégorie</th>  
                <th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 1</th>  
                <th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 2</th>       
            </tr> 
        </thead>
        <tbody> 
            <?php
            $c=1;
            while ($data=$modules->fetch()){
                ?>    
                <tr class="text-center">
                    <td> <?= htmlspecialchars($data['code']) ?> </td>
                    <td> <?= htmlspecialchars($data['intitule']) ?> </td>
                    <td> <?= htmlspecialchars($data['Nom'] )?>  </td>
                    <td> <?= htmlspecialchars($data['Prenom']) ?></td>   
                    <td> <?= htmlspecialchars($data['nom'] )?> </td>
                    <td><button type="button" class="btn btn-danger badge-pill"> <a href="../Controleur/contr_supp_modules.php?mid=<?=$data['mid']?>&annee=<?= $data['annee'] ?>"><i style="font-size:24px" class="fa">&#xf1f8;</i>Supprimer</a></td>
                        <td><button type="button" class="btn btn-primary badge-pill"> <a href="../Vue/modf_modules.php?mid=<?=$data['mid']?>&annee=<?= $data['annee'] ?>"><i style='font-size:24px' class='fas'>&#xf044;</i> Modifier</a></td>
                        </tr>
                        
                        <?php
                        $c++;  
                        $annee=$data['annee'] ;   
                    }
                    $modules->closeCursor() ; 
                    ?>
                </tbody>
            </table>
        </div>  
        <h2 class="center"><i style="font-size:24px" class="fa">&#xf0ad;</i>Plus d'option: <button type="button" class="btn btn-info badge-pill" > <a href="../Vue/ajouter_modules.php?annee=<?= $annee ?>"> <i class="fa fa-plus-circle" style="font-size:24px"></i>Ajouter un module </a></button>  </h2>

        <?php

        include("../trame/footer.php");

