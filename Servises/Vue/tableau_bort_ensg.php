<?php
$title="Tableau de bord enseignants";

include("../Vue/tableaux_bort.php");
?>
<h2 class="center"> <?php 
$nom_ensg=nom_enseignants($_GET['eid']);
$data2=$nom_ensg->fetch();
?>
<i style='font-size:24px' class='fas'>&#xf51c;</i><?=("L'enseignant: ".$data2['nom']." ".$data2['prenom']);
?></h2><br>

<div class="container">
  <h2 class="center">Informations sur l'enseignant: </h2><br>
  <table class="table table-bordered">
    <thead>
      <tr class="text-center">
        <th> <i style='font-size:24px' class='fas'>&#xf518;</i>Module </th>
        <th><i style="font-size:24px" class="fa">&#xf0c0;</i>Groupe </th>
        <th><i class='far fa-calendar-check' style='font-size:24px'></i>Nombre d'heure(s) en nbh</th>
        <th> <i style='font-size:24px' class='fas'>&#xf1ec;</i>Nombre d'heure(s) en eqtd </th>
        <th><i class='far fa-calendar-minus' style='font-size:24px'></i> nbh non effectué(s)</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $b=1;
      $enseignants=list_groupe_par_module_pour_chaque_eng($_GET['eid']) ;
      $nbh=0;
      $eqtd=0;
      $nbh_non_effect=0;
      while($data=$enseignants->fetch())
      {
        $nbh+=$data['nbh'];
        $eqtd+=$data['eqtd'];
        $nbh_non_effect+=$data['nbh_non_effect'];

        ?>  
        <tr class="text-center">
          <td> <?= htmlspecialchars($data['intitule']) ?>  </td>
          <td> <?= htmlspecialchars($data['nom']) ?>  </td>
          <td> <?= htmlspecialchars($data['nbh']) ?> </td>
          <td> <?= htmlspecialchars($data['eqtd']) ?> </td>
          <td> <?= htmlspecialchars($data['nbh_non_effect']) ?> </td>

        </tr>
        <?php
      }
      $enseignants->closeCursor() ; 
      ?> 
    </tbody>
  </table>
</div><br>
<!--tableau 2-->
<div class="container">
  <h2 class="center"><i class='far fa-clock' style='font-size:24px'></i>Informations sur ses heures :</h2></br>
  <table class="table table-bordered">
    <thead>
     <tr class="text-center">
      <th><i class='far fa-calendar-check' style='font-size:24px'></i></i>Total nombre d'heure(s)</th>
      <th><i style='font-size:24px' class='fas'>&#xf1ec;</i>Total des eqtd  </th>
      <th><i class='far fa-calendar-minus' style='font-size:24px'></i>Total nombre d'heure(s) non effectuées  </th>
    </tr>
  </thead>
  <tbody>
    <tr class="text-center">
      <td><?=htmlspecialchars($nbh)?> </td>
      <td><?=htmlspecialchars($eqtd)?> </td>
      <td><?=htmlspecialchars($nbh_non_effect)?> </td>
    </tr>
  </tbody>
</table>
</div>
<?php

include("../trame/footer.php");


