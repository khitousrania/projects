<?php
$title="Modificaition des information module";
require("../Modele/m1.php");
include("../trame/headerconnect.php");
?>
<h2 class="center"><i style='font-size:36px' class='fas'>&#xf044;</i>Modificaition des information module <?= $_GET['annee'] ?></h2>
<div class="center">
  <form method="post" action="../Controleur/contr_modif.php?anne=<?= $_GET['annee'] ?>&mid=<?= $_GET['mid'] ?>">
    <p class="error"><?= $error??""?></p>  
    <table>
     <tr>
      <td><label for="inputNom" class="label">Module </label></td>
      <td><input type="text" name="intitule" class="input" id="inputNom" placeholder="Intitule"  value="">
      </td>
    </tr>
    <tr>
     <td> <label for="inputCode" class="label">Code module </label></td>
     <td>  <input type="text" name="code" class="input" id="inputCode" placeholder="2SX**R*0"  value="" placeholder="Code" value=""></td>
   </tr>   
   <tr>
    <td><label for="enseignants" class="label"> Enseignants: </label></td>
    <td>  
      <select name = "enseignants" id="enseignants">
        <?php
        $enseignants=list_enseignants($_GET['annee']) ; 
        while($data=$enseignants->fetch()){
          ?>
          <option value="<?= $data['eid'] ?> "> <?= $data['nom'] ?> <?= $data['prenom'] ?>  </option> 
          <?php 
        }
        $enseignants->closeCursor() ; 
        ?> 
      </select> 
    </td>
  </tr>
  <tr>
    <td><label for="category" class="control-label"> Categorie : </label></td>
    <td>  
      <select name = "category" id="category">
        <?php
        $category=list_categorie() ; 
        while($data=$category->fetch()){
          ?>
          <option value="<?= $data['cid'] ?> "> <?= $data['nom'] ?>  </option> 
          <?php 
        }
        $category->closeCursor() ; 
        ?> 

      </select> 

    </td>
  </tr>
</table><br>
<div class="form-group">
  <button type="submit" class="btn btn-primary badge-pill">Modifier</button>
</div>
</div>
</form>
</div>
<?php

include("../trame/footer.php");
