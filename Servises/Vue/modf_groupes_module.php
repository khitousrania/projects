<?php
$title="Modification association enseignant";
require("../trame/auth/EtreAuthentifie.php");
include("../trame/headerconnect.php");
include("../Modele/m1.php");
?>
<h2 class="center"><i style='font-size:36px' class='fas'>&#xf044;</i>Modification  : </h2>
<div class="center">
 <form method="post" action="../Controleur/contr_groupes_modules.php?gid=<?= $_GET['gid'] ?>&annee=<?=$_GET['annee']?>">
  <p class="error"><?= $error??""?></p>  
  <table>
    <tr>
      <td><label for="modules" class="label"> Module : </label></td>
      <td>  
        <select name = "modules" id="modules">
          <?php
          $modules=list_modules($_GET['annee']) ; 
          while($data=$modules->fetch()){
            ?>
            <option value="<?= $data['mid'] ?> "> <?= $data['intitule'] ?>   </option> 
            <?php 
          }
          $modules->closeCursor() ; 
          ?> 
        </select> 
      </td>
    </tr>
    
  </table>
  
  <div class="form-group">
    <button type="submit" class="btn btn-primary badge-pill">Modifier </button>
  </div>
</div>
</form>
</div>

<?php

include("../trame/footer.php");
