<?php
$title="Tableau de bord modules";
include("../Vue/tableaux_bort.php");
?>
<h2 class="center"><?php 
$nom_module=nom_modules($_GET['mid']);
$data2=$nom_module->fetch();
?><i style='font-size:24px' class='fas'>&#xf518;</i>
<?=("Module: ".$data2['intitule']);?>
</h2><br>


<div class="container">
  <h2 class="center">Informations sur les groupes du module: </h2><br>
  <table class="table table-bordered">
    <thead>
      <tr class="text-center">
        <th><i style="font-size:24px" class="fa">&#xf0c0;</i> Groupe </th>
        <th><i style='font-size:24px' class='fas'>&#xf51c;</i> Nom enseignant </th>
        <th><i style='font-size:24px' class='fas'>&#xf51c;</i>Prenom de l'enseignant</th>
        <th><i class='far fa-calendar-check' style='font-size:24px'></i>Nombre d'heure(s) en nbh</th>
        <th> <i style='font-size:24px' class='fas'>&#xf1ec;</i>Nombre d'heure(s) en eqtd </th>
        <th><i class='far fa-calendar-minus' style='font-size:24px'></i>Nombre d'heure(s) manquante(s) </th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $eqtd=0;
      $modules=list_modules_par_groupes($_GET['mid']) ;
      while($data1=$modules->fetch())
      {
        
       $list_groupes=list_groupes_ensg($data1['gid']) ;
       while($data=$list_groupes->fetch()){
        $eqtd+=$data['eqtd'];
        ?>  
        <tr class="text-center">
          <td> <?= htmlspecialchars($data['nom'])   ?>  </td>
          <td> <?= htmlspecialchars($data['Nom'])   ?> </td>
          <td> <?= htmlspecialchars($data['Prenom'])?> </td>
          <td> <?= htmlspecialchars($data['nbh'])   ?> </td>
          <td> <?= htmlspecialchars($data['eqtd'])  ?> </td>
          <td> <?= htmlspecialchars($data['nbrmnq'])?> </td>
        </tr>
        <?php
      }
      $list_groupes->closeCursor() ; 
      ?>
      <?php
    }
    $modules->closeCursor() ; 
    ?>  
  </tbody>
</table>
</div><br>
<div class="container">
  <h2 class="center"><i class='far fa-clock' style='font-size:24px'></i>Informations sur les heures du module:</h2></br>
  <table class="table table-bordered">
    <thead>
      <tr class="text-center">
        <th><i class='far fa-calendar-check' style='font-size:24px'></i></i> Nombre d'heure(s) a faire</th>
        <th> <i class='far fa-calendar-check' style='font-size:24px'></i></i>Total nombre d'heure(s) faite </th>
        <th><i style='font-size:24px' class='fas'>&#xf1ec;</i>Total des eqtd  </th>
        <th> <i class='far fa-calendar-minus' style='font-size:24px'></i>Nombre d'heure(s) manquantes </th>
      </tr>
    </thead>
    <tbody>  
      <?php
      $modules_info=modules_information($_GET['mid']);
      while($data2=$modules_info->fetch())
      {
        ?>  
        <tr class="text-center">
          <td> <?= htmlspecialchars($data2['nbh_afaire_module']) ?> </td>
          <td> <?= htmlspecialchars($data2['nbh_module']) ?> </td>
          <td><?=htmlspecialchars($eqtd)?> </td>
          <td> <?= htmlspecialchars($data2['nbh_mnq']) ?> </td>
        </tr>
        <?php
      }
      $modules_info->closeCursor() ; 
      ?> 
    </tbody>
  </table>
</div>
<?php
include("../trame/footer.php");


