<?php
$title="Tableau de bord ";
require("../Modele/m1.php");
require("../trame/auth/EtreAuthentifie.php");
include("../trame/headerconnect.php");
?>
<div ><button type="button" class="btn btn-danger" style="float: right;"><a href="../trame/home.php">Retour menu</a> </button></div>
<p class="error"><?= $error??""?></p>
<h1 class="center">Tableau de bord <?=$_GET['anneerecherche']?> </h1><br>
<!--tableau 1-->
<div class="container">
  <h2 class="center"><i style='font-size:24px' class='fas'>&#xf51c;</i>Liste des enseignants avec un service non-complet</h2><br>
  <table class="table table-bordered">
    <thead>
      <tr class="text-center">
        <th> <i style='font-size:24px' class='fas'>&#xf51c;</i> Nom enseignant  </th>
        <th> <i style='font-size:24px' class='fas'>&#xf51c;</i> Prenom enseignant </th>
        <th><i style='font-size:24px' class='fas'>&#xf518;</i>Module  enseigné</th>
        <th><i style="font-size:24px" class="fa">&#xf0c0;</i>Groupe</th>
        <th><i class='far fa-calendar-check' style='font-size:24px'></i>Nombre d'heure(s) en nbh</th>
        <th> <i class='far fa-calendar-minus' style='font-size:24px'></i>Nombre d'heure(s) manquante(s) </th>
      </tr>  
    </thead>
    <tbody>
      <?php
      $enseignant=list_ensg_nbhmnq($_GET['anneerecherche']);
      while($data1=$enseignant->fetch())
      {
        ?>  
        <tr class="text-center">
          <td> <?= htmlspecialchars($data1['Nom']) ?> </td>
          <td> <?= htmlspecialchars($data1['Prenom']) ?> </td>
          <td> <?= htmlspecialchars($data1['intitule']) ?> </td>
          <td> <?= htmlspecialchars($data1['nom']) ?> </td>
          <td> <?= htmlspecialchars($data1['nbh']) ?> </td>
          <td> <?= htmlspecialchars($data1['nbh_non_effect']) ?> </td>

        </tr>
        <?php
      }
      $enseignant->closeCursor() ; 
      ?> 
    </tbody>
  </table>
</div><br>
<div class="container">
  <h2 class="center"><i style='font-size:24px' class='fas'>&#xf518;</i></i>Liste modules ayant des heures non-affectés</h2></br>
  <table class="table table-bordered">
    <thead>
      <tr class="text-center">
        <th><i style='font-size:24px' class='fas'>&#xf518;</i> Module</th>
        <th><i style="font-size:24px" class="fa">&#xf0c0;</i>Groupe correspond ayant un nbh non-affectés</th>
        <th><i class='far fa-calendar-minus' style='font-size:24px'></i>Nombre d'heure(s) non-affectés</th>
      </tr> 
    </thead>
    <tbody> 
      <?php
      $nbr_total_non_affect=0;
      $modules=list_modules_nbhmnq($_GET['anneerecherche']);
      while($data2=$modules->fetch())
      {
        $nbr_total_non_affect+=$data2['nbh'];
        ?>  
        <tr class="text-center">
          <td> <?= htmlspecialchars($data2['intitule']) ?> </td>
          <td> <?= htmlspecialchars($data2['nom']) ?> </td>
          <td> <?= htmlspecialchars($data2['nbh']) ?> </td>

        </tr>
        <?php
      }
      $modules->closeCursor() ; 
      ?> 
    </tbody>
  </table>
</div><br>

<!--tableau3-->
<div class="container">
  <h2 class="center"><i class='far fa-clock' style='font-size:24px'></i>Informations sur les heures :</h2></br>
  <table class="table table-bordered">
    <thead>
     <tr class="text-center">
      <th><i class='far fa-calendar-check' style='font-size:24px'></i>Le nombre total des heures affectées</th>
      <th><i class='far fa-calendar-minus' style='font-size:24px'></i>Le Total des heures non affectées</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $nbr_total_affect=nbh_affect($_GET['anneerecherche']);
    $data3=$nbr_total_affect->fetch();

    ?>
    <tr class="text-center">
      <td class="text-center"><?=htmlspecialchars($nbr_total_non_affect)?> </td>
      <td class="text-center"><?=htmlspecialchars($data3['total_affect'])?> </td>
    </tr>
  </tbody>
</table>
</div><br>
<?php

include("../trame/footer.php");








