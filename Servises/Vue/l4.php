<?php
$title="Groupes";
include("../trame/headerconnect.php");
?>
<p class="error"><?= $error??""?></p>
<div ><button type="button" class="btn btn-danger" style="float: right;"><a href="../trame/home.php">Retour menu</a> </button></div>
<?php 
$annee =date('Y'); 
?>    

<div class="container">
  <h2 class="center">Liste des groupes : </h2><br>
  <table class="table table-bordered">
    <thead> 
      <tr class="text-center">
        <th><i style="font-size:24px" class="fa">&#xf0c0;</i>Groupes</th>
        <th><i style='font-size:24px' class='fas'>&#xf518;</i>Module</th>
        <th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 1</th>  
        <th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 2</th> 
        <th><i style="font-size:24px" class="fa">&#xf0ad;</i>Option 3</th>
      </tr> 
    </thead>
    <tbody> 
      <?php
      $d=1;
      while ($data=$groupes->fetch())
      {
        ?>    
        <tr class="text-center">
          <td><?= htmlspecialchars($data['GroupeNom'] )?></td>
          <td> <?= htmlspecialchars($data['intitule']) ?></td> 
          <td><button type="button" class="btn btn-danger badge-pill"> <a href="../Controleur/contr_supp_groupes.php?gid=<?=$data['gid']?>&annee=<?= $data['annee'] ?>"><i style="font-size:24px" class="fa">&#xf1f8;</i>Supprimer</a></td>  
            <td><button type="button" class="btn btn-primary badge-pill"> <a href="../Vue/modf_groupes.php?gid=<?=$data['gid']?>&annee=<?= $data['annee'] ?>"><i style='font-size:24px' class='fas'>&#xf044;</i> Modifier</a></td>
              <td><button type="button" class="btn btn-primary badge-pill"> <a href="../Vue/modf_groupes_module.php?gid=<?= $data['gid'] ?>&annee=<?=$data['annee'] ?>"><i style='font-size:24px' class='fas'>&#xf044;</i> Modification association module</a></td>
              </tr>
              <?php
              $annee=$data['annee'] ;   
              $d++;    
            }
            $groupes->closeCursor() ; 
            ?>
          </tbody>
        </table>
      </div><br> 
      <h2 class="center"><i style="font-size:24px" class="fa">&#xf0ad;</i>Plus d'option: <button type="button" class="btn btn-info badge-pill" > <a href="../Vue/ajouter_groupes.php?annee=<?= $annee ?>"><i class="material-icons">group_add</i>Ajouter un groupe </a></button>  </h2>

      <?php

      include("../trame/footer.php");



