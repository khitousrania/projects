<?php
$title="Modification informations gtype";
require("../trame/auth/EtreAuthentifie.php");
include("../trame/headerconnect.php");
?>
<p class="error"><?= $error??""?></p>
<h2 class="center"><i style='font-size:36px' class='fas'>&#xf044;</i> Modification informations gtype</h2>
<div class="center">
    <form method="post" action="../Controleur/contr_modif.php?gtid=<?= $_GET['gtid'] ?>">   
        <table>
            <tr>
                <td><label for="inputNom" class="label">Groupe</label></td>
                <td><input type="text" name="nom" class="input" id="inputNom" placeholder="nom" required value=""?> </td>
            </tr>
            <tr>
                <td><label for="inputNBH" class="label">Nombre d'heure(s):</label></td>
                <td><input type="number" name="nbh" class="input" id="inputNBH" placeholder="Nombre d'heure(s)" required value=""></td>
            </tr>
            <tr>
                <td><label for="inputCoeff" class="label">Coefficient </label></td>
                <td><input type="number" step="any" name="coeff" class="input" id="inputCoeff" placeholder="Coefficient"  value=""></td>
            </tr>
        </table><br>
        <div class="form-group">
            <button type="submit" class="btn btn-danger badge-pill">Modifier</button>
        </div>
    </div>
</form>
</div> 
<?php

include("../trame/footer.php");
