
<?php
$title="Ajouter un utilisateur";
include("../trame/headerconnect.php");
?>
<p class="error"><?= $error??""?></p>
<h2 class="center"><i style='font-size:36px' class='fas'>&#xf044;</i> Modification mot de passe :</h2>
<div class="center">
    <form method="post" action="../Controleur/contr_mdf_mdp_utili.php?uid=<?= $_GET['uid'] ?>">  
        <table>
            <tr>
                <td><label for="inputMDP" class="control-label"></label></td>
                <td><input type="password" name="mdp" class="form-control" id="inputMDP" placeholder="Nouveau mot de passe" required value=""></td>
            </tr>
            <tr>
                <td><label for="inputMDP2" class="control-label"></label></td>
                <td><input type="password" name="mdp2" class="form-control" id="inputMDP" placeholder="Retaper le mot de passe " required value=""></td>
            </tr>
        </table>
        <br>
        <div class="form-group">
            <button type="submit" class="btn btn-danger badge-pill">Modifier</button>
        </div>
    </form>
</div> 
<?php
include("../trame/footer.php");
