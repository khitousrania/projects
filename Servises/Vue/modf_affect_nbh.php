<?php

$title="Modification informations gtype";
include("../trame/headerconnect.php");

?>

<p class="error"><?= $error??""?></p>
<h2 class="center"><i style='font-size:36px' class='fas'>&#xf044;</i>Modification nombre d'heure(s) </h2>
<div class="center">
    <form method="post" 
    action="../Controleur/contr_modif_nbh.php?nbh=<?= $_GET['nbh'] ?>&eid=<?= $_GET['eid'] ?>&gid=<?= $_GET['gid'] ?>&annee=<?= $_GET['annee'] ?>">
    <p class="error"><?= $error??""?></p>  
    <table>
        <tr>
            <td><label for="inputNBH" class="label">Nombre d'heure(s):</label></td>
            <td><input type="number" name="nbh" class="input" id="inputNBH" placeholder="Nombre d'heure(s)"  required value=""></td>
        </tr>         
    </table>            
    <div class="form-group"><button type="submit" class="btn btn-primary badge-pill">Modifier</button> </div>
    
</div>
</form>
</div>
<?php

include("../trame/footer.php");
