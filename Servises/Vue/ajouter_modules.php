<?php
$title="Ajout un module";
require("../Modele/m1.php");
include("../trame/header.php");
?>

<h2 class="center"><i class="fa fa-plus-circle" style="font-size:24px"></i>Ajouter un module : </h2>
<div class="center">
  <form method="post" action="../Controleur/contr_ajout.php?anne=<?= $_GET['annee'] ?>">  
    <table>
     <tr>
      <td><label for="inputNom" class="label">Module </label></td>
      <td><input type="text" name="intitule" class="input" id="inputNom" placeholder="Intitule" required value="">
      </td>
    </tr>
    <tr>
     <td> <label for="inputCode" class="label">Code module </label></td>
     <td>  <input type="text" name="code" class="input" id="inputCode" placeholder="2SX**R*0" required aria-required="true" value="" placeholder="Code" value=""></td>
   </tr>   
   <tr>
    <td><label for="enseignants" class="label"> Enseignant: </label></td>
    <td>  
      <select name = "enseignants" id="enseignants">
        <?php
        $enseignants=list_enseignants($_GET['annee']) ; 
        while($data=$enseignants->fetch()){
          ?>
          <option value="<?= $data['eid'] ?> "> <?= $data['nom'] ?> <?= $data['prenom'] ?> </option> 
          <?php 
        }
        $enseignants->closeCursor() ; 
        ?> 
      </select> 
    </td>
  </tr>
  <tr>
    <td><label for="category" class="control-label"> Categorie : </label></td>
    <td>  
      <select name = "category" id="category">
        <?php
        $category=list_categorie() ; 
        while($data=$category->fetch()){
          ?>
          <option value="<?= $data['cid'] ?> "> <?= $data['nom'] ?>  </option> 
          <?php 
        }
        $category->closeCursor() ; 
        ?> 

      </select> 

    </td>
  </tr>
</table><br>
<div class="form-group">
  <button type="submit" class="btn btn-primary badge-pill">Ajouter</button>
</div>
</div>
</form>
</div>
<?php

include("../trame/footer.php");
