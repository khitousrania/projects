<?php 

include("../trame/headerconnect.php");

?>
<h2 class="center">Informations générales </h2>
<p class="error"><?= $error??""?></p>
<div class="container">
  <table class="table table-dark table-bordered">
    <thead>
      <tr>
        <th> Module </th>
        <th> Groupe </th>
        <th> nbh </th>
        <th>  eqtd </th>
        <th> nbh non effectué(s)</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $b=1;
      $nbh=0;
      $eqtd=0;
      $nbh_non_effect=0;
      while($data=$enseignants->fetch())
      {
        $nbh+=$data['nbh'];
        $eqtd+=$data['eqtd'];
        $nbh_non_effect+=$data['nbh_non_effect'];
        ?>  
        <tr>
          <td> <?= htmlspecialchars($data['intitule']) ?>  </td>
          <td> <?= htmlspecialchars($data['nom']) ?>  </td>
          <td> <?= htmlspecialchars($data['nbh']) ?> </td>
          <td> <?= htmlspecialchars($data['eqtd']) ?> </td>
          <td> <?= htmlspecialchars($data['nbh_non_effect']) ?> </td>

        </tr>

        <?php
      }
      $enseignants->closeCursor() ; 
      ?> 
    </tbody>
  </table>
</div>  
<h2 class="center">Informations sur les horaires </h2>
<div class="container">
  <table class="table table-dark table-bordered">
    <thead>

     <tr>
      <th>Total des nbh  </th>
      <th>Total des eqtd  </th>
      <th>Total nombre d'heure(s) non effectuées  </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?=$nbh?> </td>
      <td><?=$eqtd?> </td>
      <td><?=$nbh_non_effect?> </td>
    </tr>
  </tbody>
</table>
</div> 


<?php

include("../trame/footer.php");

