<?php
$title="Ajouter un utilisateur";
include("../trame/header.php");
?>
<p class="error"><?= $error??""?></p>
<div class="center">
    <h1>Ajouter un utilisateur </h1>
    <form method="post" action="../Controleur/modifiermdp.php?uid=<?= $_GET['uid'] ?>">  
        <table>

            <tr>
                <td><label for="inputMDP" class="control-label">MDP</label></td>
                <td><input type="password" name="mdp" class="form-control" id="inputMDP" placeholder="Mot de passe" required value=""></td>
            </tr>
            <tr>
                <td><label for="inputMDP2" class="control-label">Répéter MDP</label></td>
                <td><input type="password" name="mdp2" class="form-control" id="inputMDP" placeholder="Répéter le mot de passe" required value=""></td>
            </tr>
        </table>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">S'enregistrer</button>
        </div>
    </form>
</div>

<?php

include("../trame/footer.php");
