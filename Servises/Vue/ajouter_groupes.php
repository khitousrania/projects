
<?php
$title="Ajout un groupe";
include("../trame/headerconnect.php");
require("../Modele/m1.php");
?>

<h2 class="center"><i style='font-size:24px' class='fas'>&#xf234;</i>Ajouter un groupe <?= $_GET['annee'] ?> </h2>
<div class="center">
  <p class="error"><?= $error??""?></p>
  <form method="post" action="../Controleur/contr_ajout.php?annee=<?= $_GET['annee'] ?>">
    <table>
      <tr>
        <td><label for="modules" class="label"> Module: </label></td> 
        <td>  
          <select name = "modules" id="modules">
            <?php
            $modules=list_modules($_GET['annee']) ; 
            while($data=$modules->fetch()){
              ?>
              <option value="<?= $data['mid'] ?> "> <?= $data['intitule'] ?>   </option> 
              <?php 
            }
            $modules->closeCursor() ; 
            ?> 
          </select> 
        </td>
      </tr>

      <tr>
        <td><label for="inputNom" class="label">Groupe </label></td>
        <td><input type="text" name="nom" class="input" id="inputNom" placeholder="CM" required value="">
        </td>
      </tr>
      
      
      <tr>
        <td><label for="gtypes" class="label"> Gtype:</label></td>
        <td>  
          <select name = "gtypes" id="gtypes">
            <?php
            $gtypes=list_gtypes() ; 
            while($data=$gtypes->fetch()){
              ?>
              <option value="<?= $data['gtid'] ?> "> <?= $data['nom'] ?>  </option> 
              <?php 
            }
            $gtypes->closeCursor() ; 
            ?> 

          </select> 

        </td>
      </tr>
    </table>
    <div class="form-group">
      <button type="submit" class="btn btn-primary badge-pill">Ajouter</button>
    </div>
  </div>
</form>
</div>
<?php

include("../trame/footer.php");
