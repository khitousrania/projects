<?php
$title="Ajouter un utilisateur";
include("../trame/headerconnect.php");
?>

<div class="center">

    <h1> Modifier le mot de passe  </h1>
    <p class="error"><?= $error??""?></p>
    <form method="post" action="../Controleur/contr_mdf_mdp_user.php?uid=<?= $_GET['uid'] ?>">  
        <table>

            <tr>
                <td><label for="inputMDP" class="control-label">MDP</label></td>
                <td><input type="password" name="mdp" class="form-control" id="inputMDP" placeholder="Mot de passe" required value=""></td>
            </tr>
            <tr>
                <td><label for="inputMDP2" class="control-label">Répéter MDP</label></td>
                <td><input type="password" name="mdp2" class="form-control" id="inputMDP" placeholder="Répéter le mot de passe" required value=""></td>
            </tr>
        </table>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Modifier</button>
        </div>
    </form>
</div>

<?php

include("../trame/footer.php");
