<?php
$title="Affectation groupe ";
include("../trame/header.php");
require("../Modele/m1.php");
?>
<p class="error"><?= $error??""?></p>
<div class="center">
  <h1>Affectation du  groupe:  <?= $_GET['annee'] ?> </h1>
  
  <form method="post" action="../Vue/affect_groupes_2.php?annee=<?= $_GET['annee'] ?>">
    <table>
     <tr>
       <td><label for="enseignants" class="label"> Enseignants: </label></td>
       <td>  
         <select name = "enseignants" class="form-control" id="enseignants">
          <?php
          $enseignants=list_affect_enseignants($_POST['enseignants']) ;
          while($data=$enseignants->fetch()){
            ?>
            <option value="<?= $data['eid'] ?> "> <?= $data['nom'] ?> <?= $data['prenom'] ?> </option> 
            <?php 
          }
          $enseignants->closeCursor() ; 
          ?> 
        </select> 
      </td>
    </tr>
    <tr>
     <td><label for="modules" class="label"> modules: </label></td>
     <td>  
       <select name = "modules" class="form-control" id="modules">
         <?php
                                          //$groupes=list_groupes($_GET['annee']) ;
         $modules=list_affect_modules($_POST['modules']) ; 
         while($data=$modules->fetch()){
          ?>
          <option value="<?= $data['mid'] ?>"> <?= $data['intitule'] ?> </option> 
          <?php 
        }
        $modules->closeCursor() ; 
        ?> 
      </select> 
    </td>
  </tr>
  <tr>
   <td><label for="Groupe" class="label"> Groupe: </label></td>
   <td>  
     <select name = "Groupe" class="form-control" id="Groupe">
      <?php
                                        //$groupes=list_groupes() ;
      $groupes=list_affect_groupe($_POST['modules'],$_GET['annee']) ; 
      while($data=$groupes->fetch()){
       ?>
       <option value="<?= $data['gid'] ?>"> <?= $data['nom'] ?> </option> 
       <?php 
     }
     $groupes->closeCursor() ; 
     ?> 
   </select> 
 </td>
</tr>

</table>
<div class="form-group">
  <button type="submit" class="btn btn-primary">Suivant </button>
</div>
</form>
</div>
<?php

include("../trame/footer.php");

