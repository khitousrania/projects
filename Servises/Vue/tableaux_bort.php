<?php
$title="Tableau de bord ";
require("../trame/auth/EtreAuthentifie.php");
require("../Modele/m1.php");
include("../trame/headerconnect.php");
?>
<div ><button type="button" class="btn btn-danger" style="float: right;"><a href="../trame/home.php">Retour menu</a> </button></div>
<p class="error"><?= $error??""?></p>
<h1 class="center">Tableau de bord <?=$_GET['anneerecherche']?></h1>
<div class="container d-flex justify-content-around">
  <div class="dropdown container">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      choisissez un enseignant
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
      <?php 
      $b=false; 
      $enseignants=list_enseignants($_GET['anneerecherche']) ;
      while($data=$enseignants->fetch())
      {
       $b=true;
       ?>  
       <a class="dropdown-item" href="../Vue/tableau_bort_ensg.php?anneerecherche=<?= $data['annee'] ?>&eid=<?= $data['eid'] ?>"> <?= $data['nom'] ?> <?= $data['prenom'] ?> </a>
       <?php
     } 
     if($b==false){
      echo "<a> aucun enseignant </a>";
    } 
    $enseignants->closeCursor() ; 
    ?>
  </div>
</div>

<div class="dropdown container">

  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    choisissez un groupe
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <?php 
    $a=false;
    $groupes=list_groupes($_GET['anneerecherche']) ;
    while($data=$groupes->fetch())
    {
      ?> 
      <a class="dropdown-item" href="../Vue/tableau_bord_groupes.php?anneerecherche=<?= $data['annee'] ?>&gid=<?= $data['gid'] ?>"> <?= $data['GroupeNom'] ?> <?= $data['intitule'] ?> </a>

      <?php
      $a=true;
    }
    if($a==false){
      echo "<a> aucun groupe </a>";
    }
    $groupes->closeCursor() ; 
    ?> 
  </div>
</div>

<div class="dropdown container">

  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    choisissez un module
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <?php 
    $c=false;
    $modules=list_modules($_GET['anneerecherche']) ;
    while($data=$modules->fetch())
    {
      ?> 
      <a class="dropdown-item" href="../Vue/tableau_bord_modules.php?anneerecherche=<?= $data['annee'] ?>&mid=<?= $data['mid'] ?>"> <?= $data['intitule'] ?> </a>
      <?php
      $c=true;
    }
    if($c==false){
      echo "<a>aucun module </a>";
    }
    $modules->closeCursor() ; 
    ?> 
  </div>
</div>
</div>

