package app;

import enums.Direction;

public class Config {
	//Dimension de la fetentre 
	public static final int FRAME_LOCATION_X = 1010; 
	public static final int FRAME_LOCATION_Y = 850;  
	//Dimension de la Grille
	public static final int DIM_X = 40 ;//
	public static final int DIM_Y = 28 ;
	//pixel de la case
	public static final int PAS= FRAME_LOCATION_X / DIM_X ; 
	//Dimension du Panel
	public static final int WIDTH = (PAS*DIM_X); 
	public static final int HEIGHT = (PAS*DIM_Y) ;
	//Nombre de lemmings 
	public static final int NBLEMMINGS_IN_GAME = 15;
	//Buttons et Etat Lemmings
	public static final String[] nameLemming = { "Noraml" , "Bloqueur" , "Tunnelier", "Foreur" , "Bombeur", "Charpentier" , "Parachutiste", "Grimpeur" };
	//Direction du Lemming � l'entree 
	public static final Direction[] directions = {Direction.LEFT , Direction.RIGHT , Direction.LEFT , Direction.LEFT , Direction.RIGHT };
}
