package app;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import gameObservableObserver.GameObservable;
import gameObservableObserver.GameViewObserver;

public class Gui {
	
	public Gui() {
		GameObservable game = new GameObservable(Config.DIM_X, Config.DIM_Y);
		GameViewObserver myPanel = new GameViewObserver(game);

		JFrame frame = new JFrame("Lemmings") ; 	
		frame.setSize(Config.FRAME_LOCATION_X,Config.FRAME_LOCATION_Y);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); 
		frame.setContentPane(myPanel);
		frame.setVisible(true);
		
		while(!game.getgameEnd()) {
			game.step();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
		game.unregister(myPanel);
	}
}
