package terrainElements;

import java.awt.Color;

import enums.Direction;
import lemmingsStates.Coordinate;
import lemmingsStates.Lemming;

public class Entrance implements ElementTerrain {
	
	private Direction direction;
	private Coordinate coordinate;

	public Entrance(Direction direction, Coordinate coordinate) {
		this.direction = direction;
		this.coordinate = coordinate;
	}
	
	public Direction getDirection() {
		return direction;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	@Override
	public void action(Lemming lemming) {
		//TODO
	}

	@Override
	public Color getColor() {
		return Color.PINK;
	}

}
