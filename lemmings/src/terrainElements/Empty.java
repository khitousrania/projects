package terrainElements;

import java.awt.Color;

import lemmingsStates.Lemming;

public class Empty implements ElementTerrain {

	@Override
	public void action(Lemming lemming) {
		lemming.getStateNormal().incNbCasesEnChute();
		lemming.moveDown();
	}

	@Override
	public Color getColor() {
		return Color.WHITE;
	}

}
