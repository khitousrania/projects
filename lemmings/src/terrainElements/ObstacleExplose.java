package terrainElements;

import java.awt.Color;
import lemmingsStates.Coordinate;
import lemmingsStates.Lemming;

public class ObstacleExplose extends ObstacleDestructible {

	@Override
	public void action(Lemming lemming) {
		lemming.getStateNormal().comportement();
	}

	@Override
	public void actionDestructibleObstacle(Lemming lemming, int i, int j) {
		for(Lemming lemmings : lemming.getGame().getLemmings() ) {
			for(int k = lemming.getX()-1 ; k< lemming.getX() + 2 ; k ++) {
				for(int m = lemming.getY()-1 ; m< lemming.getY() + 2 ; m ++) {

					if(lemmings.isLemming(new Coordinate(k, m)) ) {
						lemmings.lemmingDead();
						lemming.getGame().incNbrLemmingsDead();
					}
				}
			}
		}
		lemming.setState(lemming.getStateNormal());
        lemming.getGame().remove(i,j);
	}
	
	@Override
	public Color getColor() {
		return Color.YELLOW;
	}
	
}
