package terrainElements;

import java.awt.Color;
import lemmingsStates.Lemming;

public class Arrival implements ElementTerrain {

	@Override
	public void action(Lemming lemming) {
		lemming.lemmingSaved();
	}

	@Override
	public Color getColor() {
		return Color.BLUE;
	}

}
