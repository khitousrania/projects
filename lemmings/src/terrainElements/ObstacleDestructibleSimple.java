package terrainElements;
import java.awt.Color;
import lemmingsStates.Lemming;

public class ObstacleDestructibleSimple extends ObstacleDestructible {

	@Override
	public void action( Lemming lemming ) {
		lemming.getStateNormal().comportement();
	}
	
	@Override
	public void actionDestructibleObstacle(Lemming lemming, int i, int j) {
		lemming.getGame().remove( i, j);
	}
	
	@Override
	public Color getColor() {
		return Color.DARK_GRAY;
	}
	
}
