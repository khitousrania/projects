package terrainElements;

import lemmingsStates.Lemming;

public abstract class  ObstacleDestructible implements ElementTerrain  {

	public abstract void action(Lemming lemming) ;
	public abstract void actionDestructibleObstacle(Lemming lemming, int i, int j);
	
	@Override
	public boolean equals(Object obj) {
		boolean b = false; 
		if (this == obj)
			b= true;
		if (obj == null)
			b = false;
		if (getClass() != obj.getClass())
			b= false;
		return b;
	}
}
