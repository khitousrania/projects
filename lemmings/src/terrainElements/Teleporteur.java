package terrainElements;

import java.awt.Color;
import lemmingsStates.Lemming;

public class Teleporteur extends ObstacleIndestructible {
	
	@Override
	public void action(Lemming lemming) {
         lemming.getCoordinate().setX(20);
         lemming.getCoordinate().setY(25);
	}

	@Override
	public Color getColor() {
		return Color.LIGHT_GRAY;
	}

}
