package terrainElements;

import java.awt.Color;
import lemmingsStates.Lemming;

public class Lave extends ObstacleIndestructible{

	@Override
	public void action(Lemming lemming) {
		lemming.lemmingDead();
	}

	@Override
	public Color getColor() {
		return Color.RED;
	}

}
