package terrainElements;

import java.awt.Color;
import lemmingsStates.Lemming;

public class ObstacleIndestructibleSimple extends ObstacleIndestructible {
	
	@Override
	public void action(Lemming lemming) {
		lemming.getStateNormal().comportement();
	}

	@Override
	public Color getColor() {
		return Color.BLACK;
	}
}
