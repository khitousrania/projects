package terrainElements;

import java.util.ArrayList;

import app.Config;
import lemmingsStates.Coordinate;

public class Map {
	public final static ElementTerrain EMPTY = new Empty() ;
	public final static ElementTerrain DESTRUCTIBLE_OBSTABLES_SIMLE = new ObstacleDestructibleSimple() ;
	public static final ElementTerrain EXPLODE_OBSTABLES = new ObstacleExplose();
	public static final ElementTerrain APPEARS_OBSTACLES =new ObstacleAppears() ;
	public static final ElementTerrain INDESTRUCTIBLE_OBSTABLES_SIMLE = new ObstacleIndestructibleSimple();
	public static final ElementTerrain TELEPORTERS_OBSTACLES = new Teleporteur();
	public static final ElementTerrain LAVE = new Lave();
	public static final ElementTerrain ARRIVALS = new Arrival();
	private static  ArrayList<ElementTerrain> entrances = new ArrayList<>();
	
	private Map() {
		
	}
	public static ElementTerrain[][] map(){
		String[][] map= {
      /*0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39      x
/*0*/ {"4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4"},
/*1*/ {"4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4"},
/*2*/ {"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4"},
/*3*/ {"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*4*/ {"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*5*/ {"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*6*/ {"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*7*/ {"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*8*/ {"4","4","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","9","4","4","4","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*9*/ {"4","4","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","9","4","4","4","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*10*/{"4","4","9","9","9","9","9","9","9","9","9","9","9","9","4","7","4","9","4","7","4","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*11*/{"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","1","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*12*/{"4","4","9","9","9","9","1","1","1","1","1","9","9","9","9","9","9","9","9","9","9","9","9","9","9","1","9","9","9","9","9","9","9","4","4","4","4","4","4","4"},
/*13*/{"4","4","9","9","9","9","1","1","1","1","1","1","9","9","9","9","9","9","9","9","9","1","1","1","1","1","9","9","9","9","9","9","9","1","4","4","4","4","4","4"},
/*14*/{"4","4","9","9","9","9","1","1","1","1","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","5","4","4","7","4","4","4"},
/*15*/{"4","4","9","9","9","9","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","9","9","9","9","9","9","4","4"},
/*16*/{"4","4","9","9","9","9","1","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","9","9","9","9","9","9","4","4"},
/*17*/{"4","4","4","4","4","4","1","1","1","1","4","4","4","4","4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","2","4","4","4","4","4","4"},
/*18*/{"4","4","4","4","4","4","1","1","1","1","4","4","4","4","4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4","4","4","4","4"},
/*19*/{"4","4","4","4","4","4","1","1","1","1","4","4","4","4","4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","4","4","4","4","4","4","4","4","4","4","4"},
/*20*/{"4","4","1","1","1","1","1","1","1","1","4","4","4","9","9","9","6","6","6","6","6","6","6","6","6","6","6","6","6","9","4","4","4","9","9","9","9","9","4","4"},
/*21*/{"4","4","1","1","1","1","1","1","1","1","4","4","4","9","9","9","6","6","6","6","6","6","6","6","6","6","6","6","6","9","4","4","4","9","9","9","9","9","4","4"},
/*22*/{"4","4","1","1","1","1","1","1","1","1","4","7","4","9","9","9","6","6","6","6","6","6","6","6","6","6","6","6","6","9","4","7","4","9","9","9","9","9","4","4"},
/*23*/{"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","1","1","9","9","9","9","9","9","9","9","1","1","9","9","9","9","9","9","9","9","9","9","4","4"},
/*24*/{"4","4","9","9","9","9","9","9","9","9","9","9","9","9","9","9","1","1","9","9","9","9","9","9","9","9","1","1","9","9","9","9","9","9","9","9","9","9","4","4"},
/*25*/{"4","4","8","9","9","9","9","9","9","9","9","9","9","9","9","9","1","1","9","9","9","9","9","9","9","9","1","1","9","9","9","9","9","9","9","9","9","8","4","4"},
/*26*/{"4","4","4","4","4","4","4","4","4","4","4","4","4","6","6","6","4","4","4","4","4","4","4","4","4","4","4","4","6","6","6","4","4","4","4","4","4","4","4","4"},
/*27*/{"4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4","4"},
/*y*/		};
		return getmap(map); 
	}

	private static ElementTerrain[][] getmap(String[][] map) {
		ElementTerrain[][] cases = new ElementTerrain[Config.DIM_X][Config.DIM_Y];
		int k = 0 ; 
		for(int j = 0 ; j<Config.DIM_Y ; j++ ) {
			for(int i = 0 ; i < Config.DIM_X ; i++) {
				if(map[j][i] == "1") {
					cases[i][j] =  DESTRUCTIBLE_OBSTABLES_SIMLE;
				}else if(map[j][i] == "2") {
					cases[i][j] = EXPLODE_OBSTABLES;
				}else if(map[j][i] == "3") {
					cases[i][j] = APPEARS_OBSTACLES;
				}else if(map[j][i] == "4") {
					cases[i][j] =INDESTRUCTIBLE_OBSTABLES_SIMLE;
				}if(map[j][i] == "5") {
					cases[i][j] =TELEPORTERS_OBSTACLES;
				}else if(map[j][i] == "6") {
					cases[i][j] = LAVE;
				}else if(map[j][i] == "7") {
					entrances.add(new Entrance(Config.directions[k],new Coordinate(i, j)));
					cases[i][j] = entrances.get(k++) ;
				}else if(map[j][i] == "8") {
					cases[i][j] =ARRIVALS;
				}else if(map[j][i] == "9") {
					cases[i][j] = EMPTY;
				}
			}
		}
		return cases;
	}

	public static ArrayList<ElementTerrain> getEntrance() {
		return entrances;
	}

}
