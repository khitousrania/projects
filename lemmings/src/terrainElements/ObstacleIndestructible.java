package terrainElements;

import lemmingsStates.Lemming;

public abstract class ObstacleIndestructible implements ElementTerrain {
	
	public abstract void action(Lemming lemming) ;
	
}
