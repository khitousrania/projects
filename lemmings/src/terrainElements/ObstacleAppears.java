package terrainElements;

import java.awt.Color;
import lemmingsStates.Lemming;

public class ObstacleAppears extends ObstacleDestructible  {

	@Override
	public void action(Lemming lemming) {
		lemming.getStateNormal().comportement();
	}

	@Override
	public void actionDestructibleObstacle(Lemming lemming, int i, int j) {
		for(int k = 16 ; k< 29 ; k++) {
			for(int h = 17 ; h< 20 ; h++) {
				lemming.getGame().build(k, h);
			}
		}
		lemming.getGame().remove( i, j);
	}
	
	@Override
	public Color getColor() {
		return Color.CYAN;
	}

}
