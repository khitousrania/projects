package terrainElements;

import java.awt.Color;

import lemmingsStates.Lemming;

public interface ElementTerrain {
	
	public void action(Lemming lemming) ;
	public Color getColor();
	
}
