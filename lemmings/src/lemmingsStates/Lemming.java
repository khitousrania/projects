package lemmingsStates;
import java.awt.Color;

import enums.Direction;
import enums.EtatLemming;
import gameObservableObserver.IGame;
import terrainElements.ElementTerrain;
import terrainElements.Entrance;

public class Lemming   {

	private IGame game ;  
	private Direction direction;
	private Coordinate coordinate;
	private EtatLemming etatLemming ;
	private StateLemming state ;
	private LemmingNormal stateNormal ;

	public Lemming (IGame game , ElementTerrain entrance) {
		this.game=game;
		direction=((Entrance) entrance).getDirection(); 
		coordinate=new Coordinate(((Entrance) entrance).getCoordinate().getX(), ((Entrance) entrance).getCoordinate().getY()+1);
		etatLemming = EtatLemming.ALIVE;
		stateNormal =  new LemmingNormal(this);
		state = stateNormal ; 
	}
	
	public Coordinate getCoordinate() {
		return coordinate;
	}
	
	public Direction getDirection() {
		return direction;
	}

	
	public IGame getGame() {
		return game;
	}
	
	public Color getColor() {
		return state.getColor();
	}
	
	public int getX() {
		return coordinate.getX();
	}
	
	public int getY() {
		return coordinate.getY();
	}
	
	public int getNextX() {
		switch (direction) {
		case RIGHT: {
			return coordinate.getX()+1 ;
		}
		case LEFT: {

			return coordinate.getX()-1 ;
		}
		default : 
			return coordinate.getX();
		}
	}
	
	public LemmingNormal getStateNormal() {
		
		return stateNormal;
	}
	
	public void setState(StateLemming state) {
		this.state = state;
	}
	
	
	public StateLemming getState() {
		return state;
	}
	public void lemmingDead() {
		etatLemming = EtatLemming.DIED ;
		game.incNbrLemmingsDead();
	}
	
	public void lemmingSaved() {
		etatLemming = EtatLemming.SAVED ;
		game.incNbrLemmingsSaved();
	}
	
	public boolean isSavedOrDied() {
		return ( etatLemming == EtatLemming.SAVED || etatLemming == EtatLemming.DIED) ;
	}

	public boolean isLemming(Coordinate c) {
		return (coordinate.equals(c));
	}

	public boolean isObstacleBiggerLemming() {
		return (game.isObstacle( getNextX(), coordinate.getY()) &&
				(game.isObstacle(getNextX(), coordinate.getY()-1) || game.isObstacle( coordinate.getX()  , coordinate.getY()-1)));
	}
	
	public boolean isObstacleOneCase() {
		return (game.isObstacle(getNextX(), coordinate.getY()) && ! game.isObstacle(getNextX(), coordinate.getY()-1));
	}
	public void step() {
		if(game.isSaved(getX(), getY())) {
			lemmingSaved();
		}
		if(game.isDead(getX(), getY())) {
			lemmingDead();
		}
		state.step();
	}

	public void moveDown() {
		coordinate.setY(getY()+1);
	}
	
	public void moveUp() {
		coordinate.setY(getY()-1);
	}
	
	public void move() {
		switch (direction) {
		case RIGHT: {
			coordinate.setX(getX()+1);
			break;
		}
		case LEFT: {
			coordinate.setX(getX()-1);
			break;
		}
		default:
			break;
		}
	}
	public void changeDirection() {
		direction = Direction.values()[(direction.ordinal()+1)%2];
	}
}


