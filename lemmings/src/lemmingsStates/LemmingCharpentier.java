package lemmingsStates;
import java.awt.Color;

public class LemmingCharpentier extends StateLemming {
	
	private int nbrCasesConstruit ;
	
	public LemmingCharpentier(Lemming lemming) {
		super(lemming);
		nbrCasesConstruit=0;
	}
	
	@Override
	public void step() {
		if( ! lemming.getGame().isObstacle( lemming.getX(), lemming.getY()+1)) {
			lemming.getStateNormal().step();
		}else if(lemming.getGame().isEmpty(lemming.getNextX(), lemming.getY()) &&
				!lemming.getGame().isObstacle(lemming.getNextX(),  lemming.getY()-1) ) {
			comportement();
		}else {
			lemming.setState(lemming.getStateNormal());
		}
	}
	
	@Override
	public void comportement() {
		lemming.getGame().build(lemming.getNextX(), lemming.getY());
		lemming.moveUp();
		lemming.move();
		nbrCasesConstruit++;
		if(isTacheTermine()) {
			lemming.setState(lemming.getStateNormal());
		}
	}
	
	@Override
	public boolean isTacheTermine() {
		return (nbrCasesConstruit == 5) ;
	}

	@Override
	public Color getColor() {
		return Color.YELLOW;
	}

	

}
	


