package lemmingsStates;
import java.awt.Color;

public class LemmingBombuer extends StateLemming {
	
	private int nbrPas ;
	
	public LemmingBombuer(Lemming lemming) {
		super(lemming);
		nbrPas = 0;
	}

	@Override
	public void step() {
		nbrPas++;
		if(isTacheTermine()) {
			comportement();

		}else{
			lemming.getStateNormal().step();
		}
	}
	
	@Override
	public void comportement() {
		for(int i = lemming.getX()-2 ; i< lemming.getX() + 3 ; i ++) {
			for(int j = lemming.getY()-2 ; j< lemming.getY() + 3 ; j ++) {
				obstacleDestructible = lemming.getGame().getObstacleDestructible(i,j ) ;
				if(obstacleDestructible != null) {
					obstacleDestructible.actionDestructibleObstacle(lemming, i, j);	
				}
			}
		}
		lemming.lemmingDead();
		return ;
	}
	
	@Override
	public boolean isTacheTermine() {
		return (nbrPas == 3);
	}

	@Override
	public Color getColor() {
		return Color.LIGHT_GRAY;
	}

}
