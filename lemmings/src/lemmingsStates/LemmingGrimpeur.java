package lemmingsStates ;
import java.awt.Color;

public class LemmingGrimpeur extends StateLemming {

	private boolean grimped ;
	
	public LemmingGrimpeur(Lemming lemming) {
		super(lemming);
		grimped = false ; 
	}

	@Override
	public void step() {
		if(lemming.getGame().isObstacle(lemming.getNextX(), lemming.getY()) &&
				(lemming.getGame().isEmpty(lemming.getX(), lemming.getY()-1) || lemming.getGame().isDead(lemming.getX(), lemming.getY()-1) )) {
			comportement();
			return;
		}
		lemming.getStateNormal().step();
	}
	
	@Override
	public void comportement() {
		lemming.moveUp();
		grimped = true ; 
		if(isTacheTermine()) {
			lemming.setState(lemming.getStateNormal());
			lemming.move();	
		}	
	}
	
	@Override
	public boolean isTacheTermine() {
		return  lemming.getGame().isEmpty(lemming.getNextX(), lemming.getY()) && grimped ;
	}

	@Override
	public Color getColor() {
		return Color.RED;
	}

}
