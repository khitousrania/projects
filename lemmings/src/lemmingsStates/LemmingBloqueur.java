package lemmingsStates;
import java.awt.Color;
import java.util.ArrayList;

public class LemmingBloqueur extends StateLemming  {
	
	public LemmingBloqueur(Lemming lemming) {
		super(lemming);
	}

	@Override
	public void step() {
		if ( !lemming.getGame().isObstacle(lemming.getX(),  lemming.getY()+1)	) {
			lemming.getStateNormal().step();
		}else{
			comportement();
		}
	}

	@Override
	public boolean isTacheTermine() {
		return false;
	}

	@Override
	public void comportement() {
		ArrayList<Lemming> lemmings = lemming.getGame().getLemmings();
		for (Lemming lemming1 : lemmings ) {
			if(lemming.isLemming(new Coordinate(lemming1.getNextX(), lemming1.getY())) ||
					lemming.isLemming(new Coordinate(lemming1.getNextX(), lemming1.getY()-1)))
				lemming1.changeDirection();
		}
	}
	
	@Override
	public Color getColor() {
		return Color.GRAY;
	}
}
