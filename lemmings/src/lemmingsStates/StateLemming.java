package lemmingsStates;
import java.awt.Color;

import terrainElements.ElementTerrain;
import terrainElements.ObstacleDestructible;

public abstract class StateLemming {
	
	protected Lemming lemming;
	protected ElementTerrain elementTerrain ; 
	protected ObstacleDestructible obstacleDestructible ;


	public StateLemming(Lemming lemming) {
		super();
		this.lemming = lemming;
	}
	
	public abstract void step();
	public abstract void comportement();
	public abstract boolean isTacheTermine() ;
	public abstract Color getColor();
}
