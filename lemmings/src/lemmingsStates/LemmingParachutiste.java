package lemmingsStates;
import java.awt.Color;


public class LemmingParachutiste extends StateLemming {
	
	private boolean parachute ;
	
	public LemmingParachutiste(Lemming lemming) {
		super(lemming);
		parachute=false ;
	}

	@Override
	public void step() {
		if( ! lemming.getGame().isObstacle(lemming.getX(),  lemming.getY()+1) ) {
			comportement() ;
		}else if(isTacheTermine()) {
			lemming.setState(lemming.getStateNormal());
		}else {
			lemming.getStateNormal().step();
		}
	}

	@Override
	public void comportement() {
		parachute =true ;
		if(lemming.getGame().getTour() % 2 == 0 ) {
			lemming.moveDown();
		}
	}

	@Override
	public boolean isTacheTermine() {
		return parachute;
	}
	
	@Override
	public Color getColor() {
		return Color.CYAN;
	}

}
