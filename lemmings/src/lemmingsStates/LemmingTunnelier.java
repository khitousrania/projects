package lemmingsStates;
import java.awt.Color;


public class LemmingTunnelier extends StateLemming {

	private int nbCasesCreuse ;

	public LemmingTunnelier(Lemming lemming) {
		super(lemming);
		nbCasesCreuse=0;
	}

	@Override
	public void step() {
		obstacleDestructible = lemming.getGame().getObstacleDestructible(lemming.getNextX(), lemming.getY() ) ;
		if(obstacleDestructible != null  && ! lemming.getGame().isEmpty(lemming.getX(), lemming.getY()+1) &&
				! lemming.getGame().isDead(lemming.getX(), lemming.getY()+1)) {
			comportement();
		}else  {
			lemming.getStateNormal().step();
		}
	}

	@Override
	public void comportement() {
		obstacleDestructible.actionDestructibleObstacle(lemming, lemming.getNextX(), lemming.getY());
		nbCasesCreuse++;
		lemming.move();		
		if(isTacheTermine()) 
			lemming.setState(lemming.getStateNormal());
	}

	@Override
	public boolean isTacheTermine() {
		return( lemming.getGame().isEmpty(lemming.getNextX(),  lemming.getY())  && nbCasesCreuse>0 ) || lemming.getGame().isEmpty(lemming.getX(),  lemming.getY()+1);
	}

	@Override
	public Color getColor() {
		return Color.YELLOW;
	}





}
