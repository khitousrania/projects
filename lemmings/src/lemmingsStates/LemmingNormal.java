package lemmingsStates;
import java.awt.Color;

public class LemmingNormal extends StateLemming {

	private int nbCasesEnChute; 

	public LemmingNormal(Lemming lemming) {
		super(lemming);
		nbCasesEnChute=0;
	}

	@Override
	public void step() {
		elementTerrain = lemming.getGame().getElementTerrain(lemming.getX(), lemming.getY()+1);
		if(isTacheTermine()) {
			lemming.lemmingDead();
		}else {
			elementTerrain.action(lemming);
		}		
	}
	
	@Override
	public void comportement() {
		nbCasesEnChute=0;
		if( lemming.isObstacleBiggerLemming()){
			lemming.changeDirection();
		}else if (lemming.isObstacleOneCase() ) {
			lemming.moveUp();
			lemming.move();
		}
		else {
			lemming.move();
		}
	}
	
	@Override
	public boolean isTacheTermine() {
		return (nbCasesEnChute == 5) ;
	}
	
	public void incNbCasesEnChute() {
		nbCasesEnChute++;
	}

	@Override
	public Color getColor() {
		return Color.GREEN;
	}
}



