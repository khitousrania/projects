package lemmingsStates;

import app.Config;

public class LemmingsFactory {
	
	private Lemming lemming ;
	
	public LemmingsFactory(Lemming lemming) {
		this.lemming = lemming;
	}

	public StateLemming getNewState(String typeNewState) {
		if(  typeNewState.equalsIgnoreCase(Config.nameLemming[0]) ){
			return lemming.getStateNormal() ;
		}
		else if( typeNewState.equalsIgnoreCase(Config.nameLemming[1]) ){
			return new LemmingBloqueur(lemming);   
			
		} else if(typeNewState.equalsIgnoreCase(Config.nameLemming[2])){
			return new LemmingTunnelier(lemming);
			
		}else if(typeNewState.equalsIgnoreCase(Config.nameLemming[3])){
			return new LemmingForeur(lemming);
			
		}else if(typeNewState.equalsIgnoreCase(Config.nameLemming[4])){
			return new LemmingBombuer(lemming);
			
		}else if(typeNewState.equalsIgnoreCase(Config.nameLemming[5])){
			return new LemmingCharpentier(lemming);
			
		}else if(typeNewState.equalsIgnoreCase(Config.nameLemming[6])){
			return new LemmingParachutiste(lemming);
		}else if(typeNewState.equalsIgnoreCase(Config.nameLemming[7])){
			return new LemmingGrimpeur(lemming);
		}
		return new LemmingNormal(lemming);
	}
}
