package lemmingsStates;
import java.awt.Color;


public class LemmingForeur extends StateLemming {
	
	private int nbCasesCreuse ;
	
	public LemmingForeur(Lemming lemming) {
		super(lemming);
		nbCasesCreuse = 0;
	}

	@Override
	public void step() {
		obstacleDestructible = lemming.getGame().getObstacleDestructible(lemming.getX(), lemming.getY()+1 ) ;
		if(isTacheTermine()) {
			 	nbCasesCreuse = 0;
				lemming.setState(lemming.getStateNormal());
				return ; 
		}else if(obstacleDestructible != null ) {
			comportement() ;
		}else {
			lemming.getStateNormal().step();
		}
	}
	
	@Override
	public void comportement() {
		obstacleDestructible.actionDestructibleObstacle(lemming,lemming.getX(), lemming.getY()+1 );
		nbCasesCreuse++;
		lemming.moveDown();
	}

	@Override
	public boolean isTacheTermine() {
		return (nbCasesCreuse== 5 ) || (lemming.getGame().isEmpty(lemming.getX(), lemming.getY()+1));
	}
	
	@Override
	public Color getColor() {
		return Color.MAGENTA;
	}

}
