package gameObservableObserver;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JPanel;
import app.Config;
import lemmingsStates.Lemming;
import terrainElements.ElementTerrain;


public class GameViewObserver extends JPanel implements ObserverGame {

	private static final long serialVersionUID = 1L;
	private GameObservable game ;
	private Font fonte; 

	public GameViewObserver(GameObservable game) {
		setBackground(Color.ORANGE);
		this.game= game ; 
		game.register(this);
		new GameController (game ,this);
		fonte = new Font(" TimesRoman ",Font.BOLD,15); 
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		drawTerrain(g);
		drawLemming(g);
		drawScore(g);
	}
	
	private void drawLemming(Graphics g) {
		for(Lemming lemming : game.getLemmings() ) {
			int cellLine = Config.WIDTH *( lemming.getX()) / game.getWidth();
			int cellCol = Config.HEIGHT * ( lemming.getY()) / game.getHeight();
			drawMotifLemming(g ,cellLine ,cellCol , lemming );
		}
	}

	public void drawTerrain(Graphics g) {
		ElementTerrain[] [] cases = game.getGrille();
		for (int i = 0; i < game.getWidth(); i++) {
			int cellLine = Config.WIDTH * i / game.getWidth();
			for (int j = 0; j < game.getHeight(); j++) {
				int cellCol = Config.HEIGHT * j / game.getHeight();
				drawfillRect(g, cellLine, cellCol,cases[i][j].getColor() );
			}
		}
	}

	private void drawScore(Graphics g) {
		g.setColor(Color.BLUE);
		g.setFont(fonte); 
		g.drawString("nombre de Lemmings sauv�: " + game.getNbrLemmingsSaved(), 50 , Config.FRAME_LOCATION_Y - 60);
		g.setColor(Color.RED);
		g.drawString("nombre de Lemmings mort : " + game.getNbrLemmingsDead(),  Config.FRAME_LOCATION_X-260, Config.FRAME_LOCATION_Y - 60 );
	}
	

	@Override
	public void update() {
		repaint();
	}

	private void drawtriagle(Graphics g , int cellLine , int  cellCol , int p1 , int p2 , int p3 , int p4 , int p5 , int p6 ) {
		int x[] = {cellLine + p1 ,cellLine + p2 ,cellLine+p3} ;
		int y[] = {cellCol + p4,cellCol+p5,cellCol+p6} ;
		Polygon p = new Polygon(x, y, 3);
		g.fillPolygon(p);
	}

	private void drawArawRight(Graphics g , int cellLine , int  cellCol ) {
		g.drawLine(cellLine, cellCol+(Config.PAS/2), cellLine+Config.PAS, cellCol+(Config.PAS/2));
		drawtriagle(g, cellLine, cellCol, ((Config.PAS/3)+(Config.PAS/2)), Config.PAS, ((Config.PAS/3)+(Config.PAS/2)), 0, (Config.PAS/2), Config.PAS);
	}

	private void drawArawLeft(Graphics g , int cellLine , int  cellCol ) {
		g.drawLine(cellLine, cellCol+(Config.PAS/2), cellLine+Config.PAS, cellCol+(Config.PAS/2));
		drawtriagle(g, cellLine, cellCol, (Config.PAS/3), 0,(Config.PAS/3), 0, (Config.PAS/2), Config.PAS);
	}

	public void drawfillRect(Graphics g , int cellLine , int cellCol , Color color) {
		g.setColor(color);
		g.fillRect(cellLine, cellCol, Config.PAS, Config.PAS);
	}

	private void drawMotifLemming(Graphics g , int cellLine , int cellCol ,  Lemming lemming ) {
		drawfillRect(g, cellLine, cellCol, lemming.getColor());
		g.setColor(Color.BLACK);
		switch (lemming.getDirection()) {
		case RIGHT:
			drawArawRight(g, cellLine, cellCol);
			break;
		case LEFT:
			drawArawLeft(g, cellLine, cellCol);
			break;
		}
	}

}
