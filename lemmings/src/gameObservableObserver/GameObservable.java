package gameObservableObserver;

import java.util.ArrayList;

import lemmingsStates.Lemming;
import terrainElements.ElementTerrain;
import terrainElements.ObstacleDestructible;

public class GameObservable implements IGame {
	
	private Game game ;
	private ArrayList<ObserverGame> observers;
	
	public GameObservable (int width, int height) {
		game = new Game(width, height);
		observers =new ArrayList<>();
	}
	
	public void register(ObserverGame o) {
		observers.add(o);
	}

	public void unregister(ObserverGame o) {
		observers.remove(o);
	}

	void notifyObservers() {
		for (ObserverGame observerGame : observers) {
			observerGame.update();
		}
	}

	@Override
	public boolean isObstacle(int i, int j) {
		return game.isObstacle(i, j);
	}

	@Override
	public ArrayList<Lemming> getLemmings() {
		return game.getLemmings();
	}

	@Override
	public void addLemmingActif() {
		game.addLemmingActif();
	}


	@Override
	public int getTour() {
		return game.getTour();
	}

	@Override
	public void step() {
		game.step();
		notifyObservers();
	}

	@Override
	public boolean getgameEnd() {
		return game.getgameEnd();
	}

	@Override
	public ObstacleDestructible getObstacleDestructible(int i, int j) {
		return game.getObstacleDestructible(i, j);
	}

	@Override
	public ElementTerrain getElementTerrain(int i, int j) {
		return game.getElementTerrain(i, j);
	}


	@Override
	public void incNbrLemmingsDead() {
		game.incNbrLemmingsDead();
	}

	@Override
	public void incNbrLemmingsSaved() {
		game.incNbrLemmingsSaved();
	}
	
	@Override
	public int getNbrLemmingsSaved() {
		return game.getNbrLemmingsSaved();
	}

	@Override
	public int getNbrLemmingsDead() {
		return game.getNbrLemmingsDead();
	}

	@Override
	public boolean isEmpty(int i, int j) {
		return game.isEmpty(i, j);
	}
	@Override
	public void gameEnd() {
		 game.gameEnd();
	}
	@Override
	public boolean isSaved(int i, int j) {
		return game.isSaved(i, j);
	}
	@Override
	public boolean isDead(int i, int j) {
		return game.isDead(i, j);
	}

	@Override
	public int getHeight() {
		return game.getHeight();
	}

	@Override
	public int getWidth() {
		return game.getWidth();
	}

	@Override
	public void remove(int i, int j) {
		game.remove(i, j);
	}

	@Override
	public void build(int i, int j) {
		game.build(i, j);
	}

	@Override
	public ArrayList<ElementTerrain> getEntrance() {
		return game.getEntrance();
	}
	@Override
	public ElementTerrain[][] getGrille() {
		return game.getGrille();
	}


}
