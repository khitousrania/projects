package gameObservableObserver;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import app.Config;
import lemmingsStates.Lemming;
import terrainElements.ElementTerrain;
import terrainElements.Map;
import terrainElements.ObstacleDestructible;

public class Game implements IGame  {

	private ArrayList<Lemming> lemmings ; 
	private int tour ; 
	private int nbLemmings ; 
	private boolean gameEnd ; 
	private int nbrLemmingsDead ; 
	private int nbrLemmingsSaved ; 
	private  int width;
	private  int height;
	private ElementTerrain[][] grille;
	
	public Game(int width, int height ) {
		this.width = width;  //40
		this.height = height; //28 
		if(height<=0 || width<=0) return;
		grille = Map.map();
		lemmings = new ArrayList<>();   
		tour = 0 ;
		nbLemmings = 0 ; 
		nbrLemmingsDead = 0 ; 
		nbrLemmingsSaved = 0 ; 
		gameEnd = false ; 
	}
	
	@Override
	public void step() { 
		for(Lemming lemming : lemmings ) 
			lemming.step();
		lemmings.removeIf(lemming -> lemming.isSavedOrDied() );
		if(getTour()%3==0 && nbLemmings<Config.NBLEMMINGS_IN_GAME){
			addLemmingActif();
		}
		gameEnd();
		tour++;
	}
	
	@Override
	public ArrayList<Lemming> getLemmings() {
		return this.lemmings;
	}
	
	@Override
	public int getHeight() {
		return this.height;
	}
	
	@Override
	public int getWidth() {
		return this.width;
	}
	
	@Override
	public int getTour() {
		return tour;
	}
	
	@Override
	public boolean getgameEnd() {
		return gameEnd ;
	}

	@Override
	public void incNbrLemmingsDead() {
		this.nbrLemmingsDead++;
	}
	
	@Override
	public void incNbrLemmingsSaved() {
		this.nbrLemmingsSaved ++;
	}

	@Override
	public int getNbrLemmingsSaved() {
		return nbrLemmingsSaved;
	}
	
	@Override
	public int getNbrLemmingsDead() {
		return nbrLemmingsDead;
	}
	
	@Override
	public ElementTerrain getElementTerrain(int i, int j) {
		if(i<0 || i>= width || j<0 || j>= height)
			return null;
		return grille[i][j];
	}
	
	@Override
	public void remove(int i, int j) {
		if ( (i >= 0 && i < width  && j >= 0 && j < height  ) )	
			grille[i][j]=Map.EMPTY;
	}
	
	@Override
	public void build(int i, int j) {
		if ( (i >= 0 && i < width  && j >= 0 && j < height  ) )	
			grille[i][j]=Map.DESTRUCTIBLE_OBSTABLES_SIMLE;
	}
	
	@Override
	public ArrayList<ElementTerrain> getEntrance() {
		return Map.getEntrance();
	}
	
	@Override
	public ElementTerrain[][] getGrille() {
		return grille;
	}

	@Override
	public void addLemmingActif() { 
		for ( ElementTerrain entrance : getEntrance() ) {
			lemmings.add(new Lemming(this,entrance));
			if(++nbLemmings >= Config.NBLEMMINGS_IN_GAME) 
				return ;
		}
	}
	
	@Override
	public boolean isEmpty(int i , int j ) {
		return (getElementTerrain(i, j).equals(Map.EMPTY));
	}
	
	@Override
	public boolean isSaved(int i , int j ) {
		return (getElementTerrain(i, j).equals(Map.ARRIVALS));
	}
	
	@Override
	public boolean isDead(int i , int j ) {
		return (getElementTerrain(i, j).equals(Map.LAVE));
	}
	
	@Override
	public boolean isObstacle(int i, int j) {
		return getElementTerrain(i, j).equals(Map.DESTRUCTIBLE_OBSTABLES_SIMLE ) ||
			   getElementTerrain(i, j).equals(Map.INDESTRUCTIBLE_OBSTABLES_SIMLE) ||
			   getElementTerrain(i, j).equals(Map.EXPLODE_OBSTABLES )||
			   getElementTerrain(i, j).equals(Map.APPEARS_OBSTACLES) ; 
	}
	
	@Override
	public ObstacleDestructible getObstacleDestructible(int i, int j) {
		if(getElementTerrain(i, j).equals(Map.DESTRUCTIBLE_OBSTABLES_SIMLE)) {
			return (ObstacleDestructible) Map.DESTRUCTIBLE_OBSTABLES_SIMLE ;
		}else if(getElementTerrain(i, j).equals(Map.EXPLODE_OBSTABLES)) {
			return (ObstacleDestructible) Map.EXPLODE_OBSTABLES ;
		}if(getElementTerrain(i, j).equals(Map.APPEARS_OBSTACLES)) {
			return (ObstacleDestructible) Map.APPEARS_OBSTACLES ;
		}
		return null ;
	}
	
	@Override
	public void gameEnd() {

			if(nbrLemmingsDead >= Config.NBLEMMINGS_IN_GAME || nbrLemmingsSaved >= Config.NBLEMMINGS_IN_GAME
					|| nbrLemmingsSaved + nbrLemmingsDead >= Config.NBLEMMINGS_IN_GAME   ) {
				gameEnd = true ; 
				JOptionPane.showMessageDialog(null,"Partie termin�e", "Lemming", JOptionPane.NO_OPTION, null);
			}

	}
	
}
