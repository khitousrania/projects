package gameObservableObserver;

import java.util.ArrayList;

import lemmingsStates.Lemming;
import terrainElements.ElementTerrain;
import terrainElements.ObstacleDestructible;

public interface IGame {
	
boolean isObstacle(int i, int j);

	ArrayList<Lemming> getLemmings();

	void addLemmingActif();

	int getTour();
	
	int getNbrLemmingsSaved() ; 
	
	int getNbrLemmingsDead() ; 

	public void incNbrLemmingsDead();
	
	public void incNbrLemmingsSaved() ;

	void step();

	boolean getgameEnd();
	
	ObstacleDestructible getObstacleDestructible(int i, int j);
	
	boolean isEmpty(int i, int j);

	void gameEnd();

	ElementTerrain getElementTerrain(int i, int j);

	boolean isSaved(int i, int j);

	boolean isDead(int i, int j);

	int getHeight();

	int getWidth();

	void remove(int i, int j);

	void build(int i, int j);

	ArrayList<ElementTerrain> getEntrance();

	ElementTerrain[][] getGrille();
	

}