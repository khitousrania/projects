package gameObservableObserver;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JButton;

import app.Config;
import lemmingsStates.Coordinate;
import lemmingsStates.Lemming;
import lemmingsStates.LemmingsFactory;

public class GameController implements ActionListener  {

	private GameObservable game;
	private GameViewObserver mypanel; 
	private String typeNewState ;
	private ArrayList<JButton> buttons ;
	
	public MouseAdapter mouseAdapter = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			super.mouseClicked(e);
			Coordinate c = new Coordinate(positionX(e.getX(), Config.WIDTH) , positionY( e.getY(), Config.HEIGHT)) ; 
			for(Lemming lemming : game.getLemmings() ) {
				if(lemming.isLemming(c)) 
					lemming.setState(new LemmingsFactory(lemming).getNewState(typeNewState));
			}
		}
	};

	
	private int positionX(int pos, int size) {
		return pos / (size / game.getWidth());
	}
	
	private int positionY(int pos, int size) {
		return pos / (size / game.getHeight());
	}

	public GameController(GameObservable game, GameViewObserver myPanel) {
		this.game =game;
		this.mypanel=myPanel ;
		buttons = new ArrayList<>();
		typeNewState="Noraml";
		createButtons();
		myPanel.addMouseListener(mouseAdapter);
	}
	
	public JButton createButton(String name) {
		JButton button = new JButton(name);
		button.setBackground(Color.WHITE);
		button.addActionListener(this);
		return button;
	}
	public void createButtons () {
		mypanel.setLayout(null);  
		int x = 20 ;int y = 120 ; 
		for (int i = 0; i < Config.nameLemming.length; i++ , x=x+y ) { 
			buttons.add(createButton(Config.nameLemming[i]))  ;
			buttons.get(i).setBounds(x, 725, 130, 30);
			mypanel.add(buttons.get(i));
		}	
	}
	public void desactiveBackground(JButton b ) {
		for(int i =0 ; i<buttons.size() ; i++) {
			if( buttons.get(i) != b)
				buttons.get(i).setBackground(Color.WHITE);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton) e.getSource();
		typeNewState = e.getActionCommand();
		source.setBackground(Color.CYAN);
		desactiveBackground(source);
	}

}
