#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <string.h>
#include "wave.h"

#define prl { printf("\n"); }


wave_t* wave_open(const char* fname){

    wave_t* w = malloc(sizeof(wave_t));

    w->fd = fopen(fname,"rb") ;

    if(w->fd==NULL){
        printf("wave_open: cannot open file %s in mode r \n", fname);
        exit(EXIT_FAILURE) ;
    }

    fread(&(w->h), sizeof(header_t), 1, w->fd);

    if(strncmp((const char*) w->h.riff,"RIFF",4)==0  && w->h.S == w->h.D+44-8 &&
            strncmp((const char*) w->h.wave,"WAVE",4)==0  && strncmp((const char*) w->h.fmt,"fmt ",4)==0 &&
            w->h.nbr16 == 16 && w->h.PCM==1 && (w->h.c >= 1  || w->h.c <=6 ) && ( w->h.r == (w->h.f)*(w->h.b) ) &&
            (w->h.b ==( (w->h.c*w->h.p)/8 )) && strncmp((const char*) w->h.data,"data",4)==0 ) {
        return w ;
    }
    else {
        printf("wave_open %s Is not wave format\n",fname);
        fclose(w->fd);
        exit(EXIT_FAILURE);
    }
}
void create_zero_file(const char* fname, size_t n) {
    FILE* fd = fopen(fname, "wb"); 	     // Ouverture du fichier en binaire avec création ou écrasement du précédent
    fseek(fd, n-1, SEEK_SET);              // Déplacement du curseur à l'avant dernière position de la nouvelle taille
    fputc(0, fd);				             // Ecriture du dernier octet
    fclose(fd);                            // Fermeture (et donc enregistrement du fichier)
}
wave_t* wave_create(const char* fname, size_t f, size_t p, size_t c, size_t B){

    wave_t* w = malloc(sizeof(wave_t)) ;
    w->fd =fopen(fname, "wb");
    w->h.riff[0]='R'; w->h.riff[1]='I'; w->h.riff[2]='F'; w->h.riff[3]='F';
    w->h.wave[0]='W'; w->h.wave[1]='A'; w->h.wave[2]='V'; w->h.wave[3]='E';
    w->h.fmt [0]='f'; w->h.fmt [1]='m'; w->h.fmt [2]='t'; w->h.fmt [3]=' ';
    w->h.data[0]='d'; w->h.data[1]='a'; w->h.data[2]='t'; w->h.data[3]='a';

    w->h.nbr16=16 ;
    w->h.PCM  = 1 	;

    w->h.f=f ;
    w->h.p=p ;
    w->h.c=c ;

    w->h.b=((w->h.c)*(w->h.p))/8 ;
    w->h.r=(w->h.f)*(w->h.b) ;

    w->h.D=B*(w->h.b) ;
    w->h.S=(w->h.D)+44-8 ;



    fwrite(&(w->h), sizeof(header_t), 1, w->fd);

    create_zero_file(fname, w->h.D);

    return w ;
}

void wave_close(wave_t* w){
    if(w == NULL) return;
    else if(w->fd){
        fclose(w->fd) ;
        free(w) ;
        w=NULL ;
    }
    return ;
}
void wave_print_info(wave_t* w){
    if(w==NULL) return ;
    printf("Number of channels: %d\n",w->h.c) ;
    printf("Sample rate: %d Hz\n",w->h.f) ;
    printf("Precision: %d bits\n",w->h.p) ;
    printf("Number of samples: %d\n", (w->h.D)/(w->h.b)) ;
}
size_t wave_get_c(wave_t* w){
    if(w==NULL) exit(EXIT_FAILURE) ;
    return w->h.c ;
}
size_t wave_get_f(wave_t* w) {
    if(w==NULL) exit(EXIT_FAILURE) ;
    return w->h.f ;
}
size_t wave_get_p(wave_t* w){
    if(w==NULL) exit(EXIT_FAILURE) ;
    return w->h.p ;
}
size_t wave_get_D(wave_t* w){
    if(w==NULL) exit(EXIT_FAILURE) ;
    return w->h.D ;
}
size_t wave_get_b(wave_t* w){
    if(w==NULL) exit(EXIT_FAILURE) ;
    return w->h.b ;
}
size_t wave_get_S(wave_t* w) {
    if(w==NULL) exit(EXIT_FAILURE) ;
    return (w->h.D)+44-8 ;
}
size_t wave_get_B(wave_t* w){
    if(w==NULL) exit(EXIT_FAILURE) ;
    return (w->h.D)/(w->h.b) ;
}

size_t wave_get_r(wave_t* w){
    if(w==NULL) exit(EXIT_FAILURE) ;
    return (w->h.r) ;
}

void wave_set(wave_t* w, size_t ch, size_t k, double sig){
    if(w==NULL || w->fd==NULL){
        printf("wave_set: w and w->fd does not null\n");
        exit(EXIT_FAILURE);
    }
    if(sig > 1) sig  = 1 ;
    if(sig <-1 ) sig = -1 ;
    if(w->h.p==8){
        fseek(w->fd, (44 + k *( w->h.b)), SEEK_SET) ;
        int8_t bloc [2] ;
        fread(bloc, sizeof(int8_t),2, w->fd) ;
        int8_t i = (int8_t) (sig * INT8_MAX ) ;
        bloc[ch]= i ;
        fwrite(&bloc[ch], sizeof(int8_t), 1, w->fd ) ;
    }else{
        fseek(w->fd, (44 + k *( w->h.b)), SEEK_SET) ;
        int16_t bloc [2] ;
        fread(bloc, sizeof(int16_t),2, w->fd) ;
        int16_t i = (int16_t) (sig * INT16_MAX ) ;
        bloc[ch]= i ;
        fwrite(&bloc[ch], sizeof(int16_t), 1, w->fd ) ;
    }
}


double wave_get(wave_t* w, size_t ch, double s){
    if(w==NULL || w->fd==NULL){
        printf("wave_get: w and w->fd does not null\n");
        exit(EXIT_FAILURE);
    }

    int32_t e1 , e2;
    double e ;
    e = (s * (w->h.f)) ;
    e1 = floor(e) ;
    e2 = floor(e)+1 ;
    size_t p1 = e1 % wave_get_B(w) ;
    size_t p2 = e2 % wave_get_B(w) ;
    if(w->h.p==8){
        fseek(w->fd, (44 + p1 * (w->h.b)), SEEK_SET) ;
        int8_t bloc1[2] ;
        fread(bloc1, sizeof(int8_t), 2, w->fd ) ;
        fseek(w->fd, (44 + p2 *( w->h.b)), SEEK_SET) ;
        int8_t bloc2[2] ;
        fread(bloc2,sizeof(int8_t), 2, w->fd ) ;
        int8_t i1 = bloc1[ch] ;
        int8_t i2 = bloc2[ch] ;
        double d1 = (double) i1 / INT8_MAX ;
        double d2 = (double) i2 / INT8_MAX ;
        double d = (e2-e)*d1 + (e-e1)*d2 ;
        return d ;
    }else{
        fseek(w->fd, (44 + p1 * (w->h.b)), SEEK_SET) ;
        int16_t bloc1[2] ;
        fread(bloc1, sizeof(int16_t), 2, w->fd ) ;
        fseek(w->fd, (44 + p2 *( w->h.b)), SEEK_SET) ;
        int16_t bloc2[2] ;
        fread(bloc2,sizeof(int16_t), 2, w->fd ) ;
        int16_t i1 = bloc1[ch] ;
        int16_t i2 = bloc2[ch] ;
        double d1 = (double) i1 / INT16_MAX ;
        double d2 = (double) i2 / INT16_MAX ;
        double d = (e2-e)*d1 + (e-e1)*d2 ;
        return d ;
    }
}




/*
int main() {
  wave_t* w;

  printf("*** Generation of three seconds of silence (listen to it!) ***"); prl; prl;
  w = wave_create("silence.wav", 44100, 16, 1, 3 * 44100);
  wave_print_info(w);
  wave_close(w);
  prl;

  printf("*** Generation of a sine 440Hz (listen to it!) ***"); prl; prl;
  w = wave_create("sine440.wav", 44100, 16, 2, 3 * 44100);
  for(size_t k = 0; k < wave_get_B(w); k++) {
    double sig = sin(2.0 * M_PI * 440 * k / wave_get_f(w));
    double pan = 0.5 + 0.5 * cos(2.0 * M_PI * 0.5 * k / wave_get_f(w));
    wave_set(w, 0, k, pan * sig);
    wave_set(w, 1, k, (1.0 - pan) * sig);
  }
  wave_close(w);

  printf("*** Opening of WAVE file (should be equal to the one previously generated) ***"); prl; prl;
  w = wave_open("testwave.wav");
  printf("  - Information\n");
  printf("    - c: %lu\n", wave_get_c(w));
  printf("    - f: %lu\n", wave_get_f(w));
  printf("    - p: %lu\n", wave_get_p(w));
  printf("    - D: %lu\n", wave_get_D(w));
  printf("    - b: %lu\n", wave_get_b(w));
  printf("    - r: %lu\n", wave_get_r(w));
  printf("    - S: %lu\n", wave_get_S(w));
  printf("    - B: %lu\n", wave_get_B(w));
  prl;
  printf("  - Some samples\n");
  double s;
  s = 0.00; printf("    - %f: %e, %e\n", s, wave_get(w, 0, s), wave_get(w, 1, s));
  s = 0.12; printf("    - %f: %e, %e\n", s, wave_get(w, 0, s), wave_get(w, 1, s));
  s = 0.23; printf("    - %f: %e, %e\n", s, wave_get(w, 0, s), wave_get(w, 1, s));
  s = M_PI; printf("    - %f: %e, %e\n", s, wave_get(w, 0, s), wave_get(w, 1, s));
  wave_close(w);

  return EXIT_SUCCESS;
}*/
