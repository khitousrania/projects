#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "xml.h"

#define prl { printf("\n"); }

xelement_t* create_xelement(const char* name)
{

    xelement_t* e = (xelement_t*) malloc (sizeof(xelement_t)) ;

    char* c = (char*) malloc ( (strlen(name)+1) * sizeof(char) ) ;

    c = strcpy(c,name) ;

    e->nom = c ;
    e->attribut = NULL ;
    e->pere = NULL ;
    e->frere = NULL ;

    e->contenu.raw = NULL ;
    e->contenu.fils = NULL ;
    e->type_contenu = Fils;

    return e ;
}

void add_xattribute(xelement_t* e, const char* name, const char* value)
{

    char * n = malloc( ( strlen(name)+1 ) ) ;
    n=strcpy(n,name ) ;

    char * v = malloc( ( strlen(value)+1) ) ;
    v=strcpy(v,value) ;

    if(e->attribut == NULL)
    {

        e->attribut = malloc(sizeof(xattribute_t));
        e->attribut->nom = n;
        e->attribut->valeur = v;
        e->attribut->suiv=NULL;

    }
    else
    {

        xattribute_t* a = e->attribut;
        xattribute_t* prec = NULL;
        while(a != NULL )
        {
            if(strcmp(a->nom, name ) == 0 )
            {
                free(a->valeur) ;
                a->valeur = v ;

                return ;
            }
            prec=a ;
            a = a->suiv ;
        }
        xattribute_t* tmp = malloc(sizeof(xattribute_t)) ;

        tmp->nom = n ;
        tmp->valeur= v ;
        tmp->suiv=NULL ;
        prec->suiv = tmp ;

    }
    return ;
}

void add_sub_xelement(xelement_t* e, xelement_t* s)
{


    if(s != NULL)
    {
        if( s->pere == NULL && s->frere==NULL && e != NULL  )
        {
            s->pere=e;
            if(e->contenu.fils==NULL)
            {
                e->contenu.fils=s;
                return ;
            }
            else
            {
                xelement_t* c = e->contenu.fils;
                s->frere = c;
                e->contenu.fils=s;
                return ;
            }
        }
        printf("EXIT_FAILURE\n");
        exit(EXIT_FAILURE);
    }
}


void add_raw(xelement_t* e, const char* r)
{

    char* c = malloc( strlen(r)+1 );
    c=strcpy(c,r);
    if(e != NULL )
    {
        if(e->contenu.raw != NULL)
        {
            free(e->contenu.raw) ;
            e->contenu.raw=c ;
        }
        e->contenu.raw = c ;
        e->type_contenu = Raw;
    }
    else
    {

        exit(EXIT_FAILURE) ;

    }
}

void delete_xattribut(xattribute_t* a)
{

    if(a==NULL)
        return;
    else
    {
        delete_xattribut(a->suiv);
        free(a->nom) ;
        a->nom = NULL ;
        free(a->valeur) ;
        a->valeur=NULL ;
        a=NULL ;
    }

}

void delete_xelement(xelement_t* e)
{
    if(e == NULL)
    {
        return ;
    }
    else
    {

        if(e->contenu.raw == NULL )
        {
            delete_xelement(e->contenu.fils) ;
            free(e->contenu.fils) ;
            e->contenu.fils=NULL ;
        }
        else
        {
            free(e->contenu.raw) ;
            e->contenu.raw=NULL ;
        }
        delete_xelement(e->frere) ;

        delete_xattribut(e->attribut);

        free (e->nom) ;
        e->nom=NULL ;
        free (e->frere ) ;
        e->frere=NULL;
    }

    free(e) ;
    e=NULL ;
}

//ACCESSEUR :

const char* get_xelement_name(xelement_t* e)
{

    if(e==NULL)
        return NULL ;

    const char* n = e->nom ;
    return n ;
}

const char* get_xelement_attribute_value(xelement_t* e, const char* attribute_name)
{
    if(e==NULL )
        return NULL ;

    while(e->attribut != NULL  && e->attribut->nom!=NULL && strcmp(e->attribut->nom,attribute_name) != 0 )
    {
        e->attribut =  e->attribut->suiv  ;
    }

    if(e->attribut != NULL && e->attribut->nom!=NULL && strcmp(e->attribut->nom,attribute_name) == 0 )
    {
        const char* n = e->attribut->valeur ;

        return n ;
    }
    return NULL ;
}

const char* get_xelement_rawdata(xelement_t* e)
{
    if(e==NULL )
        return NULL ;
    if(e->contenu.raw == NULL )
        return NULL ;
    const char* n = e->contenu.raw ;
    return n ;
}

xelement_t* get_xelement_children(xelement_t* e)
{
    if(e==NULL || e->contenu.fils == NULL)
        return NULL ;
    return e->contenu.fils ;
}

xelement_t* next_xelement_child(xelement_t* e)
{
    if(e==NULL)
        return NULL ;
    return e->frere ;
}

//EXPort
void save_xelement(FILE* fd, xelement_t* e)
{
    if(fd==NULL)
    {
        printf("le fichier fd est NULL\n") ;
        exit(EXIT_FAILURE) ;
    }

    if(e==NULL)
        return ;

    fprintf(fd,"<%s", e->nom);

    while(e!=NULL  && e->attribut != NULL)
    {
        fprintf(fd," %s=\"%s\"", e->attribut->nom, e->attribut->valeur ) ;
        e->attribut=e->attribut->suiv ;
    }

    if(e->contenu.fils == NULL  && e->contenu.raw == NULL)
    {
        fprintf(fd," />\n") ;

    }
    else
    {
        if(e->contenu.raw != NULL && e->type_contenu == Raw)
        {
            fprintf(fd,">");
        }
        else
        {
            fprintf(fd,">\n");
        }

        if( e->contenu.fils != NULL && e->type_contenu == Fils )
        {
            save_xelement(fd,e->contenu.fils) ;

        }
        else if(e->contenu.raw != NULL && e->type_contenu == Raw)
        {
            fprintf(fd,"%s",e->contenu.raw) ;

        }

        fprintf(fd,"</%s>\n",e->nom) ;
    }
    save_xelement(fd,e->frere) ;
}

void print_xelement(xelement_t* e)
{
    save_xelement(stdout, e);
}

void save_xml(const char* fname, xelement_t* e)
{
    FILE* fd = fopen (fname,"w") ;
    if(fd==NULL)
    {
        fprintf(stderr,"cannot open file %s in mode w\n", fname);
        exit(EXIT_FAILURE);
    }
    save_xelement(fd,e) ;
    fclose(fd) ;
}

//IMport

char next_char(FILE* fd)
{
    if(fd==NULL)
    {
        fprintf(stderr,"cannot open file \n");
        exit(EXIT_FAILURE);
    }
    char c ;
    while( ! feof(fd) )
    {
        c = fgetc(fd) ;
        if( c != ' ' && c != '\n' && c != '\t' && c != '\r' )
        {
            return c ;
        }
    }
    printf("EXIT_FAILURE_next_char\n") ;
    exit(EXIT_FAILURE) ;
}


void check_next_char(FILE* fd, char c)
{
    if(fd==NULL)
    {
        fprintf(stderr,"cannot open file \n");
        exit(EXIT_FAILURE);
    }
    char a =next_char(fd) ;
    if( a != c )
    {
        printf("EXIT_FAILURE_check_next_char\n") ;
        exit(EXIT_FAILURE) ;
    }
}
bool is_next_char(FILE* fd, char c, bool cons)
{
    if(fd==NULL)
    {
        fprintf(stderr,"cannot open file \n");
        exit(EXIT_FAILURE);
    }
    if ( ! feof(fd))
    {
        char nextChar = next_char(fd);
        if(nextChar  == c )
        {
            if(cons == false  )
            {
                ungetc(nextChar, fd ) ;
            }
            return true ;
        }
        else if(nextChar != c || cons == false)
        {
            ungetc(nextChar, fd ) ;
        }
    }

    return false ;
}


char* next_word(FILE* fd)
{
    if(fd==NULL)
    {
        fprintf(stderr,"cannot open file \n");
        exit(EXIT_FAILURE);
    }
    char e = next_char (fd)  ;
    ungetc(e,fd);

    is_next_char(fd,'<',true);
    is_next_char(fd,'/',true);
    is_next_char(fd,'=',true);
    char* word = malloc(sizeof(char)) ;
    char c=fgetc(fd);
    if(c=='"' )
        c=next_char(fd) ;
    if(c=='>' || c=='/' )
        ungetc(c,fd) ;
    int i = 0 ;
    while( c != '=' && c != '>' && c != '/' && c != ' ' && c != '<' && c != '\n' && c != '\t' && c != '\r' )
    {
        if( feof(fd) )
        {
            printf("le programme échouera si la fin de fichier est rencontrée pendant la lecture\n") ;
            exit(EXIT_FAILURE) ;
        }
        word[i] = c ;
        i++ ;
        word=realloc(word, i+1 ) ;
        c=fgetc(fd);
        if( c == '<' ||  c == '=' || c == '>' || c == '/' || c == ' ' || c == '\n' || c == '\t' || c == '\r'  )
        {
            ungetc(c,fd) ;
        }

    }
    word[i]='\0' ;
    if( word[0]=='\0'  )
    {
        return NULL;
    }
    return word;

}
void check_next_word(FILE* fd, const char* w)
{
    if(fd==NULL)
    {
        fprintf(stderr,"cannot open file \n");
        exit(EXIT_FAILURE);
    }
    char * a = next_word(fd);
    if( strcmp(a,w) != 0 )
    {
        printf ("a=%s,w=%s,\n",a,w);
        printf("EXIT_FAILURE_check_next_word\n") ;
        exit(EXIT_FAILURE) ;
    }
}
char* next_string(FILE* fd)
{
    if(fd==NULL)
    {
        fprintf(stderr,"cannot open file \n");
        exit(EXIT_FAILURE);
    }
    char e = next_char(fd) ;
    ungetc(e,fd);

    is_next_char(fd,'"',true);

    char* word = malloc(sizeof(char)) ;

    char c ;
    int i = 0 ;

    c=fgetc(fd) ;
    while( c != '"' )
    {
        if( feof(fd) )
        {
            exit(EXIT_FAILURE) ;
        }
        word[i] = c ;
        i++ ;
        word=realloc(word, i+1 ) ;
        c=fgetc(fd) ;
        if( c == '"'  )
        {
            ungetc(c,fd) ;
        }
    }
    word[i]='\0' ;
    return word;
}
char* next_raw(FILE* fd)
{
    if(fd==NULL)
    {
        fprintf(stderr,"cannot open file \n");
        exit(EXIT_FAILURE);
    }
    char e = next_char(fd) ;
    ungetc(e,fd);

    is_next_char(fd,'>',true);

    char* word = malloc(sizeof(char)) ;

    char c ;
    int i = 0 ;

    c=fgetc(fd) ;
    while( c != '<' )
    {
        if( feof(fd) )
        {
            exit(EXIT_FAILURE) ;
        }
        word[i] = c ;
        i++ ;
        word=realloc(word, i+1 ) ;
        c=fgetc(fd) ;
        if( c == '<'  )
        {
            ungetc(c,fd) ;
        }
    }
    word[i]='\0' ;

    return word;
}

// load_xml
xelement_t* load_xelement(FILE* fd, const char* end_tag);

void load_xelement_raw(FILE* fd, xelement_t* e)
{
    char* w = next_raw(fd);
    check_next_char(fd, '<');
    check_next_char(fd, '/');
    check_next_word(fd, e->nom);
    check_next_char(fd, '>');
    add_raw(e,w);
    free(w);
}

void load_xelement_sub(FILE* fd, xelement_t* e)
{
    xelement_t* f = load_xelement(fd, e->nom);
    if (f != NULL)
    {
        add_sub_xelement(e,f);
        load_xelement_sub(fd, e);
    }
}

void load_xelement_content(FILE* fd, xelement_t* e)
{

    if (is_next_char(fd,'<',false))
    {
        load_xelement_sub(fd, e);
    }
    else
    {
        load_xelement_raw(fd, e);
    }
}

xelement_t* load_xelement(FILE* fd, const char* end_tag)
{
    xelement_t* e = NULL;
    char c;
    check_next_char(fd,'<');

    if ((end_tag) && (is_next_char(fd,'/',true)))
    {

        check_next_word(fd,end_tag);
        check_next_char(fd,'>');
        return NULL;
    }
    char* name = next_word(fd);
    if (name == NULL)
    {
        fprintf(stderr, "load_xelement: tag name expected\n");
        exit(EXIT_FAILURE);
    }
    e = create_xelement(name);
    free(name);
    while((name = next_word(fd)) != NULL && name[0] != '\0')
    {
        check_next_char(fd,'=');
        char* value = next_string(fd);
        add_xattribute(e,name,value);
        free(name);
        free(value);
    }
    c = next_char(fd);
    if (c == '/')
    {
        check_next_char(fd,'>');
        return e;
    }

    if (c == '>')
    {
        load_xelement_content(fd, e);
        return e;
    }

    fprintf(stderr, "load_xelement: end of markup expected ('>' or '/>'), but got %c\n", c);
    exit(EXIT_FAILURE);
}

xelement_t* load_xml(const char* fname)
{
    FILE* fd = fopen(fname, "r");
    xelement_t* e = load_xelement(fd,NULL);
    fclose(fd);
    return e;
}
/*
int main() {
  xelement_t* catalog;
  xelement_t* book;
  xelement_t* prop;

  catalog = create_xelement("catalog");

  book = create_xelement("book");
  add_sub_xelement(catalog, book);

  add_xattribute(book, "id", "bk102");

  prop = create_xelement("description");
  add_sub_xelement(book, prop);
  add_raw(prop, "A former architect battles corporate zombies,\nan evil sorceress, and her own childhood to become queen\nof the world.");

  prop = create_xelement("publish_date");
  add_sub_xelement(book, prop);
  add_raw(prop, "2000-12-16");

  prop = create_xelement("price");
  add_sub_xelement(book, prop);
  add_raw(prop, "5.95");

  prop = create_xelement("genre");
  add_sub_xelement(book, prop);
  add_raw(prop, "Fantasy");

  prop = create_xelement("title");
  add_sub_xelement(book, prop);
  add_raw(prop, "Midnight Rain");

  prop = create_xelement("author");
  add_sub_xelement(book, prop);
  add_xattribute(prop, "lastname", "Ralls");
  add_xattribute(prop, "firstname", "Kim");

  book = create_xelement("book");
  add_sub_xelement(catalog, book);
  add_xattribute(book, "id", "bk101");

  prop = create_xelement("description");
  add_sub_xelement(book, prop);
  add_raw(prop, "An in-depth look at creating applications\nwith XML.");

  prop = create_xelement("publish_date");
  add_sub_xelement(book, prop);
  add_raw(prop, "2000-10-01");

  prop = create_xelement("price");
  add_sub_xelement(book, prop);
  add_raw(prop, "44.95");

  prop = create_xelement("genre");
  add_sub_xelement(book, prop);
  add_raw(prop, "Computer");

  prop = create_xelement("title");
  add_sub_xelement(book, prop);
  add_raw(prop, "XML Developer's Guide");

  prop = create_xelement("author");
  add_sub_xelement(book, prop);
  add_xattribute(prop, "lastname", "Gambardella");
  add_xattribute(prop, "firstname", "Matthew");

  printf("*** Iteration over book ids (display order does not matter) ***\n"); prl;
  for(xelement_t* elt = get_xelement_children(catalog); elt != NULL; elt = next_xelement_child(elt)) {
    printf("  * Value of attribute 'id': %s\n", get_xelement_attribute_value(elt, "id"));
  }
  prl;

  printf("*** XML display of the catalog (display order does not matter) ***\n");
  prl;
  print_xelement(catalog);
  prl;

  delete_xelement(catalog);

  printf("*** Loading from a file (display order does not matter) ***\n");
  xelement_t* e = load_xml("testxml.xml");
  prl;
  print_xelement(e);
  delete_xelement(e);

  return EXIT_SUCCESS;
}*/
