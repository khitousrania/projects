#ifndef MENU_H
#define MENU_H

typedef enum {
    Action,
    Menu,
    None
}nom_item_t;

typedef struct action_t {
    char * nom_action ;
    void (*f)();

}action_t ;


typedef struct {
    nom_item_t  nom_item ;
    union {
        struct menu* sous_menus ;
        action_t action ;
    };
}item_t ;

typedef struct menu{

    struct menu* par ;
    char* nom_menu ;
    item_t t[9];
    int nb_item ;
    // item_t*  *t ;

}menu_t ;


menu_t* createMenu(const char* text); //création nouveau menu
void addMenuAction(menu_t* m, const char* text, void(*f)());//ajoute au menu m un item de type action
void addSubMenu(menu_t* m, menu_t* sm);//ajoute au menu m un item de type action
void deleteMenu(menu_t* m);//libère la mémoire occupée par le menu m
void launchMenu(menu_t* m);// lance le menu m
void f1();
void f2() ;
void quit();

#endif //tp8.h
