#ifndef WAVE_H
#define WAVE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

// Types

typedef struct wave_s wave_t;
typedef struct {
    int8_t riff[4] ; // {'R','I','F','F,'}
    uint32_t S      ;//Taille S du fichier en octets moins 8 (4 octets)
    int8_t wave[4] ;
    int8_t fmt[4]  ;
    int32_t nbr16  ; //=16
    int16_t PCM    ;
    int16_t c ;
    int32_t f ; //Fréquence f d'échantillonnage en Hertz (4 octets)
    int32_t r ; //Nombre d'octets par seconde $r$ (4 octets)
    uint16_t b ; //Nombre d'octets par bloc $b$ (2 octets)
    int16_t p; //Précision p des échantillons en bits (2 octets : 8, 16 ou 24)
    int8_t data[4] ;
    uint32_t D ; //Taille D des données en octets (4 octets)
} header_t;

struct wave_s {
    header_t h ;
    FILE* fd;
};
// Constructors

wave_t* wave_open(const char* fname);

wave_t* wave_create(const char* fname, size_t f, size_t p, size_t c, size_t B);

// Destructors

void wave_close(wave_t* wave);

// Getters / Setters

void wave_print_info(wave_t* wave);

size_t wave_get_c(wave_t* wave);

size_t wave_get_f(wave_t* wave);

size_t wave_get_p(wave_t* wave);

size_t wave_get_D(wave_t* wave);

size_t wave_get_b(wave_t* wave);

size_t wave_get_r(wave_t* wave);

size_t wave_get_S(wave_t* wave);

size_t wave_get_B(wave_t* wave);

void wave_set(wave_t* w, size_t ch, size_t k, double sig);

double wave_get(wave_t* w, size_t ch, double s);

#endif // WAVE_H
