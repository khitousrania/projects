#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "xml.h"
#include "wave.h"
#include "partition.h"
#include "menu.h"
#include "args.h"

score_t* sc = NULL ;
instrument_t* sounds_bank[50] ;
size_t nd_ins;
char fname[50] ;
char fname1[50] ;


bool quite = false;
bool saver = false;


void new(){

    size_t tempo ;
    size_t duration ;
    size_t sig ;
    size_t nb_ins ;

    printf("Veuillez entrez un tempo\n");
    scanf("%lu",&tempo) ;

    printf("Veuillez entrez une duration\n");
    scanf("%lu",&duration) ;

    printf("Veuillez entrez une signature temporelle\n");
    scanf("%lu",&sig) ;

    printf("Veuillez entrez le nombres d'instruments\n");
    scanf("%lu",&nb_ins) ;

    score_t* scc = score(tempo, duration, sig, nb_ins);

    if(sc != NULL){
        char reponse[1];
        printf("Voulez vous sauvgarder la nouvelle partition (Y: Oui | N: Non):\n");
        scanf("%s", reponse);

        if(reponse[0]=='Y'){
            score_delete(sc);
            sc = scc;
        }
        else{
            score_delete(scc);
        }
    }
    else{
        sc = scc;
    }
    saver = true;
}


void openOpt(const char* filename){

    char fname[50] ;
    if(filename == NULL || strcmp(filename,"")==0){
        printf("Veuillez entrez un nom d'un fichier xml\n");
        scanf("%s",fname);
    }
    else{
        strcpy(fname, filename);
    }

    score_t* scc = score_load_xml(fname);
    if(sc != NULL){
        char reponse[1];
        printf("Voulez vous sauvgarder la nouvelle partition (Y: Oui | N: Non):\n");
        scanf("%s", reponse);

        if(reponse[0]=='Y'){
            sc = scc;
            score_save_xml(sc,fname);
            score_delete(sc);
        }
        else{
            score_delete(scc);
            scc=NULL;
        }
    }
    else{
        sc = scc;
    }
}
void open(){
    openOpt(NULL);
}
void save(){
    char fname[50];

    printf("Veuillez entrez un nom pour sauvegarder la partition courante\n");
    scanf("%s",fname);

    if(sc != NULL){
        score_save_xml(sc,fname);
    }
    else{
        printf("Y a aucune partition dans la memoire\n");
        return ;
    }
}

void information(){
    score_print_info(sc);
}


void quitter() {
    if(saver){
        printf("Voulez vous sauvegarder la partition courante ? (Y: Oui | N: Non):\n");
        char reponse[1];
        scanf("%s", reponse);

        if(reponse[0]=='Y'){
            save();
        }
    }
    score_delete(sc);
    quite = true;
}
void add_note(){
    
    size_t ins ;
    size_t bar ;
    pitch_t p ;
    size_t start ;
    size_t duration ;
    
    printf("Veuillez precisé l'instrument\n");
    scanf("%lu",&ins);
    printf("Veuillez precisé la mesure\n");
    scanf("%lu",&bar);
    printf("Veuillez precisé la hauteur\n");
    scanf("%hhu",&p);
    printf("Veuillez precisé le tic de depart\n");
    scanf("%lu",&start);
    printf("Veuillez precisé la duration\n");
    scanf("%lu",&duration);
    // score_add_note(score_t* sc, size_t ins, size_t bar, pitch_t p, size_t start, size_t duration)
    score_add_note(sc,  ins,  bar,  p, start, duration) ;
    saver = true;
}

void exportopt(const char* filename){
    char fname[50] ;
    if(filename == NULL || strcmp(filename,"")==0){
        printf("Veuillez indiqué le nom d'un fichier WAVE à generer\n");
        scanf("%s",fname);
    }
    else{
        strcpy(fname, filename);
        strcpy(fname1, filename);
    }

    if(sc==NULL){
        printf("ya aucune partition\n");
        return ;
    }
    score_export_wave(sc,sounds_bank,fname) ;
}
void export(){
    exportopt(NULL);
}

void loadInst(int nd_ins, const char* fname, int h, float a, float d){


    sounds_bank[nd_ins] = instrument_sample(fname,h) ;

    instrument_set_attack(sounds_bank[nd_ins], a);
   // if(d<1.0) d=10*d ;
    instrument_set_duration(sounds_bank[nd_ins] , d);

    exportopt(fname1);

}

void load() {

    size_t numero_ins;
    char fname[50] ;
    pitch_t pitch_ref;
    printf("Veuillez entrez un numero d'un instrument\n");
    scanf("%lu",&numero_ins);

    printf("Veuillez entrez le nom d'un fichier WAVE\n");
    scanf("%s",fname);

    printf("Veuillez entrez la note de référence\n");
    scanf("%hhu",&pitch_ref);

    sounds_bank[numero_ins] = instrument_sample(fname,pitch_ref) ;
}
void unload(){
    size_t nd_ins;
    printf("Veuillez entrez un numero d'un instrument\n");
    scanf("%lu",&nd_ins);
    free(sounds_bank[nd_ins]) ;
}


int main(int argc, char** argv){

    int i;
    for(i=0; i<50; i++){
        sounds_bank[i]=NULL;
    }

    if(argc >1){

        option_t* opt = NOOPTION;
        opt = opt_string(opt, "-i" , openOpt);

        opt = opt_string(opt, "-o" , exportopt );

        opt = opt_inst  (opt,"-ins", loadInst );

        process_options(opt, argc, argv);
    }
    else{

        menu_t* m = createMenu("");
        menu_t* file = createMenu("File");

        addMenuAction(file,"New" ,new);
        addMenuAction(file,"Open",open);
        addMenuAction(file,"Save",save);
        addMenuAction(file,"Information",information);
        addMenuAction(file,"Quitter",quitter);
        addSubMenu(m,file);
        menu_t* score = createMenu("score");
        addSubMenu(m,score) ;
        addMenuAction(score,"Add note" ,add_note);

        menu_t* sm = createMenu("Instruments");
        addMenuAction(sm,"Load",load);
        addMenuAction(sm,"Unload",new);
        addSubMenu(score,sm);

        addMenuAction(score,"Export",export);

        while(!quite){
            launchMenu(m);
        }

        deleteMenu(m);

    }
    return EXIT_SUCCESS ;

}

