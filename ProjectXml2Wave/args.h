#ifndef ARGS_H
#define ARGS_H

#define NOOPTION NULL

struct option_s {
    const char* keyword;
    enum { OptVoid, OptInt, OptString, OptFloat, OptInst } spec;
    union {
        void (*opt_void)();
        void (*opt_int)(int);
        void (*opt_str)(const char*);
        void (*opt_float)(float);
        void (*opt_inst)(int, const char*, int, float, float);
    } fct;
    struct option_s* next;
};
typedef struct option_s option_t;

option_t* opt_void(option_t* l, const char* kw, void (*f)());
option_t* opt_int(option_t* l, const char* kw, void (*f)(int));
option_t* opt_float(option_t* l, const char* kw, void (*f)(float));
option_t* opt_string(option_t* l, const char* kw, void (*f)(const char*));

option_t* opt_inst  (option_t* l, const char* kw, void (*f)(int, const char*, int, float, float));

void opt_delete(option_t* l);
void process_options(option_t* l, int argc, char* *argv);

#endif // ARGS_H
