#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "args.h"


option_t* opt_void(option_t* l, const char* kw, void (*f)()){
    if(!l){
        l = malloc(sizeof(option_t));
        l -> next = NOOPTION;
        l -> spec = OptVoid;
        l -> fct.opt_void = f;
        l -> keyword = kw;
    }else{
        option_t* tmp = l;
        while(tmp -> next){
            tmp = tmp -> next;
        }
        tmp -> next = NULL;
        tmp -> next = malloc(sizeof(option_t));
        tmp -> next -> next = NOOPTION;
        tmp -> next -> spec = OptVoid;
        tmp -> next -> fct.opt_void = f;
        tmp -> next -> keyword = kw;
    }
    return l;
}

option_t* opt_int(option_t* l, const char* kw, void (*f)(int)){
    if(!l){
        l = malloc(sizeof(option_t));
        l -> next = NOOPTION;
        l -> spec = OptInt;
        l -> fct.opt_int = f;
        l -> keyword = kw;
    }else{
        option_t* tmp = l;
        while(tmp -> next){
            tmp = tmp -> next;
        }
        tmp -> next = NULL;
        tmp -> next = malloc(sizeof(option_t));
        tmp -> next -> next = NOOPTION;
        tmp -> next -> spec = OptInt;
        tmp -> next -> fct.opt_int = f;
        tmp -> next -> keyword = kw;
    }
    return l;
}

option_t* opt_float(option_t* l, const char* kw, void (*f)(float)){
    if(!l){
        l = malloc(sizeof(option_t));
        l -> next = NOOPTION;
        l -> spec = OptFloat;
        l -> fct.opt_float = f;
        l -> keyword = kw;
    }else{
        option_t* tmp = l;
        while(tmp -> next){
            tmp = tmp -> next;
        }
        tmp -> next = NULL;
        tmp -> next = malloc(sizeof(option_t));
        tmp -> next -> next = NOOPTION;
        tmp -> next -> spec = OptFloat;
        tmp -> next -> fct.opt_float = f;
        tmp -> next -> keyword = kw;
    }
    return l;
}

option_t* opt_string(option_t* l, const char* kw, void (*f)(const char*)){
    if(!l){
        l = malloc(sizeof(option_t));
        l -> next = NOOPTION;
        l -> spec = OptString;
        l -> fct.opt_str = f;
        l -> keyword = kw;
    }else{
        option_t* tmp = l;
        while(tmp -> next){
            tmp = tmp -> next;
        }
        tmp -> next = NULL;
        tmp -> next = malloc(sizeof(option_t));
        tmp -> next -> next = NOOPTION;
        tmp -> next -> spec = OptString;
        tmp -> next -> fct.opt_str = f;
        tmp -> next -> keyword = kw;
    }
    return l;
}


option_t* opt_inst  (option_t* l, const char* kw, void (*f)(int, const char*, int, float, float)){
    if(!l){
        l = malloc(sizeof(option_t));
        l -> next = NOOPTION;
        l -> spec = OptInst;
        l -> fct.opt_inst = f;
        l -> keyword = kw;
    }else{
        option_t* tmp = l;
        while(tmp -> next){
            tmp = tmp -> next;
        }
        tmp -> next = NULL;
        tmp -> next = malloc(sizeof(option_t));
        tmp -> next -> next = NOOPTION;
        tmp -> next -> spec = OptInst;
        tmp -> next -> fct.opt_inst = f;
        tmp -> next -> keyword = kw;
    }
    return l;
}


void opt_delete(option_t* l){
    if(l -> next) opt_delete(l -> next);
    free(l);
}

void process_options(option_t* l, int argc, char* *argv){

    option_t* curr;
    curr = l;
    int i=1;

    for(i=1; i+1<argc; i++){
        curr = l;
        while(curr != NULL){

            if((strcmp(curr->keyword, argv[i]) == 0 && strcmp(curr->keyword, argv[i+1]) != 0)){

                int optType =  curr->spec;

                switch (optType) {

                    case  OptVoid: curr->fct.opt_void(); break;

                    case  OptInt: curr->fct.opt_int(atoi(argv[i+1]));  break;

                    case OptString:curr->fct.opt_str(argv[i+1]);  break;

                    case OptFloat: curr->fct.opt_float(atof(argv[i+1])); break;

                    case OptInst: curr->fct.opt_inst(atoi(argv[i+1]), argv[i+2], atoi(argv[i+3]), atof(argv[i+4]), atof(argv[i+5])); break;

                    default: break;
                }
            }

            curr = curr->next;
        }

    }
}
/*
void f1(const char* str) {
    printf("F1: %s\n", str);
}

void f2(int i) {
    printf("F2: %d\n", i);
}

void f3() {
    printf("F3: no param\n");
}

void f4(float f) {
    printf("F4: %f\n", f);
}


int main(int argc, char** argv) {
    option_t* opt = NOOPTION;
    opt = opt_string(opt, "-a", f1);
    opt = opt_int   (opt, "-b", f2);
    opt = opt_void  (opt, "-c", f3);
    opt = opt_float (opt, "-d", f4);

    process_options(opt, argc, argv);

    opt_delete(opt);

    return EXIT_SUCCESS;
}*/
