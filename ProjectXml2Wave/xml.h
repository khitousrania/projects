#ifndef XML_H
#define XML_H

#include <stdio.h>

// Types

typedef struct xattribute_s {
    char * nom ;
    char * valeur ;
    struct xattribute_s* suiv ;

}xattribute_t;


typedef struct xelement_s xelement_t;

struct xelement_s{

    char * nom ;
    xattribute_t * attribut;

    xelement_t   *pere  ;
    xelement_t   *frere ;

    enum {
        Raw,
        Fils,
    }type_contenu;

    union {
        char        * raw ; // soit une donnée brute (chaîne de caractères) ou raw en anglais,
        xelement_t  * fils ; // soit une liste d'éléments fils dont il est le père. est ce que xelement_t   * fils est une liste !!!!?
    }contenu;
};

// Constructors

xelement_t* create_xelement(const char* name);

void add_xattribute(xelement_t* e, const char* name, const char* value);

void add_sub_xelement(xelement_t* e, xelement_t* s);

void add_raw(xelement_t* e, const char* r);

// Destructors

void delete_xelement(xelement_t* e);

// Accessors

const char* get_xelement_name(xelement_t* e);

const char* get_xelement_attribute_value(xelement_t* e, const char* attribute_name);

xelement_t* get_xelement_children(xelement_t* e);

xelement_t* next_xelement_child(xelement_t* e);

const char* get_xelement_rawdata(xelement_t* e);

// Input/output

void save_xml(const char* fname, xelement_t* e);

void print_xelement(xelement_t* e);

xelement_t* load_xml(const char* fname);

#endif
