#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "wave.h"
#include "xml.h"
#include "partition.h"

#define prl { printf("\n"); }

//hauteur
pitch_t pitch(pitchclass_t c, char o){
    pitch_t h ;
    h=12*(o+1)+c;
    return h;
}

char pitch_octave(pitch_t h){
    char o =(h/12)-1;
    return o ;
}
pitchclass_t pitch_class(pitch_t h){
    pitchclass_t c = h%12;
    return c;
}

void pitch_print(pitch_t h){
    int a = pitch_class(h) ;
    switch (a) {
    case 0 : printf("C") ; printf("%hhu",pitch_octave(h)); break ;
    case 1 : printf("C#") ; break ;
    case 2 : printf("D") ;printf("%hhu",pitch_octave(h)); break ;
    case 3 : printf("D#") ; break ;
    case 4 : printf("E") ;printf("%hhu",pitch_octave(h)); break ;
    case 5 : printf("F") ;  printf("%hhu",pitch_octave(h)); break ;
    case 6 : printf("F#") ; break ;
    case 7 : printf("G") ; printf("%hhu",pitch_octave(h)); break ;
    case 8 : printf("G#") ; break ;
    case 9 : printf("A") ; printf("%hhu",pitch_octave(h)); break ;
    case 10 : printf("A#") ; break ;
    case 11: printf("B") ; printf("%hhu",pitch_octave(h)); break ;
    default: printf("ERROR pitch_print Unknown Pitch\n"); break;
    }
}

double pitch_frequency(pitch_t h){

    double f=440*pow (R,h-69) ;
    return f;
}

//partition ;
void inverser_liste(bar_t *l){
    if(*l==NULL)return ;

    bar_t ptr =  NULL ;
    bar_t tmp = *l ;

    while ((*l)->next != NULL ){
        *l=(*l)->next;
        tmp->next = ptr ;
        ptr = tmp ;
        tmp = *l ;
    }
    (*l)->next = ptr ;

    return ;
}
void inverser_xelement(xelement_t* *l){
    if(*l==NULL)return ;

    xelement_t* ptr =  NULL ;
    xelement_t* tmp = *l ;

    while ((*l)->frere != NULL ){
        *l=(*l)->frere;
        tmp->frere = ptr ;
        ptr = tmp ;
        tmp = *l ;
    }
    (*l)->frere = ptr ;

    return ;
}


score_t* score(size_t tempo, size_t duration, size_t sig, size_t nbInstr){

    score_t* s = malloc(sizeof(score_t)) ;

    s->tempo= tempo/60;
    s->duration=duration;
    s->sig=sig;
    s->nbins=nbInstr ;
    s->systeme= (staff_t*) malloc(nbInstr * sizeof(staff_t));

    size_t i;
    for (i=0 ; i<nbInstr ; i++ ) {
        s->systeme[i].bars = NULL;
    }
    return s ;
}
void score_add_note(score_t* sc, size_t ins, size_t bar, pitch_t p, size_t start, size_t duration){

    if(sc == NULL || bar >= sc->duration || ins >= sc->nbins){
        fprintf(stderr, "sc == NULL || bar  sc->duration\n");
        return ;
    }

    if(sc->systeme[ins].bars == NULL){

        sc->systeme[ins].bars = (bar_t*) malloc (sc->duration * sizeof(bar_t));
        size_t i ;
        for(i=0 ; i<sc->duration ; i++) {
            sc->systeme[ins].bars[i]=NULL ;
        }

    }

    if(sc->systeme[ins].bars[bar]==NULL && bar < sc->duration){

        sc->systeme[ins].bars[bar] =(bar_t)malloc(sizeof(note_t)) ;
        sc->systeme[ins].bars[bar]->debut=start ;
        sc->systeme[ins].bars[bar]->duree = duration ;
        sc->systeme[ins].bars[bar]->hauteur = p ;
        sc->systeme[ins].bars[bar]->next = NULL ;

        return ;
    }
    /*
    bar_t tmp = sc->systeme[ins].bars[bar] ;
    bar_t prec = NULL ;

    while ( tmp !=NULL ) {
        prec = tmp ;
        tmp = tmp->next ;
    }

    tmp = malloc(sizeof(note_t)) ;
    tmp->debut=start ;
    tmp->duree = duration ;
    tmp->hauteur = p ;
    tmp->next = NULL ;
    prec->next = tmp ;
    */


    bar_t n  = malloc(sizeof(note_t)) ;
    n->debut = start ;
    n->duree = duration ;
    n->hauteur = p ;
    n->next = sc->systeme[ins].bars[bar] ;
    sc->systeme[ins].bars[bar]= n ;

    return ;
}
void score_print_info(score_t* sc){
    if(sc == NULL ) return ;
    printf("Tempo: %lu beat(s) per minute\n",sc->tempo * 60 / 12 ) ;
    printf("Duration: %.1f sec\n",((float)sc->sig / sc->tempo) * sc->duration ) ;
    printf("Instrument(s): %lu \n", sc->nbins) ;
    size_t  i ;
    for(i=0 ; i < sc->nbins ; i++ ) {
        printf("\t* Instrument %ld \n", i ) ;

        size_t j= 0 ;

        if(sc->systeme[i].bars == NULL){
            printf("\t  - Bar %ld :  ",j+1) ;
            prl ;
        }
        else{
            for(j=0 ; j < sc->duration; j++) {
                printf("\t  - Bar %ld :  ",j+1) ;
                bar_t tmp = sc->systeme[i].bars[j] ;
                //inverser_liste(&tmp);
                while(tmp !=NULL ){
                    pitch_print(tmp->hauteur); printf(" ") ;
                    tmp=tmp->next ;
                }
                prl ;

            }
        }
    }
}

void bar_delete( bar_t* b ) {
    if(b == NULL || *b==NULL ) return ;
    else {
        bar_delete( &((*b)->next) ) ;
        free(*b) ;
        *b = NULL;
        return ;
    }
}
void bart_delete(bar_t* b ) {
    if(b==NULL ) return ;
    else {
        free(b) ;
        b=NULL ;
        return ;
    }
}

void staff_delete( staff_t* s ) {

    if(s == NULL){

        return ;
    }
    else{
        free(s) ;
        s=NULL;
    }

}

void score_delete(score_t* sc){

    if(sc == NULL ) return ;
    size_t i,j;
    for(i=0 ; i < sc->nbins ; i++){
        if(sc->systeme[i].bars!=NULL){
            for(j=0 ; j < sc->duration  ; j++ ) {

                bar_delete(&(sc->systeme[i].bars[j]));

            }
            bart_delete(sc->systeme[i].bars);

        }
    }
    staff_delete(sc->systeme) ;
    free(sc) ;
    sc=NULL ;
}

void score_save_xml(score_t* sc, const char* fname){
    FILE* fd = fopen (fname,"w") ;
    if(fd==NULL){
        fprintf(stderr,"cannot open file %s in mode w\n", fname);
        exit(EXIT_FAILURE);
    }

    fprintf(fd,"<score tempo=\"%lu\" duration=\"%lu\" timeSignature=\"%lu\" nbInstrument=\"%lu\">\n",(sc->tempo)*60,sc->duration,sc->sig,sc->nbins) ;
    int i ;
    for(i=sc->nbins - 1  ; i>=0 ; i--) {
        fprintf(fd,"<instrument id=\"%d\">\n",i+1 ) ;
        int j ;

        for(j = sc->duration -1 ; j>=0 && sc->systeme[i].bars != NULL; j--) {


            bar_t n = sc->systeme[i].bars[j] ;

            // inverser_liste(&(n));
            int nbNote = 0 ;
            if(n!=NULL && n->next != NULL){
                nbNote++ ;
            }

            if(nbNote>0)
                fprintf(fd,"<bar id=\"%d\">\n",j+1) ;
            else
                fprintf(fd,"<bar id=\"%d\"/>\n",j+1) ;

            while(n!=NULL){
                fprintf(fd,"<note pitch=\"%hhu\" start=\"%lu\" duration=\"%lu\"/>\n",n->hauteur,n->debut,n->duree);
                n=n->next ;
            }
            if(nbNote>0)
                fprintf(fd,"</bar>\n");

        }
        fprintf(fd,"</instrument>\n") ;
    }
    fprintf(fd,"</score>\n");


    fclose(fd) ;

}
size_t* get_attribute_values(xattribute_t* att, size_t* nbValues) {
    if(att==NULL){
        exit(EXIT_FAILURE ) ;
    }
    size_t*  values = malloc (sizeof(size_t ) ) ;
    size_t i = 0 ;
    xattribute_t* a = att ;

    while(a!= NULL){
        values = realloc(values, (i+1)*sizeof(size_t));
        values[i]=atoi(a->valeur) ;
        a=a->suiv ;
        i++;
    }
    *nbValues=i;
    return values ;
}

score_t* score_load_xml(const char* fname){

    FILE* fd = fopen (fname , "r" ) ;
    if(fd==NULL){
        fprintf(stderr,"cannot open file %s in mode r\n", fname);
        exit(EXIT_FAILURE);
    }
    score_t* s=NULL;
    xelement_t* e = load_xml(fname);

    if(e==NULL || strcmp(e->nom , "score" )!=0 ) {
        printf("is not xml partition\n") ;
        exit(EXIT_FAILURE);
    }
    size_t nbValues;
    size_t* scoreValues=get_attribute_values(e->attribut, &nbValues);

    if(nbValues == 4)
        s=score(scoreValues[0],scoreValues[1],scoreValues[2],scoreValues[3]) ;
    else{
        printf("is not xml partition nbValues !=4\n") ;
        exit(EXIT_FAILURE);
    }

    xelement_t* eins= e->contenu.fils ;

    size_t i, j ;
    for(i=0; i<s->nbins && eins != NULL; i++){
        xelement_t* ebar= eins->contenu.fils ;
        for(j=0; j<s->duration && ebar != NULL; j++ ){
            xelement_t* enote = ebar->contenu.fils ;
            //inverser_xelement(&(enote)) ;
            while(enote != NULL){
                size_t* noteValues = get_attribute_values(enote->attribut, &nbValues);
                if(nbValues != 3) continue;
                score_add_note(s, i, j, noteValues[0], noteValues[1], noteValues[2]);
                enote=enote->frere ;
            }
            ebar=ebar->frere ;
        }
        eins=eins->frere ;

    }
    fclose(fd) ;
    return s ;

}

void display(const char* fn) {
    FILE* fd = fopen(fn, "r");

    if (fd == NULL) {
        fprintf(stderr, "display: cannot open %s for reading\n", fn);
        return;
    }

    char buff[100];
    size_t in;

    while((in = fread(buff, 1, 100, fd)) > 0) {
        fwrite(buff, in, 1, stdout);
    }

    fclose(fd);
}

instrument_t* instrument_signal(double (*sig)(double freq, double sec)){
    instrument_t* ins = (instrument_t*) malloc ( sizeof(instrument_t)) ;
    ins->volume = 1.0 ;
    ins->attaque = 0.0 ;
    ins->duree = INFINITY ;
    ins->type_signal = Signal ;
    ins->signal.sig = sig ;

    return ins ;
}
instrument_t* instrument_sample(const char* fname, pitch_t pitch_ref){
    instrument_t* ins = (instrument_t*) malloc ( sizeof(instrument_t)) ;
    ins->volume = 1.0 ;
    ins->attaque = 0.0 ;
    ins->duree = INFINITY ;
    ins->type_signal = Sample ;
    ins->signal.sample.hauteur = pitch_ref ;
    ins->signal.sample.wave = wave_open(fname) ;

    return ins ;
}

void instrument_delete(instrument_t* ins){
    if(ins==NULL) return ;
    else {
        if(ins->type_signal == Sample ) {
            wave_close(ins->signal.sample.wave);
        }
        free(ins) ;
        ins=NULL ;
    }
}

void instrument_set_volume(instrument_t* ins, double volume){
    if(ins==NULL)return ;
    ins->volume = volume ;
}
void instrument_set_attack(instrument_t* ins, double attack){
    if(ins==NULL)return ;
    ins->attaque = attack ;

}

void instrument_set_duration(instrument_t* ins, double duration){
    if(ins==NULL)return ;
    ins->duree = duration ;
}


double compute_attenuation(double attack, double duration, double s) {
    double attenuation;
    if (s < attack) {
        attenuation = s / attack;
    } else if (s <= duration) {
        attenuation = (duration == INFINITY) ? 1.0 : (duration - s) / (duration - attack);
    } else {
        attenuation = 0.0;
    }
    return attenuation;
}
double instrument_play(instrument_t* ins, pitch_t n, double s){
    double attenuation =  compute_attenuation(ins->attaque, ins->duree, s) ;
    double base ;

    if(ins->type_signal == Signal ) {
        base = ins->signal.sig(pitch_frequency(n), s) ;
    }

    if(ins->type_signal == Sample ) {
        wave_t* w = ins->signal.sample.wave ;
        pitch_t ref = ins->signal.sample.hauteur ;

        base = wave_get(w, 0, (pitch_frequency(n) / pitch_frequency(ref)) * s) ;
    }
    double play=((ins->volume) * base * attenuation);
    return play ;


}

void score_export_wave(score_t* sc, instrument_t** sounds_bank, const char* fname) {
    size_t tempo    = sc->tempo ;   // Tempo of score sc
    size_t duration = sc->duration ;   // Duration (= number of bars) of score sc
    size_t timeSig  = sc->sig ;      // Time signature of score sc
    size_t nbInstr  = sc->nbins ;   // Number of instruments of score sc


    size_t freq = 44100;                                   // Desired sampling frequency
    size_t splPerTick = freq / tempo;                      // Number of samples per tick
    freq = splPerTick * tempo;                             // Real sampling frequency
    size_t nbSample = splPerTick * duration * timeSig;     // Total number of samples

    double period = 1.0 / freq;

    wave_t* w = wave_create(fname, freq, 16, 1, nbSample);
    size_t i ;
    size_t s ;
    size_t t ;
    size_t b ; 
    
    size_t ss = 0,z=0;
    for(b = 0; b < duration; b++) {
        for( t = 0; t < timeSig; t++) {
            for( s = 0; s < splPerTick; s++) {
                double x =  0;
                for( i = 0; i < nbInstr; i++) {
                    if (sounds_bank[i] == NULL) continue;
		    bar_t n;
                    for(n = sc->systeme[i].bars[b] ; n!=NULL; n=n->next) { // For each note n of bar b of instrument i (can be a while loop)
                        size_t  n_start    =n->debut ;  // Start tick of note n
                        size_t  n_duration = n->duree ;  // Duration of note n in ticks
                        pitch_t n_pitch    = n->hauteur ;  // Pitch of note n
                        if (n_start <= t && t < n_start + n_duration) {
                            z++;
                            x = x + instrument_play(sounds_bank[i], n_pitch, period * (s + splPerTick * (t - n_start)));
                        }
                    }
                }
                wave_set(w, 0, ss, x);
                ss++;
            }
        }
    }
    wave_close(w);
}


double signal_sine(double freq, double s) {
    return sin(2.0 * M_PI * freq * s);
}

double signal_violin(double freq, double s) {
    double r = 0.0;
    r += 0.995 * sin(2.0 * M_PI * 1.0 * freq * s);
    r += 0.940 * cos(2.0 * M_PI * 2.0 * freq * s);
    r += 0.425 * sin(2.0 * M_PI * 3.0 * freq * s);
    r += 0.480 * cos(2.0 * M_PI * 4.0 * freq * s);
    r += 0.365 * cos(2.0 * M_PI * 6.0 * freq * s);
    r += 0.040 * sin(2.0 * M_PI * 7.0 * freq * s);
    r += 0.085 * cos(2.0 * M_PI * 8.0 * freq * s);
    r += 0.090 * cos(2.0 * M_PI * 10.0 * freq * s);
    r *= 0.49;
    return r;
}
/*
int main() {

    printf("*** Pitch implementation ***"); prl; prl;

    pitch_print(pitch(C,4)); prl;
    pitch_print(69); prl;
    printf("Octave of pitch 69: %hhd", pitch_octave(69)); prl;
    printf("Class of pitch 69: %d", pitch_class(69)); prl;
    printf("A0: %lf", pitch_frequency(pitch(A,0))); prl;
    printf("C4: %lf", pitch_frequency(pitch(C,4))); prl;
    printf("C8: %lf", pitch_frequency(pitch(C,8))); prl;

    prl;
    printf("*** Score implementation ***");
    prl;

    score_t* sc = score(100*Quarter, 1, 4*Quarter, 2);
    score_print_info(sc);
    prl;

    score_add_note(sc, 0, 0, pitch(C,4), 0*Eighth, Eighth);
    score_add_note(sc, 0, 0, pitch(D,4), 1*Eighth, Eighth);
    score_add_note(sc, 0, 0, pitch(E,4), 2*Eighth, Eighth);
    score_add_note(sc, 0, 0, pitch(F,4), 3*Eighth, Eighth);
    score_add_note(sc, 0, 0, pitch(G,4), 4*Eighth, Eighth);
    score_add_note(sc, 0, 0, pitch(A,4), 5*Eighth, Eighth);
    score_add_note(sc, 0, 0, pitch(B,4), 6*Eighth, Eighth);
    score_add_note(sc, 0, 0, pitch(C,5), 7*Eighth, Eighth);

    score_add_note(sc, 1, 0, pitch(C,2), 0*Eighth, Eighth);
    score_add_note(sc, 1, 0, pitch(G,2), 1*Eighth, Eighth);
    score_add_note(sc, 1, 0, pitch(C,2), 2*Eighth, Eighth);
    score_add_note(sc, 1, 0, pitch(F,2), 3*Eighth, Eighth);
    score_add_note(sc, 1, 0, pitch(C,2), 4*Eighth, Eighth);
    score_add_note(sc, 1, 0, pitch(F,2), 5*Eighth, Eighth);
    score_add_note(sc, 1, 0, pitch(G,2), 6*Eighth, Eighth);
    score_add_note(sc, 1, 0, pitch(C,2), 7*Eighth, Eighth);

    score_print_info(sc);
    prl;

    prl;
    printf("*** XML export of score ***");
    prl;
    prl;

    score_save_xml(sc, "c_major_scale.xml");
    display("c_major_scale.xml");

    prl;
    printf("*** XML import of score ***");
    prl;
    prl;

    score_t* sc2 = score_load_xml("au_clair_de_la_lune.xml");
    score_print_info(sc2);

    prl;
    printf("*** Instrument implementation (listen to it!) ***");
    prl;
    prl;

    instrument_t* sb[6];

    sb[0] = instrument_signal(signal_sine);

    sb[1] = instrument_signal(signal_violin);
    instrument_set_attack(sb[1], 0.08);
    instrument_set_duration(sb[1], 2.0);

    sb[2] = instrument_sample("313-grand_piano-c4-60.wav", 60);
    instrument_set_attack(sb[2], 0.002);
    instrument_set_duration(sb[2], 1.0);

    sb[3] = instrument_sample("318-grand_piano-d2-38.wav", 38);
    instrument_set_attack(sb[3], 0.002);
    instrument_set_duration(sb[3], 2.0);

    sb[4] = instrument_sample("316-grand_piano-c3-48.wav", 48);
    instrument_set_attack(sb[4], 0.002);
    instrument_set_duration(sb[4], 2.0);

    sb[5] = instrument_sample("132-cello-b2-59.wav", 59);
    instrument_set_attack(sb[5], 0.075);
    instrument_set_duration(sb[5], 2.0);

    wave_t* w = wave_create("ins.wav", 44100, 16, 1, 8*44100);
    for(size_t b = 0; b < 2*44100; b++) {
        double s = b / 44100.0;
        wave_set(w, 0, b, instrument_play(sb[0], pitch(A,4), s));
    }
    for(size_t b = 2*44100; b < 4*44100; b++) {
        double s = b / 44100.0 - 2.0;
        wave_set(w, 0, b, instrument_play(sb[1], pitch(A,4), s));
    }
    for(size_t b = 4*44100; b < 6*44100; b++) {
        double s = b / 44100.0 - 4.0;
        wave_set(w, 0, b, instrument_play(sb[2], pitch(A,4), s));
    }
    for(size_t b = 6*44100; b < 8*44100; b++) {
        double s = b / 44100.0 - 6.0;
        wave_set(w, 0, b, instrument_play(sb[3], pitch(A,2), s));
    }
    wave_close(w);

    prl;
    printf("*** WAVE synthesis of score (listen to it!) ***");
    prl;
    prl;

    score_export_wave(sc, sb+2, "c_major_scale.wav");
    score_delete(sc);

    score_export_wave(sc2, sb+1, "au_clair_de_la_lune.wav");
    score_delete(sc2);

    score_t* sc3 = score_load_xml("gymnopedie.xml");
    score_export_wave(sc3, sb+4, "gymnopedie.wav");
    score_delete(sc3);

    return EXIT_SUCCESS;
}*/

