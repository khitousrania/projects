#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "menu.h"

menu_t* createMenu(const char* text) {
    menu_t *m = malloc(sizeof(menu_t));
    m->par=NULL ;

    char* desc =(char*)malloc(strlen(text)+1) ;
    desc=strcpy(desc,text);
    m->nom_menu=desc ;
    int i = 0 ;
    for(i = 0 ; i< 9 ; i++ ) {
        m->t[i].nom_item = None ;

    }
    m->nb_item = 0 ;


    return m ;
}
void addMenuAction(menu_t* m, const char* text, void(*f)()){

    if(m->nb_item>=9){
        printf("error 9 items maximun\n");
        exit(1);
    }
    m->t[m->nb_item].nom_item=Action ;
    char* desc =(char*)malloc(strlen(text)+1) ;
    desc=strcpy(desc,text);
    m->t[m->nb_item].action.nom_action = desc ;

    m->t[m->nb_item].action.f=f;

    m->nb_item++;

}
void addSubMenu(menu_t* m, menu_t* sm){
    if(sm->par!=NULL){
        printf("error sm n'est pas un sous_menus\n");
        exit(1) ;
    }
    sm->par=m ;
    m->t[m->nb_item].nom_item=Menu ;
    m->t[m->nb_item].sous_menus=sm ;

    m->nb_item++;

}
void deleteMenu(menu_t *m){

    if(m == NULL || m->par!=NULL ) {
        return ;
    }
    else {
        int i ;
        for(i=0 ; i<m->nb_item ;i++){
            if(m->t[i].nom_item==Action) {
                free(m->t[i].action.nom_action) ;
            }
            if( m->t[i].nom_item==Menu ) {
                m->t[i].sous_menus->par = NULL;
                deleteMenu(m->t[i].sous_menus);
            }
        }
        free(m->nom_menu);
        free(m);
    }
}

void launchMenu(menu_t* m){

    printf("%s\n",m->nom_menu) ;

    int i ;
    for(i=0 ; i < m->nb_item ; i++){

        if(m->t[i].nom_item == Menu) {
            printf("\t%d - %s\n",i+1, m->t[i].sous_menus->nom_menu);
        }
        if(m->t[i].nom_item == Action){
            printf("\t%d - %s\n",i+1, m->t[i].action.nom_action);
        }
    }
    char b[1];
    printf("Choice?\n");
    scanf( "%s",b);

    int a = (int) b[0] - '0' ;
    if(b[0]=='p'){
        if(m->par != NULL ) {
            launchMenu(m->par);
        }
        else{
            printf("le menu n'a pas de menu parent \n" ) ;
            launchMenu(m) ;
        }
    }

    else{

        if(a>0 && a<=9 ){
            if(m->t[a-1].nom_item==Action ){
                (m->t[a-1].action.f)();
            }
            if(m->t[a-1].nom_item==Menu ){
                return launchMenu(m->t[a-1].sous_menus ) ;
            }

            if (m->t[a-1].nom_item==None) {
                printf("l'item est un None\n");
                launchMenu(m);
            }
        }
    }

}
/*
void f1() {
  printf("Functionality 1 is called\n");
}

void f2() {
  printf("Functionality 2 is called\n");
}

bool cont = true;

void quit() {
  cont = false;
}

int main() {
  menu_t* m;
  menu_t* sm;

  m = createMenu("Main menu");

  addMenuAction(m,"Apply functionnality 1",f1);
  addMenuAction(m,"Apply functionnality 2",f2);

  sm = createMenu("First submenu");
  addMenuAction(sm,"Apply functionnality 2",f2);
  addSubMenu(m,sm);

  sm = createMenu("Second submenu");
  addMenuAction(sm,"Apply functionnality 1",f1);
  addSubMenu(m,sm);

  addMenuAction(m,"Quit",quit);

  while(cont) launchMenu(m);

  deleteMenu(m);

  return EXIT_SUCCESS;
}*/
