#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <string.h>
#include <features.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <ctype.h>
#include <stdarg.h>
#include <linux/input.h>
#include <poll.h>


#ifndef TIOCGWINSZ
#include <sys/ioctl.h>
#endif

/*** defines ***/
#define VIM_TAB_STOP 6
#define VIM_VERSION "Projet Vim"

#define ERROR_MSG_32  "E32: Aucun nom de fichier"
#define ERROR_MSG_37  "E37: Modifications non enregistrées (ajoute  ! pour passer outre)"
#define ERROR_MSG_492 "E492: Commande inconnue:"

#define MOUSEFILE ""
enum editorKey {
  BACK_SPACE =  127,
  ENTER = 13, 
  ARROW_LEFT = 1000, //flech gauche 
  ARROW_RIGHT, //flech droite
  ARROW_UP, //flech haut
  ARROW_DOWN, //flech bas.
  DEL_KEY,  
  HOME_KEY, // debut
  END_KEY, // fin
  PAGE_UP, // haut
  PAGE_DOWN // bas
}; 

enum editorHighlight {
  HL_NORMAL = 0,
  HL_NUMBER,
  HL_MATCH
};


/*** data ***/

typedef struct erow { //stocker une ligne de texte dans notre éditeur structure de l'éditeur 
  char *chars; // la chaine lu dans le fichier 
  int size; // taille de chaine lu dans le fichier
  char *render; //copie la chaine de caractaire chars et gere la tabulation lu .
  int rsize;  //nouvelle taile de render 
 
  unsigned char *hl;// definition de chaque surbrillance pour chaque 
} erow;

struct editorConfig {
  int cx, cy; //cordonné du curcur
  int rx;// même que cx. S'il y a des tabulations dans chars on utilise rx le nouvaux index qui gere la tabulation 
  int rowoff; 
  int coloff;
  int dirty ; 
  int screencur ; 
  int screencurbar ; // gestion du curcur sur  l’invite de commandes complexes 
  int screenrows;  //ligne de l'ecrans
  int screencols; // collonne de l'ecrans 
  int numrows;  //nombre de lecture de la ligne dans le fichier 
  erow *row; //tablaux de lignes lus dans le fichier 
  char *filename; // nom du fichier a ouvrir
  char statusmsg[80] ; // renvoyée apre avoir passé un commande 
  int errorsmsg; // indicateur erreur
  struct termios orig_termios; 
  enum  {
    Mode_Normal,
    Mode_INSERTION,
    Mode_Complex_command_prompt
  }Mode;
  char* oper;  

};  
struct editorConfig E;

/*** prototypes ***/

void editorSetStatusMessage(const char *fmt, ...);
void editorRefreshScreen();
char *editorPrompt(char *prompt, void (*callback)(char *, int));

//struct termios orig_termios;

/*** terminal ***/


/*** fonction pour la gestion d'error ***/
void die(const char *s) {
    write(STDOUT_FILENO, "\x1b[2J", 4);
    write(STDOUT_FILENO, "\x1b[H", 3);
    perror(s);
    exit(1);
}
//desactivation du mode row 
void disableRawMode() {
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &E.orig_termios) == -1)
    die("tcsetattr");
}
//activation du mode row 
void enableRawMode() {
  if (tcgetattr(STDIN_FILENO, &E.orig_termios) == -1) die("tcgetattr");
  atexit(disableRawMode);
  struct termios raw = E.orig_termios;
  cfmakeraw(&raw);
  raw.c_cc[VMIN] = 0;
  raw.c_cc[VTIME] = 1;
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) die("tcsetattr");
}
// fonction pour la lecture des entrer clavier et sortie 
int editorReadKey() {
  int nread ;
  char c;
  int fd_mouse;
  struct input_event event;
  unsigned char *ptr = (unsigned char*)&event; 
  //unsigned char button/*,bLeft,bMiddle,bRight*/;
  char /*x,*/y;               // the relX , relY datas
  //int absolute_x,absolute_y;
  fd_mouse = open("/dev/input/mice", O_RDONLY ) ; 
  if(fd_mouse == -1)
  { //printf("NonBlocking %s open ERROR\n",MOUSEFILE);
    die("NonBlocking open ERROR");
  }
  struct pollfd fds[2] ; 
  fds[0].fd = STDIN_FILENO ;
  fds[1].fd = fd_mouse ;
  fds[0].events=POLLIN ;  
  fds[1].events=POLLIN ; 
   while(1)
   {       
    poll(fds,2,1);
    if(fds[0].revents&POLLIN){
      while ((nread = read(fds[0].fd, &c, 1)) != 1) {
        if (nread == -1 && errno != EAGAIN) die("read");
      }

      if (c == '\x1b') {
        char data[3];
        if (read(fds[0].fd, &data[0], 1) != 1) {
          E.Mode=Mode_Normal ; 
          return '\x1b';
        }
        if (read(fds[0].fd, &data[1], 1) != 1){
          return '\x1b';
        }
        if (data[0] == '[') {
          if (data[1] >= '0' && data[1] <= '9') {
            if (read(fds[0].fd, &data[2], 1) != 1) return '\x1b';
            if (data[2] == '~') {
              switch (data[1]) {
                case '1': return HOME_KEY;
                case '3': return DEL_KEY;
                case '4': return END_KEY;
                case '5': return PAGE_UP;
                case '6': return PAGE_DOWN;
                case '7': return HOME_KEY;
                case '8': return END_KEY;
              }
            }
          } else {
            switch (data[1]) {
              case 'A': return ARROW_UP;
              case 'B': return ARROW_DOWN;
              case 'C': return ARROW_RIGHT;
              case 'D': return ARROW_LEFT;
              case 'H': return HOME_KEY;
              case 'F': return END_KEY;
            }
          }
        } else if (data[0] == 'O') {
          switch (data[1]) {
            case 'H': return HOME_KEY;
            case 'F': return END_KEY;
          }
        }
        return ARROW_RIGHT;
      } else if(c==127){
        return BACK_SPACE;
      } else if(c==13){
        return ENTER;
      } else if(E.Mode==Mode_Normal && c=='i' ){
        E.Mode=Mode_INSERTION ; 
        return '\x1b' ;
      } else {
        return c;
      }
    }
    if(fds[1].revents & POLLIN){
     nread = read(fds[1].fd, &event, sizeof(struct input_event)); 
      if (nread == -1 && errno != EAGAIN) die("read");

        y=(char) ptr[2];
        if(y<0){
          return ARROW_UP  ; 
        }
        if(y>0){
          return ARROW_DOWN  ;
        }

        
    }
  }
}
//fonction pour la recuperation de la taille du terminal 
int getWindowSize(int *rows, int *cols ,int fd) {
  struct winsize ws;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
    return -1 ; 
  } else {
    *cols = ws.ws_col;
    *rows = ws.ws_row;
    return 0;
  }
}
// handler on cas de signal SIGWINCH captuter et mise a jour de la taille du terminal
void sig_winch(int signo) {
  getWindowSize(&E.screenrows, &E.screencols,STDIN_FILENO);
}

/*** syntax highlighting ***/

//fonction pour difinir la coleur pour les nombres 
void editorUpdateSyntax(erow *row) {
  row->hl = realloc(row->hl, row->rsize);
  memset(row->hl, HL_NORMAL, row->rsize);
  int i;
  for (i = 0; i < row->rsize; i++) {
    if (isdigit(row->render[i])) {
      row->hl[i] = HL_NUMBER;
    }
  }
}
//fonction pour attribuer des colleur a different partie du texte
int editorSyntaxToColor(int hl) {
  switch (hl) {
    case HL_NUMBER: return 35;
    case HL_MATCH: return 37;
    default: return 37;
  }
}

/*** row operations ***/

// redéfinir la position du curceur cx vert rx on cas de tabulation dans la ligne 
int editorRowCxToRx(erow *row, int cx) {
  int rx = 0;
  
  for (int i = 0; i < cx; i++) {
    if (row->chars[i] == '\t')
      rx += (VIM_TAB_STOP - 1) - (rx % VIM_TAB_STOP);
    rx++;
  }
  return rx;
}
int editorRowRxToCx(erow *row, int rx) {
  int cur_rx = 0;
  int cx;
  for (cx = 0; cx < row->size; cx++) {
    if (row->chars[cx] == '\t')
      cur_rx += (VIM_TAB_STOP - 1) - (cur_rx % VIM_TAB_STOP);
    cur_rx++;
    if (cur_rx > rx) return cx;
  }
  return cx;
}
// fonction qui copie la chaine chars vert render et qui gere la tabulation lu 
void editorUpdateRow(erow *row) {
  int tabs = 0;
  int j;
  for (j = 0; j < row->size; j++)
    if (row->chars[j] == '\t') tabs++;
  free(row->render);
  row->render = malloc(row->size + tabs*(VIM_TAB_STOP - 1) + 1);
  int idx = 0;
  for (j = 0; j < row->size; j++) {
    if (row->chars[j] == '\t') {
      row->render[idx++] = ' ';
      while (idx % VIM_TAB_STOP != 0) row->render[idx++] = ' ';
    } else {
      row->render[idx++] = row->chars[j];
    }
  }
  row->render[idx] = '\0';
  row->rsize = idx;
  editorUpdateSyntax(row);

}

//fonction qui Iserte une ligne dans le tablaux structure erow

void editorInsertRow(int at, char *s, size_t len) {
  if (at < 0 || at > E.numrows) return;
  E.row = realloc(E.row, sizeof(erow) * (E.numrows + 1));
  memmove(&E.row[at + 1], &E.row[at], sizeof(erow) * (E.numrows - at));
  E.row[at].size = len;
  E.row[at].chars = malloc(len + 1);
  memcpy(E.row[at].chars, s, len);
  E.row[at].chars[len] = '\0';
  E.row[at].rsize = 0;
  E.row[at].render = NULL;
  E.row[at].hl = NULL;
  editorUpdateRow(&E.row[at]);
  E.numrows++;
  E.dirty++;
}

void editorFreeRow(erow *row) {
  free(row->render);
  free(row->chars);
  free(row->hl);
}

// supprime une ligne dans le tablaux structure erow
void editorDelRow(int at) {
  if (at < 0 || at >= E.numrows) return;
  editorFreeRow(&E.row[at]);
  memmove(&E.row[at], &E.row[at + 1], sizeof(erow) * (E.numrows - at - 1));
  E.numrows--;
  E.dirty++;
}
// fonction qui ajoute un charactere dans le tablaux structure erow
void editorRowInsertChar(erow *row, int at, int c) {
  if (at < 0 || at > row->size) at = row->size;
  row->chars = realloc(row->chars, row->size + 2);
  memmove(&row->chars[at + 1], &row->chars[at], row->size - at + 1);
  row->size++;
  row->chars[at] = c;
  editorUpdateRow(row);
  E.dirty++;
}

void editorRowAppendString(erow *row, char *s, size_t len) {
  row->chars = realloc(row->chars, row->size + len + 1);
  memcpy(&row->chars[row->size], s, len);
  row->size += len;
  row->chars[row->size] = '\0';
  editorUpdateRow(row);
  E.dirty++;
}
// fonction qui supprime un caractere dans le tablaux structure erow 
void editorRowDelChar(erow *row, int at) {
  if (at < 0 || at >= row->size) return;
  memmove(&row->chars[at], &row->chars[at + 1], row->size - at);
  row->size--;
  editorUpdateRow(row);
  E.dirty++;
}

/*** editor operations ***/
//insert une nouvelle ligne 
void editorInsertNewline() {
  if (E.cx == 0) {
    editorInsertRow(E.cy, "", 0);
  } else {
    erow *row = &E.row[E.cy];
    editorInsertRow(E.cy + 1, &row->chars[E.cx], row->size - E.cx);
    row = &E.row[E.cy];
    row->size = E.cx;
    row->chars[row->size] = '\0';
    editorUpdateRow(row);
  }
  E.cy++;
  E.cx = 0;
}
//insert un caractere 

void editorInsertChar(int c) {
  if (E.cy == E.numrows) {
    editorInsertRow(E.numrows, "", 0);
  }
  if( E.screencols ==  E.row[E.cy].rsize ){
    E.cx=E.screencols;
    editorInsertNewline() ; 

  }
  editorRowInsertChar(&E.row[E.cy], E.cx, c);
  E.cx++;
}


//qui supprime un caractere
void editorDelChar() {
  if (E.cy == E.numrows) return;
  if (E.cx == 0 && E.cy == 0) return;
  erow *row = &E.row[E.cy];
  if (E.cx > 0) {
    editorRowDelChar(row, E.cx - 1);
    E.cx--;
  } else {
    E.cx = E.row[E.cy - 1].size;
    editorRowAppendString(&E.row[E.cy - 1], row->chars, row->size);
    editorDelRow(E.cy);
    E.cy--;
  }
} 
/*** file i/o ***/
//convertie les ligne de text on un seul buffer

char *editorRowsToString(int *buflen) {
  int totlen = 0;
  int j;
  for (j = 0; j < E.numrows; j++)
    totlen += E.row[j].size + 1;
  *buflen = totlen;
  char *buf = malloc(totlen);
  char *p = buf;
  for (j = 0; j < E.numrows; j++) {
    memcpy(p, E.row[j].chars, E.row[j].size);
    p += E.row[j].size;
    *p = '\n';
    p++;
  }
  return buf;
}
// fonction qui ouvre et lit le fichier 
void editorOpen(char *filename) {
  free(E.filename);
  E.filename = strdup(filename);
  FILE *fp = fopen(filename, "r");
  if (!fp) die("fopen");
  char *line = NULL;
  size_t linecap = 0;
  ssize_t linelen;
  while ((linelen = getline(&line, &linecap, fp)) != -1) {
    while (linelen > 0 && (line[linelen - 1] == '\n' || line[linelen - 1] == '\r'))
                           
      linelen--;
    editorInsertRow(E.numrows, line, linelen);
  }
  free(line);
  fclose(fp);
  E.dirty=0;
}

//fonction qui sauvegarde les modification 
void editorSave() {
  if (E.filename == NULL) {
      E.errorsmsg= 32;
      E.Mode=Mode_Normal ; 
      return;
  }
  int len;
  char *buf = editorRowsToString(&len);
  int fd = open(E.filename, O_RDWR | O_CREAT, 0644);
  if (fd != -1) {
    if (ftruncate(fd, len) != -1) {
      if (write(fd, buf, len) == len) {
        close(fd);
        free(buf);
        E.dirty = 0;
        E.Mode=Mode_Normal ; 
        return;
      }
    }
    close(fd);
  }
  free(buf);
}
/*** find ***/

//pointure fonction de recherche dans le texte
void editorFindCallback(char *query, int key) {
  static int last_match = -1;
  static int direction = 1;
  static int saved_hl_line;
  static char *saved_hl = NULL;
  if (saved_hl) {
    memcpy(E.row[saved_hl_line].hl, saved_hl, E.row[saved_hl_line].rsize);
    free(saved_hl);
    saved_hl = NULL;
  }
  if (key == '\r' || key == '\x1b') {
    last_match = -1;
    direction = 1;
    return;
  } else if (key == ARROW_RIGHT || key == ARROW_DOWN) {
    direction = 1;
  } else if (key == ARROW_LEFT || key == ARROW_UP) {
    direction = -1;
  } else {
    last_match = -1;
    direction = 1;
  }
  if (last_match == -1) direction = 1;
  int current = last_match;
  int i;
  for (i = 0; i < E.numrows; i++) {
    current += direction;
    if (current == -1) current = E.numrows - 1;
    else if (current == E.numrows) current = 0;
    erow *row = &E.row[current];
    char *match = strstr(row->render, query);
    if (match) {
      last_match = current;
      E.cy = current;
      E.cx = editorRowRxToCx(row, match - row->render);
      saved_hl_line = current;
      saved_hl = malloc(row->rsize);
      memcpy(saved_hl, row->hl, row->rsize);
      memset(&row->hl[match - row->render], HL_MATCH, strlen(query));
      break;
    }
  }
}
//recherche dans le texte
void editorFind() {
  int saved_cx = E.cx;
  int saved_cy = E.cy;
  int saved_coloff = E.coloff;
  int saved_rowoff = E.rowoff;
  char *query = editorPrompt("/%s", editorFindCallback);
  if (query) {
    E.Mode = Mode_Normal ; 
    free(query);
  } else {
    E.cx = saved_cx;
    E.cy = saved_cy;
    E.coloff = saved_coloff;
    E.rowoff = saved_rowoff;
    E.Mode=Mode_Normal ;
  }
}
/*** append buffer ***/

struct abuf {
  char *b;
  int len;
};

void abAppend(struct abuf *ab, const char *s, int len) {
  char *new = realloc(ab->b, ab->len + len);
  if (new == NULL) return;
  memcpy(&new[ab->len], s, len);
  ab->b = new;
  ab->len += len;
}
void abFree(struct abuf *ab) {
  free(ab->b);
}

#define ABUF_INIT {NULL, 0}
/*** input ***/

char *editorPrompt(char *prompt, void (*callback)(char *, int)) {
  size_t bufsize = 128;
  char *buf = malloc(bufsize);
  size_t buflen = 0;
  E.screencur = 2 ; 
  E.screencurbar = E.screenrows +1 ; 
  buf[0] = '\0';
  while (1) {

    editorSetStatusMessage(prompt, buf);
    editorRefreshScreen();
    int c = editorReadKey();
    if (c == DEL_KEY || c == BACK_SPACE) {
      if (buflen == 0){
        if (callback) callback(buf, c);
        free(buf);
        return NULL;
      }else{
        E.screencur--;
        buf[--buflen] = '\0';
      }
    } else if (c == '\x1b'  ) {

      if (callback) callback(buf, c);
      free(buf);
      return NULL;
    } else if (c == '\r') {
      if (buflen != 0) {
            
        if (callback) callback(buf, c);
        return buf;
      }
    } else if (!iscntrl(c) && c < 128) {
      if (buflen == bufsize - 1) {
        bufsize *= 2;
        buf = realloc(buf, bufsize);
      }
      E.screencur++;
      buf[buflen++] = c;
      buf[buflen] = '\0';

    }
    if (callback) callback(buf, c);

  }
}


void editorMoveCursor(int key) {
  erow *row = (E.cy >= E.numrows) ? NULL : &E.row[E.cy];
  switch (key) {
    case ARROW_LEFT:
      if (E.cx != 0) {
        E.cx--;
      } 
      break;
    case BACK_SPACE :
      if (E.cx != 0) {
        E.cx--;
      } 
      else if (E.cy > 0) {
        E.cy--;
        E.cx = E.row[E.cy].size;
      }
       break;
    case ARROW_RIGHT:
      if (row && E.cx < row->size) {
        E.cx++;
      }
      break;
    case ARROW_UP:
      if (E.cy != 0) {
        E.cy--;
      }
      break;
    case ARROW_DOWN:
      if (E.cy < E.numrows  ) {
        E.cy++;
      }

      break;

  }
  row = (E.cy >= E.numrows) ? NULL : &E.row[E.cy];
  int rowlen = row ? row->size : 0;
  if (E.cx > rowlen) {
    E.cx = rowlen;
  }
}

void editorProcessKeypress() {
  int c = editorReadKey();
  if(E.Mode==Mode_Normal){
    switch (c) {
      case ':':
      {
        E.errorsmsg=0 ; 
        E.Mode=Mode_Complex_command_prompt ; 
        E.oper = editorPrompt(":%s ",NULL);
        if(E.oper != NULL){
          if(E.oper[0]=='q' && E.dirty == 0 && (E.oper[1]==' ' || E.oper[1]=='\0')){
           write(STDOUT_FILENO, "\x1b[2J", 4);
           write(STDOUT_FILENO, "\x1b[H", 3);
           exit(0);
          }else if(E.oper[0]=='w' && E.oper[1]=='\0'){
                editorSave();

          }else if(E.oper[0]=='w' && E.oper[1]=='q'){
                  editorSave();
                write(STDOUT_FILENO, "\x1b[2J", 4);
                write(STDOUT_FILENO, "\x1b[H", 3);
                exit(0);
          }else if(E.oper[0]=='q' ){
                  if(E.oper[1]=='!'){
                      write(STDOUT_FILENO, "\x1b[2J", 4);
                       write(STDOUT_FILENO, "\x1b[H", 3);
                       exit(0);
                  }
                  E.errorsmsg=37 ; 
                  E.Mode=Mode_Normal ; 
          }else if(E.oper[0]=='w'){
              E.filename=strchr(E.oper,E.oper[2]) ; 
              editorSave();

          }else{
              E.errorsmsg=492 ; 
              E.Mode=Mode_Normal ; 
          }

        }else{
             E.Mode=Mode_Normal ;
        }

    }
    break;
    case '/':
      {
        E.Mode=Mode_Complex_command_prompt ; 
        editorFind();
      }
    break;
    case HOME_KEY:
      E.cx = 0;
    break;
    case END_KEY:
    if (E.cy < E.numrows)
      E.cx = E.row[E.cy].size;
    break;
    case PAGE_UP:
    case PAGE_DOWN:
    {
      if (c == PAGE_UP) {
        E.cy = E.rowoff;
      } else if (c == PAGE_DOWN) {
        E.cy = E.rowoff + E.screenrows - 1;
        if (E.cy > E.numrows) E.cy = E.numrows;
      }
      int times = E.screenrows;
      while (times--)
        editorMoveCursor(c == PAGE_UP ? ARROW_UP : ARROW_DOWN);
    }
    break;
    case ARROW_UP:
    case ARROW_DOWN:
    case ARROW_LEFT:
    case BACK_SPACE : 
    case ARROW_RIGHT:
    editorMoveCursor(c);
    break;
    case ENTER :
    if (E.cy < E.numrows -1 ) {
        E.cy++;
        E.cx =0 ; 
      }
    break;
  }
}
if (E.Mode==Mode_INSERTION){
  switch (c) {
    case '\r':
      editorInsertNewline();
    break;
    case HOME_KEY:
      E.cx = 0;
    break;
    case END_KEY:
      if (E.cy < E.numrows)
        E.cx = E.row[E.cy].size;
    break;
    case BACK_SPACE:
    case DEL_KEY:
      {
        editorMoveCursor(ARROW_RIGHT);
        editorDelChar();
      }
    break;
    case PAGE_UP:
    case PAGE_DOWN:
      {
        if (c == PAGE_UP) {
          E.cy = E.rowoff;
        } else if (c == PAGE_DOWN) {
          E.cy = E.rowoff + E.screenrows - 1;
          if (E.cy > E.numrows) E.cy = E.numrows;
        }
        int times = E.screenrows;
        while (times--)
          editorMoveCursor(c == PAGE_UP ? ARROW_UP : ARROW_DOWN);
      }
    break;
    case ARROW_UP:
    case ARROW_DOWN:
    case ARROW_LEFT:
    case ARROW_RIGHT:
      editorMoveCursor(c);
    break;
    case '\x1b':

    break;
    default:
      editorInsertChar(c);
    break;
  }

}

}

/*** output ***/
void editorScroll() {
    E.rx = 0;
    if (E.cy < E.numrows) {
      E.rx = editorRowCxToRx(&E.row[E.cy], E.cx);
    }
    if (E.cy < E.rowoff) {
      E.rowoff = E.cy;
    }
    if (E.cy >= E.rowoff + E.screenrows) {
      E.rowoff = E.cy - E.screenrows + 1;
    }
    if (E.rx < E.coloff) {
      E.coloff = E.rx;
    }
}



void editorDrawRows(struct abuf *ab) {
  int y;
  for (y = 0; y < E.screenrows; y++) {
    int filerow = y + E.rowoff;
    if (filerow >= E.numrows) {
      if (E.numrows == 0 && y == E.screenrows / 3) {
        char welcome[80];
        int welcomelen = snprintf(welcome, sizeof(welcome),"%s", VIM_VERSION);  
        if (welcomelen > E.screencols) welcomelen = E.screencols;
        int padding = (E.screencols - welcomelen) / 2;
        if (padding) {
          abAppend(ab, "~", 1);
          padding--;
        }
        while (padding--) abAppend(ab, " ", 1);
        abAppend(ab, welcome, welcomelen);
      } else {
        abAppend(ab, "~", 1);
      }
    } else {
      int len = E.row[filerow].rsize - E.coloff;
      if (len < 0) len = 0;
      if (len > E.screencols) len = E.screencols;

      char *c = &E.row[filerow].render[E.coloff];
      unsigned char *hl = &E.row[filerow].hl[E.coloff];
      int current_color = -1;
      int j;
      for (j = 0; j < len; j++) {
         if (hl[j] == HL_MATCH) {
          abAppend(ab, "\x1b[7m", 4);
          abAppend(ab, &c[j], 1);
          abAppend(ab, "\x1b[m", 3);
         if (current_color != -1) {
            char buf[16];
            int clen = snprintf(buf, sizeof(buf), "\x1b[%dm", current_color);
            abAppend(ab, buf, clen);
          }
        } else if (hl[j] == HL_NORMAL) {
          if (current_color != -1) {
            abAppend(ab, "\x1b[39m", 5);
            current_color = -1;
          }
          abAppend(ab, &c[j], 1);
        } else {
          int color = editorSyntaxToColor(hl[j]);
          if (color != current_color) {
            current_color = color;
            char buf[16];
            int clen = snprintf(buf, sizeof(buf), "\x1b[%dm", color);
            abAppend(ab, buf, clen);
          }
          abAppend(ab, &c[j], 1);
        }
      }
      abAppend(ab, "\x1b[m", 3);
      abAppend(ab, "\x1b[39m", 5);

    }
    abAppend(ab, "\x1b[K", 3);
    abAppend(ab, "\r\n", 2);
  }
 
}

void editorDrawStatusBar(struct abuf *ab) {


  if(E.Mode!= Mode_Complex_command_prompt){
    char status[80], rstatus[80];
    int len ; 
    if(E.Mode==Mode_INSERTION){
      abAppend(ab, "\e[37;1m", 7);
      len = snprintf(status, sizeof(status), "--  INSERTION --");
      
    }else if (E.Mode==Mode_Normal && E.errorsmsg==0){
      abAppend(ab, "\e[37;1m", 7);
      if(E.filename  != NULL ){
        len = snprintf(status, sizeof(status), "\"%.20s\" %dL,%dC", E.filename , E.numrows , E.numrows*(E.screencols)); 
      }else {
        len = snprintf(status, sizeof(status), "%.20s %dL", "" , E.numrows); 
      }
      
    }
    else if (E.Mode==Mode_Normal && E.errorsmsg==32) {
      abAppend(ab, "\e[41m", 5);
      len = snprintf(status, sizeof(status), ERROR_MSG_32 );
    }
    else if (E.Mode==Mode_Normal && E.errorsmsg==37) {
      abAppend(ab, "\e[41m", 5);
      len = snprintf(status, sizeof(status), ERROR_MSG_37 );
    }else if (E.Mode==Mode_Normal && E.errorsmsg==492) {
      abAppend(ab, "\e[41m", 5);
      len = snprintf(status, sizeof(status), ERROR_MSG_492 );
    }
      if (len > E.screencols) len = E.screencols;
      abAppend(ab, status, len);
      abAppend(ab, "\x1b[m", 3);
      abAppend(ab, "\e[37;1m", 7);
      int rlen ;
    if(E.numrows < E.screenrows){
      rlen = snprintf(rstatus, sizeof(rstatus), "%d,%d       Tout",E.cy +1,E.cx+1);
    }
    if(E.cy < E.screenrows){
      rlen = snprintf(rstatus, sizeof(rstatus), "%d,%d       Haut",E.cy +1,E.cx+1);
    }else {
      rlen = snprintf(rstatus, sizeof(rstatus), "%d,%d       Bas",E.cy +1,E.cx+1);
    }
        


    while (len < E.screencols) {
      if (E.screencols - len == rlen) {
        abAppend(ab, rstatus, rlen);
        break;
      } else {
        abAppend(ab, " ", 1);
        len++;
      }
    }
    abAppend(ab, "\x1b[m", 3);
  }else{
     abAppend(ab, "\x1b[K", 3);
     int msglen = strlen(E.statusmsg);
     if (msglen > E.screencols) msglen = E.screencols;
    abAppend(ab, E.statusmsg, msglen);
  }
  
}

void editorDrawMessageBar(struct abuf *ab) {
  abAppend(ab, "\x1b[K", 3);
  int msglen = strlen(E.statusmsg);
  if (msglen > E.screencols) msglen = E.screencols;
  abAppend(ab, E.statusmsg, msglen);
} 

void editorRefreshScreen() {
  editorScroll();
  struct abuf ab = ABUF_INIT;
  abAppend(&ab, "\x1b[?25l", 6);
  abAppend(&ab, "\x1b[H", 3);
       
  editorDrawRows(&ab);
  if(E.Mode!= Mode_Complex_command_prompt){
      editorDrawStatusBar(&ab);
  }else{
      editorDrawStatusBar(&ab);
  }

  char buf[32];
  if(E.Mode!= Mode_Complex_command_prompt){
    snprintf(buf, sizeof(buf), "\x1b[%d;%dH", (E.cy - E.rowoff) + 1, (E.rx - E.coloff) + 1);
    abAppend(&ab, buf, strlen(buf));  
    abAppend(&ab, "\x1b[?25h", 6);
  }else{
    snprintf(buf, sizeof(buf), "\x1b[%d;%dH", E.screencurbar, E.screencur);
    abAppend(&ab, buf, strlen(buf));
    abAppend(&ab, "\x1b[?25h", 6);
  }

  write(STDOUT_FILENO, ab.b, ab.len);
  abFree(&ab);
}

void editorSetStatusMessage(const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vsnprintf(E.statusmsg, sizeof(E.statusmsg), fmt, ap);
  va_end(ap);

}

/*** init ***/

void initEditor() {
  E.cx = 0;
  E.cy = 0;
  E.rx = 0;
  E.rowoff = 0;
  E.coloff = 0;
  E.dirty= 0;
  E.numrows = 0;
  E.row = NULL; 
  if ( getWindowSize(&E.screenrows, &E.screencols,STDIN_FILENO)== -1) die("getWindowSize");
  E.screenrows -= 1;
  E.filename = NULL;
  E.Mode=Mode_Normal ; 
  E.statusmsg[0] = '\0';
  E.errorsmsg = 0 ;
  E.oper=NULL ; 
} 
int main(int argc,char** argv)
{
  enableRawMode();
  initEditor();
  if (argc >= 2) {
      editorOpen(argv[1]);
    }
    if (signal(SIGWINCH, sig_winch) == SIG_ERR)
    perror("signal error");
    int i = 1 ; 
  while (i) {
    editorRefreshScreen() ; 
    editorProcessKeypress();
    i++;
 }
  return 0;
}